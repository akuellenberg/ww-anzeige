EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title "Winkelwertanzeige für Steuerpult"
Date "2020-12-05"
Rev "A"
Comp "MPIfR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4600 4850 4700 4850
Text HLabel 4500 5300 0    50   Input ~ 0
~7SEG_OE
Text HLabel 4500 4500 0    50   Input ~ 0
7SEG_LE
Text HLabel 4500 4400 0    50   Input ~ 0
7SEG_CLK
Text HLabel 4500 4300 0    50   Input ~ 0
7SEG_SDI
Wire Wire Line
	4800 5300 4500 5300
Wire Wire Line
	4800 4500 4500 4500
Wire Wire Line
	4500 4400 4800 4400
Wire Wire Line
	4800 4300 4500 4300
Wire Wire Line
	5200 5550 5200 5500
$Comp
L power:GND #PWR017
U 1 1 5F94E9D5
P 5200 5550
AR Path="/5F94EF90/5F94E9D5" Ref="#PWR017"  Part="1" 
AR Path="/5F9CACEF/5F94E9D5" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5F94E9D5" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 5200 5300 50  0001 C CNN
F 1 "GND" H 5205 5377 50  0001 C CNN
F 2 "" H 5200 5550 50  0001 C CNN
F 3 "" H 5200 5550 50  0001 C CNN
	1    5200 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4050 5200 4100
$Comp
L power:+3V3 #PWR016
U 1 1 5F94C591
P 5200 4050
AR Path="/5F94EF90/5F94C591" Ref="#PWR016"  Part="1" 
AR Path="/5F9CACEF/5F94C591" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5F94C591" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 5200 3900 50  0001 C CNN
F 1 "+3V3" H 5215 4223 50  0000 C CNN
F 2 "" H 5200 4050 50  0001 C CNN
F 3 "" H 5200 4050 50  0001 C CNN
	1    5200 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2050 5200 2100
$Comp
L power:+3V3 #PWR014
U 1 1 5F94798B
P 5200 2050
AR Path="/5F94EF90/5F94798B" Ref="#PWR014"  Part="1" 
AR Path="/5F9CACEF/5F94798B" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5F94798B" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 5200 1900 50  0001 C CNN
F 1 "+3V3" H 5215 2223 50  0000 C CNN
F 2 "" H 5200 2050 50  0001 C CNN
F 3 "" H 5200 2050 50  0001 C CNN
	1    5200 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3250 5200 3200
$Comp
L power:GND #PWR015
U 1 1 5F9455C0
P 5200 3250
AR Path="/5F94EF90/5F9455C0" Ref="#PWR015"  Part="1" 
AR Path="/5F9CACEF/5F9455C0" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5F9455C0" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 5200 3000 50  0001 C CNN
F 1 "GND" H 5205 3077 50  0001 C CNN
F 2 "" H 5200 3250 50  0001 C CNN
F 3 "" H 5200 3250 50  0001 C CNN
	1    5200 3250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:TBD62783A U1
U 1 1 5F93C100
P 5200 2600
AR Path="/5F94EF90/5F93C100" Ref="U1"  Part="1" 
AR Path="/5F9CACEF/5F93C100" Ref="U?"  Part="1" 
AR Path="/5F9D4CEE/5F93C100" Ref="U3"  Part="1" 
F 0 "U3" H 5350 3050 50  0000 C CNN
F 1 "TBD62783A" H 5450 2050 50  0000 C CNN
F 2 "Package_SO:SSOP-18_4.4x6.5mm_P0.65mm" H 5200 2050 50  0001 C CNN
F 3 "http://toshiba.semicon-storage.com/info/docget.jsp?did=30523&prodName=TBD62783APG" H 4900 3000 50  0001 C CNN
F 4 "TBD62783AFNG,EL" H 5200 2600 50  0001 C CNN "Manufacturer"
F 5 "757-TBD62783AFNGEL" H 5200 2600 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 2600 50  0001 C CNN "FP checked"
	1    5200 2600
	1    0    0    -1  
$EndComp
$Comp
L Driver_LED:STP08CP05XT U2
U 1 1 5F93BE4E
P 5200 4800
AR Path="/5F94EF90/5F93BE4E" Ref="U2"  Part="1" 
AR Path="/5F9CACEF/5F93BE4E" Ref="U?"  Part="1" 
AR Path="/5F9D4CEE/5F93BE4E" Ref="U4"  Part="1" 
F 0 "U4" H 5350 5450 50  0000 C CNN
F 1 "STP08CP05XT" H 5500 4150 50  0000 C CNN
F 2 "Package_SO:TSSOP-16-1EP_4.4x5mm_P0.65mm" H 5200 4800 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stp08cp05.pdf" H 5200 4800 50  0001 C CNN
F 4 "STP08CP05XTTR" H 5200 4800 50  0001 C CNN "Manufacturer"
F 5 "511-STP08CP05XTTR" H 5200 4800 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 4800 50  0001 C CNN "FP checked"
F 7 "188-9327" H 5200 4800 50  0001 C CNN "RS"
	1    5200 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2300 5850 2300
Text HLabel 5850 2300 2    50   Output ~ 0
DIGIT_0
Wire Wire Line
	5600 2400 5850 2400
Wire Wire Line
	5600 2500 5850 2500
Wire Wire Line
	5600 2600 5850 2600
Wire Wire Line
	5600 2700 5850 2700
Wire Wire Line
	5600 2800 5850 2800
Wire Wire Line
	5600 2900 5850 2900
Wire Wire Line
	5600 3000 5850 3000
Text HLabel 5850 2400 2    50   Output ~ 0
DIGIT_1
Text HLabel 5850 2500 2    50   Output ~ 0
DIGIT_2
Text HLabel 5850 2600 2    50   Output ~ 0
DIGIT_3
Text HLabel 5850 2700 2    50   Output ~ 0
DIGIT_4
Text HLabel 5850 2800 2    50   Output ~ 0
DIGIT_5
Text HLabel 5850 2900 2    50   Output ~ 0
DIGIT_6
Text HLabel 5850 3000 2    50   Output ~ 0
DIGIT_7
Text HLabel 5750 4700 2    50   Output ~ 0
DP
Text HLabel 5750 4800 2    50   Output ~ 0
SEG_G
Text HLabel 5750 4900 2    50   Output ~ 0
SEG_F
Text HLabel 5750 5000 2    50   Output ~ 0
SEG_E
Text HLabel 5750 4300 2    50   Output ~ 0
SEG_D
Text HLabel 5750 4400 2    50   Output ~ 0
SEG_C
Text HLabel 5750 4500 2    50   Output ~ 0
SEG_B
Text HLabel 5750 4600 2    50   Output ~ 0
SEG_A
Wire Wire Line
	5600 5000 5750 5000
Wire Wire Line
	5600 4900 5750 4900
Wire Wire Line
	5600 4800 5750 4800
Wire Wire Line
	5600 4700 5750 4700
Wire Wire Line
	5600 4600 5750 4600
Wire Wire Line
	5600 4500 5750 4500
Wire Wire Line
	5600 4400 5750 4400
Wire Wire Line
	5600 4300 5750 4300
Wire Wire Line
	5600 5200 5750 5200
Text HLabel 5750 5200 2    50   Output ~ 0
7SEG_SDO
Text Notes 4650 1700 0    79   ~ 0
8 channel P-MOS array
Text Notes 3900 3750 0    79   ~ 0
Shift register with programmable output current
Text Notes 1400 1150 0    157  ~ 0
Multiplexer for 8 digit 7-segment LED display with common anode
$Comp
L power:+3V3 #PWR?
U 1 1 5FB40006
P 7350 2400
AR Path="/5F9C95B0/5FB40006" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5FB40006" Ref="#PWR025"  Part="1" 
AR Path="/5F94EF90/5FB40006" Ref="#PWR018"  Part="1" 
F 0 "#PWR025" H 7350 2250 50  0001 C CNN
F 1 "+3V3" H 7365 2573 50  0000 C CNN
F 2 "" H 7350 2400 50  0001 C CNN
F 3 "" H 7350 2400 50  0001 C CNN
	1    7350 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5FB4000C
P 7350 2600
AR Path="/5F9C95B0/5FB4000C" Ref="C?"  Part="1" 
AR Path="/5F94EF90/5FB4000C" Ref="C1"  Part="1" 
AR Path="/5F9D4CEE/5FB4000C" Ref="C4"  Part="1" 
F 0 "C4" H 7438 2646 50  0000 L CNN
F 1 "47uF" H 7438 2555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 7350 2600 50  0001 C CNN
F 3 "~" H 7350 2600 50  0001 C CNN
F 4 "T491B476K010AT" H 7350 2600 50  0001 C CNN "Manufacturer"
F 5 "80-T491B476K010AUTO" H 7350 2600 50  0001 C CNN "Mouser"
F 6 "ok" H 7350 2600 50  0001 C CNN "FP checked"
F 7 "648-0597" H 7350 2600 50  0001 C CNN "RS"
	1    7350 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB40012
P 7350 2800
AR Path="/5F9C95B0/5FB40012" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5FB40012" Ref="#PWR026"  Part="1" 
AR Path="/5F94EF90/5FB40012" Ref="#PWR019"  Part="1" 
F 0 "#PWR026" H 7350 2550 50  0001 C CNN
F 1 "GND" H 7355 2627 50  0001 C CNN
F 2 "" H 7350 2800 50  0001 C CNN
F 3 "" H 7350 2800 50  0001 C CNN
	1    7350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2800 7350 2750
Wire Wire Line
	7350 2500 7350 2450
Wire Wire Line
	4800 2300 4400 2300
Wire Wire Line
	4800 2400 4400 2400
Wire Wire Line
	4800 2500 4400 2500
Wire Wire Line
	4800 2600 4400 2600
Wire Wire Line
	4800 2700 4400 2700
Wire Wire Line
	4800 2800 4400 2800
Wire Wire Line
	4800 2900 4400 2900
Wire Wire Line
	4800 3000 4400 3000
Text HLabel 4400 2300 0    50   Input ~ 0
Y0
Text HLabel 4400 2400 0    50   Input ~ 0
Y1
Text HLabel 4400 2500 0    50   Input ~ 0
Y2
Text HLabel 4400 2600 0    50   Input ~ 0
Y3
Text HLabel 4400 2700 0    50   Input ~ 0
Y4
Text HLabel 4400 2800 0    50   Input ~ 0
Y5
Text HLabel 4400 2900 0    50   Input ~ 0
Y6
Text HLabel 4400 3000 0    50   Input ~ 0
Y7
$Comp
L Device:C_Small C2
U 1 1 60033708
P 7750 2600
AR Path="/5F94EF90/60033708" Ref="C2"  Part="1" 
AR Path="/5F9D4CEE/60033708" Ref="C5"  Part="1" 
F 0 "C5" H 7842 2646 50  0000 L CNN
F 1 "100nF" H 7842 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7750 2600 50  0001 C CNN
F 3 "~" H 7750 2600 50  0001 C CNN
F 4 "06035C104K4T2A" H 7750 2600 50  0001 C CNN "Manufacturer"
F 5 "698-3251" H 7750 2600 50  0001 C CNN "RS"
	1    7750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2450 7750 2450
Wire Wire Line
	7750 2450 7750 2500
Connection ~ 7350 2450
Wire Wire Line
	7350 2450 7350 2400
Connection ~ 7750 2450
Wire Wire Line
	7350 2750 7750 2750
Wire Wire Line
	7750 2750 7750 2700
Connection ~ 7350 2750
Wire Wire Line
	7350 2750 7350 2700
Connection ~ 7750 2750
Wire Wire Line
	8150 2750 8150 2700
Wire Wire Line
	7750 2750 8150 2750
Wire Wire Line
	8150 2450 8150 2500
Wire Wire Line
	7750 2450 8150 2450
$Comp
L Device:C_Small C3
U 1 1 60033ECA
P 8150 2600
AR Path="/5F94EF90/60033ECA" Ref="C3"  Part="1" 
AR Path="/5F9D4CEE/60033ECA" Ref="C6"  Part="1" 
F 0 "C6" H 8242 2646 50  0000 L CNN
F 1 "100nF" H 8242 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8150 2600 50  0001 C CNN
F 3 "~" H 8150 2600 50  0001 C CNN
F 4 "06035C104K4T2A" H 8150 2600 50  0001 C CNN "Manufacturer"
F 5 "698-3251" H 8150 2600 50  0001 C CNN "RS"
	1    8150 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5F9DF820
P 4500 4850
AR Path="/5F94EF90/5F9DF820" Ref="R2"  Part="1" 
AR Path="/5F9CACEF/5F9DF820" Ref="R?"  Part="1" 
AR Path="/5F9D4CEE/5F9DF820" Ref="R4"  Part="1" 
F 0 "R4" V 4696 4850 50  0000 C CNN
F 1 "100R" V 4605 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4500 4850 50  0001 C CNN
F 3 "~" H 4500 4850 50  0001 C CNN
F 4 "CRG0603F100R" H 4500 4850 50  0001 C CNN "Manufacturer"
F 5 "134-5430" H 4500 4850 50  0001 C CNN "RS"
	1    4500 4850
	0    1    -1   0   
$EndComp
Wire Wire Line
	4700 4850 4700 4700
Wire Wire Line
	4700 4700 4800 4700
$Comp
L Device:R_POT_TRIM RV2
U 1 1 5FCB87E6
P 3900 4850
AR Path="/5F9D4CEE/5FCB87E6" Ref="RV2"  Part="1" 
AR Path="/5F94EF90/5FCB87E6" Ref="RV1"  Part="1" 
F 0 "RV2" V 3693 4850 50  0000 C CNN
F 1 "5k" V 3784 4850 50  0000 C CNN
F 2 "ak:Potentiometer_Bourns_3214W_Vertical" H 3900 4850 50  0001 C CNN
F 3 "~" H 3900 4850 50  0001 C CNN
F 4 "3214W-1-502E" H 3900 4850 50  0001 C CNN "Manufacturer"
F 5 "240-1556" H 3900 4850 50  0001 C CNN "RS"
	1    3900 4850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5F9E03BA
P 3900 5050
AR Path="/5F94EF90/5F9E03BA" Ref="#PWR013"  Part="1" 
AR Path="/5F9CACEF/5F9E03BA" Ref="#PWR?"  Part="1" 
AR Path="/5F9D4CEE/5F9E03BA" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 3900 4800 50  0001 C CNN
F 1 "GND" H 3905 4877 50  0001 C CNN
F 2 "" H 3900 5050 50  0001 C CNN
F 3 "" H 3900 5050 50  0001 C CNN
	1    3900 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4850 4150 4850
Wire Wire Line
	3900 5050 3900 5000
NoConn ~ 3750 4850
$Comp
L Device:R_Small R1
U 1 1 5FD51A7F
P 4250 4850
AR Path="/5F94EF90/5FD51A7F" Ref="R1"  Part="1" 
AR Path="/5F9CACEF/5FD51A7F" Ref="R?"  Part="1" 
AR Path="/5F9D4CEE/5FD51A7F" Ref="R3"  Part="1" 
F 0 "R3" V 4446 4850 50  0000 C CNN
F 1 "100R" V 4355 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4250 4850 50  0001 C CNN
F 3 "~" H 4250 4850 50  0001 C CNN
F 4 "CRG0603F100R" H 4250 4850 50  0001 C CNN "Manufacturer"
F 5 "134-5430" H 4250 4850 50  0001 C CNN "RS"
	1    4250 4850
	0    1    -1   0   
$EndComp
Wire Wire Line
	4350 4850 4400 4850
$EndSCHEMATC
