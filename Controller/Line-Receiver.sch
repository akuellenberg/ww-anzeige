EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title "Winkelwertanzeige für Steuerpult"
Date "2020-12-05"
Rev "A"
Comp "MPIfR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U7
U 1 1 5FF9190C
P 4550 3750
AR Path="/5FF144B4/5FF9190C" Ref="U7"  Part="1" 
AR Path="/5F956CA5/5FF9190C" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FF9190C" Ref="U?"  Part="1" 
AR Path="/5FF9190C" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FF9190C" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FF9190C" Ref="U14"  Part="1" 
F 0 "U14" H 4650 3900 50  0000 L CNN
F 1 "74AHC1G14" H 4600 3650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 3615 50  0001 C CNN
F 3 "" H 4695 3865 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 3750 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 3750 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 3750 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 3750 50  0001 C CNN "RS"
	1    4550 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR041
U 1 1 5FF91912
P 4500 4100
AR Path="/5FF144B4/5FF91912" Ref="#PWR041"  Part="1" 
AR Path="/5F956CA5/5FF91912" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91912" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91912" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91912" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 4500 3850 50  0001 C CNN
F 1 "GND" H 4505 3927 50  0001 C CNN
F 2 "" H 4500 4100 50  0001 C CNN
F 3 "" H 4500 4100 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4100 4500 3950
$Comp
L Diode:BAT54S D3
U 1 1 5FF91919
P 3100 3750
AR Path="/5FF144B4/5FF91919" Ref="D3"  Part="1" 
AR Path="/5F956CA5/5FF91919" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF91919" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF91919" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF91919" Ref="D11"  Part="1" 
F 0 "D11" V 3146 3838 50  0000 L CNN
F 1 "BAT54S" V 3055 3838 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 3875 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 3750 50  0001 C CNN
F 4 "BAT54S,215" H 3100 3750 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 3750 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 3750 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 3750 50  0001 C CNN "RS"
	1    3100 3750
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5FF9191F
P 2700 3750
AR Path="/5FF144B4/5FF9191F" Ref="R13"  Part="1" 
AR Path="/5F956CA5/5FF9191F" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF9191F" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF9191F" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF9191F" Ref="R39"  Part="1" 
F 0 "R39" V 2650 3900 50  0000 C CNN
F 1 "10K" V 2650 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 3750 50  0001 C CNN
F 3 "~" H 2700 3750 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 3750 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 3750 50  0001 C CNN "Mouser"
	1    2700 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5FF91925
P 3700 3750
AR Path="/5FF144B4/5FF91925" Ref="R19"  Part="1" 
AR Path="/5F956CA5/5FF91925" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91925" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91925" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91925" Ref="R45"  Part="1" 
F 0 "R45" V 3650 3900 50  0000 C CNN
F 1 "1k" V 3650 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 3750 50  0001 C CNN
F 3 "~" H 3700 3750 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 3750 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 3750 50  0001 C CNN "RS"
	1    3700 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 4100 3100 4050
Wire Wire Line
	2800 3750 2900 3750
Wire Wire Line
	2900 3750 3600 3750
Connection ~ 2900 3750
Wire Wire Line
	3800 3750 4000 3750
$Comp
L power:GND #PWR042
U 1 1 5FF9193E
P 4500 5200
AR Path="/5FF144B4/5FF9193E" Ref="#PWR042"  Part="1" 
AR Path="/5F956CA5/5FF9193E" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF9193E" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF9193E" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF9193E" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 4500 4950 50  0001 C CNN
F 1 "GND" H 4505 5027 50  0001 C CNN
F 2 "" H 4500 5200 50  0001 C CNN
F 3 "" H 4500 5200 50  0001 C CNN
	1    4500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5200 4500 5050
$Comp
L Diode:BAT54S D4
U 1 1 5FF91945
P 3100 4850
AR Path="/5FF144B4/5FF91945" Ref="D4"  Part="1" 
AR Path="/5F956CA5/5FF91945" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF91945" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF91945" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF91945" Ref="D12"  Part="1" 
F 0 "D12" V 3146 4938 50  0000 L CNN
F 1 "BAT54S" V 3055 4938 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 4975 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 4850 50  0001 C CNN
F 4 "BAT54S,215" H 3100 4850 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 4850 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 4850 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 4850 50  0001 C CNN "RS"
	1    3100 4850
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5FF9194B
P 2700 4850
AR Path="/5FF144B4/5FF9194B" Ref="R14"  Part="1" 
AR Path="/5F956CA5/5FF9194B" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF9194B" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF9194B" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF9194B" Ref="R40"  Part="1" 
F 0 "R40" V 2650 5000 50  0000 C CNN
F 1 "10K" V 2650 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 4850 50  0001 C CNN
F 3 "~" H 2700 4850 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 4850 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 4850 50  0001 C CNN "Mouser"
	1    2700 4850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R20
U 1 1 5FF91951
P 3700 4850
AR Path="/5FF144B4/5FF91951" Ref="R20"  Part="1" 
AR Path="/5F956CA5/5FF91951" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91951" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91951" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91951" Ref="R46"  Part="1" 
F 0 "R46" V 3650 5000 50  0000 C CNN
F 1 "1k" V 3650 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 4850 50  0001 C CNN
F 3 "~" H 3700 4850 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 4850 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 4850 50  0001 C CNN "RS"
	1    3700 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 5200 3100 5150
Wire Wire Line
	2800 4850 2900 4850
Wire Wire Line
	2900 4850 3600 4850
Connection ~ 2900 4850
Wire Wire Line
	3800 4850 4000 4850
Wire Wire Line
	2450 4850 2600 4850
Wire Wire Line
	2450 3950 2450 4850
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U9
U 1 1 5FF9196C
P 4550 6000
AR Path="/5FF144B4/5FF9196C" Ref="U9"  Part="1" 
AR Path="/5F956CA5/5FF9196C" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FF9196C" Ref="U?"  Part="1" 
AR Path="/5FF9196C" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FF9196C" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FF9196C" Ref="U16"  Part="1" 
F 0 "U16" H 4650 6150 50  0000 L CNN
F 1 "74AHC1G14" H 4600 5900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 5865 50  0001 C CNN
F 3 "" H 4695 6115 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 6000 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 6000 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 6000 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 6000 50  0001 C CNN "RS"
	1    4550 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5FF91972
P 4500 6350
AR Path="/5FF144B4/5FF91972" Ref="#PWR043"  Part="1" 
AR Path="/5F956CA5/5FF91972" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91972" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91972" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91972" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 4500 6100 50  0001 C CNN
F 1 "GND" H 4505 6177 50  0001 C CNN
F 2 "" H 4500 6350 50  0001 C CNN
F 3 "" H 4500 6350 50  0001 C CNN
	1    4500 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6350 4500 6200
$Comp
L Diode:BAT54S D5
U 1 1 5FF91979
P 3100 6000
AR Path="/5FF144B4/5FF91979" Ref="D5"  Part="1" 
AR Path="/5F956CA5/5FF91979" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF91979" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF91979" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF91979" Ref="D13"  Part="1" 
F 0 "D13" V 3146 6088 50  0000 L CNN
F 1 "BAT54S" V 3055 6088 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 6125 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 6000 50  0001 C CNN
F 4 "BAT54S,215" H 3100 6000 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 6000 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 6000 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 6000 50  0001 C CNN "RS"
	1    3100 6000
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5FF9197F
P 2700 6000
AR Path="/5FF144B4/5FF9197F" Ref="R15"  Part="1" 
AR Path="/5F956CA5/5FF9197F" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF9197F" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF9197F" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF9197F" Ref="R41"  Part="1" 
F 0 "R41" V 2650 6150 50  0000 C CNN
F 1 "10K" V 2650 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 6000 50  0001 C CNN
F 3 "~" H 2700 6000 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 6000 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 6000 50  0001 C CNN "Mouser"
	1    2700 6000
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R21
U 1 1 5FF91985
P 3700 6000
AR Path="/5FF144B4/5FF91985" Ref="R21"  Part="1" 
AR Path="/5F956CA5/5FF91985" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91985" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91985" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91985" Ref="R47"  Part="1" 
F 0 "R47" V 3650 6150 50  0000 C CNN
F 1 "1k" V 3650 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 6000 50  0001 C CNN
F 3 "~" H 3700 6000 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 6000 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 6000 50  0001 C CNN "RS"
	1    3700 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 6350 3100 6300
Wire Wire Line
	2800 6000 2900 6000
Wire Wire Line
	2900 6000 3600 6000
Connection ~ 2900 6000
Wire Wire Line
	3800 6000 4000 6000
$Comp
L power:GND #PWR044
U 1 1 5FF91998
P 4500 7450
AR Path="/5FF144B4/5FF91998" Ref="#PWR044"  Part="1" 
AR Path="/5F956CA5/5FF91998" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91998" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91998" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91998" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 4500 7200 50  0001 C CNN
F 1 "GND" H 4505 7277 50  0001 C CNN
F 2 "" H 4500 7450 50  0001 C CNN
F 3 "" H 4500 7450 50  0001 C CNN
	1    4500 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 7450 4500 7300
$Comp
L Diode:BAT54S D6
U 1 1 5FF9199F
P 3100 7100
AR Path="/5FF144B4/5FF9199F" Ref="D6"  Part="1" 
AR Path="/5F956CA5/5FF9199F" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF9199F" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF9199F" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF9199F" Ref="D14"  Part="1" 
F 0 "D14" V 3146 7188 50  0000 L CNN
F 1 "BAT54S" V 3055 7188 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 7225 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 7100 50  0001 C CNN
F 4 "BAT54S,215" H 3100 7100 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 7100 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 7100 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 7100 50  0001 C CNN "RS"
	1    3100 7100
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R22
U 1 1 5FF919AB
P 3700 7100
AR Path="/5FF144B4/5FF919AB" Ref="R22"  Part="1" 
AR Path="/5F956CA5/5FF919AB" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF919AB" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF919AB" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF919AB" Ref="R48"  Part="1" 
F 0 "R48" V 3650 7250 50  0000 C CNN
F 1 "1k" V 3650 6950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 7100 50  0001 C CNN
F 3 "~" H 3700 7100 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 7100 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 7100 50  0001 C CNN "RS"
	1    3700 7100
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 7450 3100 7400
Wire Wire Line
	2800 7100 2900 7100
Wire Wire Line
	2900 7100 3600 7100
Connection ~ 2900 7100
Wire Wire Line
	3800 7100 4000 7100
Wire Wire Line
	2450 7100 2600 7100
Wire Wire Line
	2450 6200 2450 7100
Wire Wire Line
	5000 4850 5200 4850
Wire Wire Line
	5000 6000 5200 6000
Wire Wire Line
	5000 7100 5200 7100
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U10
U 1 1 5FF919F9
P 4550 7100
AR Path="/5FF144B4/5FF919F9" Ref="U10"  Part="1" 
AR Path="/5F956CA5/5FF919F9" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FF919F9" Ref="U?"  Part="1" 
AR Path="/5FF919F9" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FF919F9" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FF919F9" Ref="U17"  Part="1" 
F 0 "U17" H 4650 7250 50  0000 L CNN
F 1 "74AHC1G14" H 4600 7000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 6965 50  0001 C CNN
F 3 "" H 4695 7215 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 7100 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 7100 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 7100 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 7100 50  0001 C CNN "RS"
	1    4550 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3750 5200 3750
$Comp
L Connector:TestPoint TP9
U 1 1 5FF91A03
P 5200 3600
AR Path="/5FF144B4/5FF91A03" Ref="TP9"  Part="1" 
AR Path="/5F956CA5/5FF91A03" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A03" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A03" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A03" Ref="TP24"  Part="1" 
F 0 "TP24" H 5258 3718 50  0000 L CNN
F 1 "red" H 5258 3627 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 5400 3600 50  0001 C CNN
F 3 "~" H 5400 3600 50  0001 C CNN
F 4 "5000" H 5200 3600 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 5200 3600 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 3600 50  0001 C CNN "FP checked"
	1    5200 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3600 5200 3750
$Comp
L Connector:TestPoint TP10
U 1 1 5FF91A0A
P 5200 4700
AR Path="/5FF144B4/5FF91A0A" Ref="TP10"  Part="1" 
AR Path="/5F956CA5/5FF91A0A" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A0A" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A0A" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A0A" Ref="TP25"  Part="1" 
F 0 "TP25" H 5258 4818 50  0000 L CNN
F 1 "black" H 5258 4727 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 5400 4700 50  0001 C CNN
F 3 "~" H 5400 4700 50  0001 C CNN
F 4 "5001" H 5200 4700 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 5200 4700 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 4700 50  0001 C CNN "FP checked"
	1    5200 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5FF91A10
P 5200 5850
AR Path="/5FF144B4/5FF91A10" Ref="TP11"  Part="1" 
AR Path="/5F956CA5/5FF91A10" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A10" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A10" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A10" Ref="TP26"  Part="1" 
F 0 "TP26" H 5258 5968 50  0000 L CNN
F 1 "red" H 5258 5877 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 5400 5850 50  0001 C CNN
F 3 "~" H 5400 5850 50  0001 C CNN
F 4 "5000" H 5200 5850 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 5200 5850 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 5850 50  0001 C CNN "FP checked"
	1    5200 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5FF91A16
P 5200 6950
AR Path="/5FF144B4/5FF91A16" Ref="TP12"  Part="1" 
AR Path="/5F956CA5/5FF91A16" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A16" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A16" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A16" Ref="TP27"  Part="1" 
F 0 "TP27" H 5258 7068 50  0000 L CNN
F 1 "black" H 5258 6977 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 5400 6950 50  0001 C CNN
F 3 "~" H 5400 6950 50  0001 C CNN
F 4 "5001" H 5200 6950 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 5200 6950 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 6950 50  0001 C CNN "FP checked"
	1    5200 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4700 5200 4850
Wire Wire Line
	5200 5850 5200 6000
Wire Wire Line
	5200 6950 5200 7100
$Comp
L Connector:TestPoint TP3
U 1 1 5FF91A25
P 2050 3700
AR Path="/5FF144B4/5FF91A25" Ref="TP3"  Part="1" 
AR Path="/5F956CA5/5FF91A25" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A25" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A25" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A25" Ref="TP18"  Part="1" 
F 0 "TP18" H 2108 3818 50  0000 L CNN
F 1 "red" H 2108 3727 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 2250 3700 50  0001 C CNN
F 3 "~" H 2250 3700 50  0001 C CNN
F 4 "5000" H 2050 3700 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 2050 3700 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 3700 50  0001 C CNN "FP checked"
	1    2050 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3700 2050 3750
Connection ~ 2050 3750
Wire Wire Line
	2050 3750 2600 3750
$Comp
L Connector:TestPoint TP4
U 1 1 5FF91A2E
P 2050 4000
AR Path="/5FF144B4/5FF91A2E" Ref="TP4"  Part="1" 
AR Path="/5F956CA5/5FF91A2E" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A2E" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A2E" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A2E" Ref="TP19"  Part="1" 
F 0 "TP19" H 1992 4026 50  0000 R CNN
F 1 "black" H 1992 4117 50  0000 R CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 2250 4000 50  0001 C CNN
F 3 "~" H 2250 4000 50  0001 C CNN
F 4 "5001" H 2050 4000 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 2050 4000 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 4000 50  0001 C CNN "FP checked"
	1    2050 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 4000 2050 3950
Connection ~ 2050 3950
Wire Wire Line
	2050 3950 2450 3950
$Comp
L Connector:TestPoint TP6
U 1 1 5FF91A37
P 2050 6250
AR Path="/5FF144B4/5FF91A37" Ref="TP6"  Part="1" 
AR Path="/5F956CA5/5FF91A37" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A37" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A37" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A37" Ref="TP21"  Part="1" 
F 0 "TP21" H 1992 6276 50  0000 R CNN
F 1 "black" H 1992 6367 50  0000 R CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 2250 6250 50  0001 C CNN
F 3 "~" H 2250 6250 50  0001 C CNN
F 4 "5001" H 2050 6250 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 2050 6250 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 6250 50  0001 C CNN "FP checked"
	1    2050 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 6200 2050 6250
Connection ~ 2050 6200
Wire Wire Line
	2050 6200 2450 6200
$Comp
L Connector:TestPoint TP5
U 1 1 5FF91A40
P 2050 5950
AR Path="/5FF144B4/5FF91A40" Ref="TP5"  Part="1" 
AR Path="/5F956CA5/5FF91A40" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91A40" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91A40" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91A40" Ref="TP20"  Part="1" 
F 0 "TP20" H 2108 6068 50  0000 L CNN
F 1 "red" H 2108 5977 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 2250 5950 50  0001 C CNN
F 3 "~" H 2250 5950 50  0001 C CNN
F 4 "5000" H 2050 5950 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 2050 5950 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 5950 50  0001 C CNN "FP checked"
	1    2050 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5950 2050 6000
Connection ~ 2050 6000
Wire Wire Line
	2050 6000 2600 6000
Text Label 5250 3750 0    50   ~ 0
DIFF_1_3V3
Text Label 5250 4850 0    50   ~ 0
~DIFF_1_3V3
Text Label 5250 6000 0    50   ~ 0
DIFF_2_3V3
Text Label 5250 7100 0    50   ~ 0
~DIFF_2_3V3
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U5
U 1 1 5FF91A87
P 4550 1550
AR Path="/5FF144B4/5FF91A87" Ref="U5"  Part="1" 
AR Path="/5F956CA5/5FF91A87" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FF91A87" Ref="U?"  Part="1" 
AR Path="/5FF91A87" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FF91A87" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FF91A87" Ref="U12"  Part="1" 
F 0 "U12" H 4650 1700 50  0000 L CNN
F 1 "74AHC1G14" H 4600 1450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 1415 50  0001 C CNN
F 3 "" H 4695 1665 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 1550 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 1550 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 1550 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 1550 50  0001 C CNN "RS"
	1    4550 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5FF91A8D
P 4500 1900
AR Path="/5FF144B4/5FF91A8D" Ref="#PWR039"  Part="1" 
AR Path="/5F956CA5/5FF91A8D" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91A8D" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91A8D" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91A8D" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 4500 1650 50  0001 C CNN
F 1 "GND" H 4505 1727 50  0001 C CNN
F 2 "" H 4500 1900 50  0001 C CNN
F 3 "" H 4500 1900 50  0001 C CNN
	1    4500 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1900 4500 1750
Wire Wire Line
	4500 1200 4500 1250
$Comp
L Diode:BAT54S D1
U 1 1 5FF91A95
P 3100 1550
AR Path="/5FF144B4/5FF91A95" Ref="D1"  Part="1" 
AR Path="/5F956CA5/5FF91A95" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF91A95" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF91A95" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF91A95" Ref="D9"  Part="1" 
F 0 "D9" V 3146 1638 50  0000 L CNN
F 1 "BAT54S" V 3055 1638 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 1675 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 1550 50  0001 C CNN
F 4 "BAT54S,215" H 3100 1550 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 1550 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 1550 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 1550 50  0001 C CNN "RS"
	1    3100 1550
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5FF91A9B
P 2700 1550
AR Path="/5FF144B4/5FF91A9B" Ref="R11"  Part="1" 
AR Path="/5F956CA5/5FF91A9B" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91A9B" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91A9B" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91A9B" Ref="R37"  Part="1" 
F 0 "R37" V 2650 1700 50  0000 C CNN
F 1 "10K" V 2650 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 1550 50  0001 C CNN
F 3 "~" H 2700 1550 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 1550 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 1550 50  0001 C CNN "Mouser"
	1    2700 1550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 5FF91AA1
P 3700 1550
AR Path="/5FF144B4/5FF91AA1" Ref="R17"  Part="1" 
AR Path="/5F956CA5/5FF91AA1" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91AA1" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91AA1" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91AA1" Ref="R43"  Part="1" 
F 0 "R43" V 3650 1700 50  0000 C CNN
F 1 "1k" V 3650 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 1550 50  0001 C CNN
F 3 "~" H 3700 1550 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 1550 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 1550 50  0001 C CNN "RS"
	1    3700 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 1200 3100 1250
Wire Wire Line
	3100 1900 3100 1850
Wire Wire Line
	2800 1550 2900 1550
Wire Wire Line
	2900 1550 3600 1550
Connection ~ 2900 1550
Wire Wire Line
	3800 1550 4000 1550
$Comp
L Device:R_Small R5
U 1 1 5FF91AAD
P 1350 2200
AR Path="/5FF144B4/5FF91AAD" Ref="R5"  Part="1" 
AR Path="/5F956CA5/5FF91AAD" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91AAD" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91AAD" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91AAD" Ref="R31"  Part="1" 
F 0 "R31" H 1291 2154 50  0000 R CNN
F 1 "75R" H 1291 2245 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1350 2200 50  0001 C CNN
F 3 "~" H 1350 2200 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1350 2200 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1350 2200 50  0001 C CNN "RS"
	1    1350 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 1750 1350 1850
$Comp
L power:GND #PWR040
U 1 1 5FF91ABA
P 4500 3000
AR Path="/5FF144B4/5FF91ABA" Ref="#PWR040"  Part="1" 
AR Path="/5F956CA5/5FF91ABA" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91ABA" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91ABA" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91ABA" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 4500 2750 50  0001 C CNN
F 1 "GND" H 4505 2827 50  0001 C CNN
F 2 "" H 4500 3000 50  0001 C CNN
F 3 "" H 4500 3000 50  0001 C CNN
	1    4500 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3000 4500 2850
$Comp
L Diode:BAT54S D2
U 1 1 5FF91AC1
P 3100 2650
AR Path="/5FF144B4/5FF91AC1" Ref="D2"  Part="1" 
AR Path="/5F956CA5/5FF91AC1" Ref="D?"  Part="1" 
AR Path="/5F9C92FC/5FF91AC1" Ref="D?"  Part="1" 
AR Path="/5FC1767E/5FF91AC1" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FF91AC1" Ref="D10"  Part="1" 
F 0 "D10" V 3146 2738 50  0000 L CNN
F 1 "BAT54S" V 3055 2738 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3175 2775 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds11005.pdf" H 2980 2650 50  0001 C CNN
F 4 "BAT54S,215" H 3100 2650 50  0001 C CNN "Manufacturer"
F 5 "771-BAT54S-T/R " H 3100 2650 50  0001 C CNN "Mouser"
F 6 "ok" H 3100 2650 50  0001 C CNN "FP checked"
F 7 "544-4584" H 3100 2650 50  0001 C CNN "RS"
	1    3100 2650
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5FF91AC7
P 2700 2650
AR Path="/5FF144B4/5FF91AC7" Ref="R12"  Part="1" 
AR Path="/5F956CA5/5FF91AC7" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91AC7" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91AC7" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91AC7" Ref="R38"  Part="1" 
F 0 "R38" V 2650 2800 50  0000 C CNN
F 1 "10K" V 2650 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 2650 50  0001 C CNN
F 3 "~" H 2700 2650 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 2650 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 2650 50  0001 C CNN "Mouser"
	1    2700 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5FF91ACD
P 3700 2650
AR Path="/5FF144B4/5FF91ACD" Ref="R18"  Part="1" 
AR Path="/5F956CA5/5FF91ACD" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91ACD" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91ACD" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91ACD" Ref="R44"  Part="1" 
F 0 "R44" V 3650 2800 50  0000 C CNN
F 1 "1k" V 3650 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 2650 50  0001 C CNN
F 3 "~" H 3700 2650 50  0001 C CNN
F 4 "CRG0603F1K0" H 3700 2650 50  0001 C CNN "Manufacturer"
F 5 "213-2266" H 3700 2650 50  0001 C CNN "RS"
	1    3700 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 2300 3100 2350
Wire Wire Line
	3100 3000 3100 2950
Wire Wire Line
	2800 2650 2900 2650
Wire Wire Line
	2900 2650 3600 2650
Connection ~ 2900 2650
Wire Wire Line
	3800 2650 4000 2650
Connection ~ 1650 1550
Wire Wire Line
	1650 1550 2050 1550
Wire Wire Line
	1650 1550 1650 1850
Wire Wire Line
	1000 1550 1650 1550
$Comp
L Device:R_Small R8
U 1 1 5FF91ADD
P 1650 2200
AR Path="/5FF144B4/5FF91ADD" Ref="R8"  Part="1" 
AR Path="/5F956CA5/5FF91ADD" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF91ADD" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF91ADD" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF91ADD" Ref="R34"  Part="1" 
F 0 "R34" H 1591 2154 50  0000 R CNN
F 1 "75R" H 1591 2245 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1650 2200 50  0001 C CNN
F 3 "~" H 1650 2200 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1650 2200 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1650 2200 50  0001 C CNN "RS"
	1    1650 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1000 1750 1350 1750
Wire Wire Line
	2450 2650 2600 2650
Connection ~ 1350 1750
Wire Wire Line
	1350 1750 2050 1750
Wire Wire Line
	2450 1750 2450 2650
Wire Wire Line
	4700 1250 4500 1250
$Comp
L power:GND #PWR045
U 1 1 5FF91AEC
P 5000 1300
AR Path="/5FF144B4/5FF91AEC" Ref="#PWR045"  Part="1" 
AR Path="/5F956CA5/5FF91AEC" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91AEC" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91AEC" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91AEC" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 5000 1050 50  0001 C CNN
F 1 "GND" H 5005 1127 50  0001 C CNN
F 2 "" H 5000 1300 50  0001 C CNN
F 3 "" H 5000 1300 50  0001 C CNN
	1    5000 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1250 5000 1250
Wire Wire Line
	5000 1250 5000 1300
$Comp
L Device:C_Small C7
U 1 1 5FF91AF4
P 4800 1250
AR Path="/5FF144B4/5FF91AF4" Ref="C7"  Part="1" 
AR Path="/5F956CA5/5FF91AF4" Ref="C?"  Part="1" 
AR Path="/5F9C92FC/5FF91AF4" Ref="C?"  Part="1" 
AR Path="/5FC1767E/5FF91AF4" Ref="C?"  Part="1" 
AR Path="/5FC76325/5FF91AF4" Ref="C10"  Part="1" 
F 0 "C10" V 4571 1250 50  0000 C CNN
F 1 "100nF" V 4662 1250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4800 1250 50  0001 C CNN
F 3 "~" H 4800 1250 50  0001 C CNN
F 4 "06035C104K4T2A" H 4800 1250 50  0001 C CNN "Manufacturer"
F 5 "698-3251" H 4800 1250 50  0001 C CNN "RS"
	1    4800 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 1550 5200 1550
$Comp
L Connector:TestPoint TP7
U 1 1 5FF91B14
P 5200 1400
AR Path="/5FF144B4/5FF91B14" Ref="TP7"  Part="1" 
AR Path="/5F956CA5/5FF91B14" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B14" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B14" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B14" Ref="TP22"  Part="1" 
F 0 "TP22" H 5258 1518 50  0000 L CNN
F 1 "red" H 5258 1427 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 5400 1400 50  0001 C CNN
F 3 "~" H 5400 1400 50  0001 C CNN
F 4 "5000" H 5200 1400 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 5200 1400 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 1400 50  0001 C CNN "FP checked"
	1    5200 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1400 5200 1550
$Comp
L Connector:TestPoint TP8
U 1 1 5FF91B1C
P 5200 2500
AR Path="/5FF144B4/5FF91B1C" Ref="TP8"  Part="1" 
AR Path="/5F956CA5/5FF91B1C" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B1C" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B1C" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B1C" Ref="TP23"  Part="1" 
F 0 "TP23" H 5258 2618 50  0000 L CNN
F 1 "black" H 5258 2527 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 5400 2500 50  0001 C CNN
F 3 "~" H 5400 2500 50  0001 C CNN
F 4 "5001" H 5200 2500 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 5200 2500 50  0001 C CNN "Mouser"
F 6 "ok" H 5200 2500 50  0001 C CNN "FP checked"
	1    5200 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2500 5200 2650
$Comp
L Connector:TestPoint TP1
U 1 1 5FF91B24
P 2050 1500
AR Path="/5FF144B4/5FF91B24" Ref="TP1"  Part="1" 
AR Path="/5F956CA5/5FF91B24" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B24" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B24" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B24" Ref="TP16"  Part="1" 
F 0 "TP16" H 2108 1618 50  0000 L CNN
F 1 "red" H 2108 1527 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5000_red" H 2250 1500 50  0001 C CNN
F 3 "~" H 2250 1500 50  0001 C CNN
F 4 "5000" H 2050 1500 50  0001 C CNN "Manufacturer"
F 5 "534-5000 " H 2050 1500 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 1500 50  0001 C CNN "FP checked"
	1    2050 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1500 2050 1550
Connection ~ 2050 1550
Wire Wire Line
	2050 1550 2600 1550
$Comp
L Connector:TestPoint TP2
U 1 1 5FF91B2D
P 2050 1800
AR Path="/5FF144B4/5FF91B2D" Ref="TP2"  Part="1" 
AR Path="/5F956CA5/5FF91B2D" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B2D" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B2D" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B2D" Ref="TP17"  Part="1" 
F 0 "TP17" H 1992 1826 50  0000 R CNN
F 1 "black" H 1992 1917 50  0000 R CNN
F 2 "ak:TestPoint_Keystone_5001_black" H 2250 1800 50  0001 C CNN
F 3 "~" H 2250 1800 50  0001 C CNN
F 4 "5001" H 2050 1800 50  0001 C CNN "Manufacturer"
F 5 "534-5001" H 2050 1800 50  0001 C CNN "Mouser"
F 6 "ok" H 2050 1800 50  0001 C CNN "FP checked"
	1    2050 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 1800 2050 1750
Connection ~ 2050 1750
Wire Wire Line
	2050 1750 2450 1750
Wire Wire Line
	1650 2350 1650 2300
Wire Wire Line
	1350 2350 1350 2300
Text Label 5250 1550 0    50   ~ 0
DIFF_0_3V3
Text Label 5250 2650 0    50   ~ 0
~DIFF_0_3V3
Wire Wire Line
	6250 2250 6250 7100
Wire Wire Line
	6250 2250 6850 2250
Wire Wire Line
	6150 2150 6150 6000
Wire Wire Line
	6150 2150 6850 2150
Wire Wire Line
	7950 2150 8350 2150
Connection ~ 7950 2150
Wire Wire Line
	7950 2100 7950 2150
$Comp
L Connector:TestPoint TP15
U 1 1 5FF91B69
P 7950 2100
AR Path="/5FF144B4/5FF91B69" Ref="TP15"  Part="1" 
AR Path="/5F956CA5/5FF91B69" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B69" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B69" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B69" Ref="TP30"  Part="1" 
F 0 "TP30" H 8008 2218 50  0000 L CNN
F 1 "white" H 8008 2127 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5002_white" H 8150 2100 50  0001 C CNN
F 3 "~" H 8150 2100 50  0001 C CNN
F 4 "5002" H 7950 2100 50  0001 C CNN "Manufacturer"
F 5 "534-5002" H 7950 2100 50  0001 C CNN "Mouser"
F 6 "ok" H 7950 2100 50  0001 C CNN "FP checked"
	1    7950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1850 8350 1850
Connection ~ 7950 1850
Wire Wire Line
	7950 1800 7950 1850
$Comp
L Connector:TestPoint TP14
U 1 1 5FF91B72
P 7950 1800
AR Path="/5FF144B4/5FF91B72" Ref="TP14"  Part="1" 
AR Path="/5F956CA5/5FF91B72" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91B72" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91B72" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91B72" Ref="TP29"  Part="1" 
F 0 "TP29" H 8008 1918 50  0000 L CNN
F 1 "white" H 8008 1827 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5002_white" H 8150 1800 50  0001 C CNN
F 3 "~" H 8150 1800 50  0001 C CNN
F 4 "5002" H 7950 1800 50  0001 C CNN "Manufacturer"
F 5 "534-5002" H 7950 1800 50  0001 C CNN "Mouser"
F 6 "ok" H 7950 1800 50  0001 C CNN "FP checked"
	1    7950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2150 7950 2150
Wire Wire Line
	7750 1850 7950 1850
Wire Wire Line
	6800 2850 6850 2850
$Comp
L power:GND #PWR046
U 1 1 5FF91B7C
P 6800 3300
AR Path="/5FF144B4/5FF91B7C" Ref="#PWR046"  Part="1" 
AR Path="/5F956CA5/5FF91B7C" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91B7C" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91B7C" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91B7C" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 6800 3050 50  0001 C CNN
F 1 "GND" H 6805 3127 50  0001 C CNN
F 2 "" H 6800 3300 50  0001 C CNN
F 3 "" H 6800 3300 50  0001 C CNN
	1    6800 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR047
U 1 1 5FF91B89
P 7300 1050
AR Path="/5FF144B4/5FF91B89" Ref="#PWR047"  Part="1" 
AR Path="/5F956CA5/5FF91B89" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91B89" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91B89" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91B89" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 7300 900 50  0001 C CNN
F 1 "+3V3" H 7315 1223 50  0000 C CNN
F 2 "" H 7300 1050 50  0001 C CNN
F 3 "" H 7300 1050 50  0001 C CNN
	1    7300 1050
	1    0    0    -1  
$EndComp
$Comp
L WW-Steuerpult-Anzeige-rescue:AM26LS32-ak U11
U 1 1 5FF91B8F
P 7300 1350
AR Path="/5FF144B4/5FF91B8F" Ref="U11"  Part="1" 
AR Path="/5F956CA5/5FF91B8F" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FF91B8F" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FF91B8F" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FF91B8F" Ref="U18"  Part="1" 
F 0 "U18" H 7450 1400 50  0000 C CNN
F 1 "AM26LV32E" H 7550 -400 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 8050 1500 50  0001 C CNN
F 3 "" H 8050 1500 50  0001 C CNN
F 4 "AM26LV32EIPWR" H 7300 1350 50  0001 C CNN "Manufacturer"
F 5 "595-AM26LV32EIPWR" H 7300 1350 50  0001 C CNN "Mouser"
F 6 "ok" H 7300 1350 50  0001 C CNN "FP checked"
F 7 "162-6489" H 7300 1350 50  0001 C CNN "RS"
	1    7300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5FF91B95
P 7500 1100
AR Path="/5FF144B4/5FF91B95" Ref="C8"  Part="1" 
AR Path="/5F956CA5/5FF91B95" Ref="C?"  Part="1" 
AR Path="/5F9C92FC/5FF91B95" Ref="C?"  Part="1" 
AR Path="/5FC1767E/5FF91B95" Ref="C?"  Part="1" 
AR Path="/5FC76325/5FF91B95" Ref="C11"  Part="1" 
F 0 "C11" V 7271 1100 50  0000 C CNN
F 1 "100nF" V 7362 1100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7500 1100 50  0001 C CNN
F 3 "~" H 7500 1100 50  0001 C CNN
F 4 "06035C104K4T2A" H 7500 1100 50  0001 C CNN "Manufacturer"
F 5 "698-3251" H 7500 1100 50  0001 C CNN "RS"
	1    7500 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 1100 7650 1150
Wire Wire Line
	7600 1100 7650 1100
$Comp
L power:GND #PWR049
U 1 1 5FF91B9D
P 7650 1150
AR Path="/5FF144B4/5FF91B9D" Ref="#PWR049"  Part="1" 
AR Path="/5F956CA5/5FF91B9D" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91B9D" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91B9D" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91B9D" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 7650 900 50  0001 C CNN
F 1 "GND" H 7655 977 50  0001 C CNN
F 2 "" H 7650 1150 50  0001 C CNN
F 3 "" H 7650 1150 50  0001 C CNN
	1    7650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1100 7300 1150
Connection ~ 7300 1100
Wire Wire Line
	7400 1100 7300 1100
Wire Wire Line
	7300 3300 7300 3250
$Comp
L power:GND #PWR048
U 1 1 5FF91BA7
P 7300 3300
AR Path="/5FF144B4/5FF91BA7" Ref="#PWR048"  Part="1" 
AR Path="/5F956CA5/5FF91BA7" Ref="#PWR?"  Part="1" 
AR Path="/5F9C92FC/5FF91BA7" Ref="#PWR?"  Part="1" 
AR Path="/5FC1767E/5FF91BA7" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FF91BA7" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 7300 3050 50  0001 C CNN
F 1 "GND" H 7305 3127 50  0001 C CNN
F 2 "" H 7300 3300 50  0001 C CNN
F 3 "" H 7300 3300 50  0001 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1050 7300 1100
NoConn ~ 6850 2450
NoConn ~ 6850 2550
Wire Wire Line
	7950 1550 8350 1550
Connection ~ 7950 1550
Wire Wire Line
	7950 1500 7950 1550
$Comp
L Connector:TestPoint TP13
U 1 1 5FF91BB5
P 7950 1500
AR Path="/5FF144B4/5FF91BB5" Ref="TP13"  Part="1" 
AR Path="/5F956CA5/5FF91BB5" Ref="TP?"  Part="1" 
AR Path="/5F9C92FC/5FF91BB5" Ref="TP?"  Part="1" 
AR Path="/5FC1767E/5FF91BB5" Ref="TP?"  Part="1" 
AR Path="/5FC76325/5FF91BB5" Ref="TP28"  Part="1" 
F 0 "TP28" H 8008 1618 50  0000 L CNN
F 1 "white" H 8008 1527 50  0000 L CNN
F 2 "ak:TestPoint_Keystone_5002_white" H 8150 1500 50  0001 C CNN
F 3 "~" H 8150 1500 50  0001 C CNN
F 4 "5002" H 7950 1500 50  0001 C CNN "Manufacturer"
F 5 "534-5002" H 7950 1500 50  0001 C CNN "Mouser"
F 6 "ok" H 7950 1500 50  0001 C CNN "FP checked"
	1    7950 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1550 7950 1550
NoConn ~ 7750 2450
Wire Wire Line
	3100 1200 3050 1200
Wire Wire Line
	3100 2300 3050 2300
Text HLabel 8350 2150 2    50   Output ~ 0
SE_2
Text GLabel 3050 2300 0    50   Input ~ 0
Vclamp
Text GLabel 3050 1200 0    50   Input ~ 0
Vclamp
Text HLabel 1000 1550 0    50   Input ~ 0
DIFF_0
Text HLabel 1000 1750 0    50   Input ~ 0
~DIFF_0
Text HLabel 1000 3750 0    50   Input ~ 0
DIFF_1
Text HLabel 1000 3950 0    50   Input ~ 0
~DIFF_1
Text HLabel 1000 6000 0    50   Input ~ 0
DIFF_2
Text HLabel 1000 6200 0    50   Input ~ 0
~DIFF_2
Text GLabel 4450 1200 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 1200 4500 1200
Text GLabel 4450 2300 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 2300 4500 2300
Wire Wire Line
	3100 3400 3100 3450
Wire Wire Line
	3100 3400 3050 3400
Text GLabel 3050 3400 0    50   Input ~ 0
Vclamp
Wire Wire Line
	4500 3400 4500 3550
Text GLabel 4450 3400 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 3400 4500 3400
Wire Wire Line
	3100 4500 3100 4550
Wire Wire Line
	3100 4500 3050 4500
Text GLabel 3050 4500 0    50   Input ~ 0
Vclamp
Wire Wire Line
	4500 4500 4500 4650
Text GLabel 4450 4500 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 4500 4500 4500
Wire Wire Line
	3100 5650 3100 5700
Wire Wire Line
	3100 5650 3050 5650
Text GLabel 3050 5650 0    50   Input ~ 0
Vclamp
Text GLabel 4450 5650 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 5650 4500 5650
Wire Wire Line
	3100 6750 3100 6800
Wire Wire Line
	3100 6750 3050 6750
Text GLabel 3050 6750 0    50   Input ~ 0
Vclamp
Wire Wire Line
	4500 6750 4500 6900
Text GLabel 4450 6750 0    50   Input ~ 0
Vschmitt
Wire Wire Line
	4450 6750 4500 6750
$Comp
L Device:D_Small D?
U 1 1 5FA78704
P 9900 1350
AR Path="/5FA78704" Ref="D?"  Part="1" 
AR Path="/5F9BBF68/5FA78704" Ref="D?"  Part="1" 
AR Path="/5FF144B4/5FA78704" Ref="D7"  Part="1" 
AR Path="/5FC1767E/5FA78704" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FA78704" Ref="D15"  Part="1" 
F 0 "D15" H 9900 1145 50  0000 C CNN
F 1 "S1M" H 9900 1236 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" V 9900 1350 50  0001 C CNN
F 3 "~" V 9900 1350 50  0001 C CNN
F 4 "512-S1M " H 9900 1350 50  0001 C CNN "Mouser"
F 5 "S1M" H 9900 1350 50  0001 C CNN "Manufacturer"
F 6 "ok" H 9900 1350 50  0001 C CNN "FP checked"
F 7 "670-8836" H 9900 1350 50  0001 C CNN "RS"
	1    9900 1350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5FA7870A
P 9650 1350
AR Path="/5FA7870A" Ref="R?"  Part="1" 
AR Path="/5F9BBF68/5FA7870A" Ref="R?"  Part="1" 
AR Path="/5FF144B4/5FA7870A" Ref="R24"  Part="1" 
AR Path="/5FC1767E/5FA7870A" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA7870A" Ref="R50"  Part="1" 
F 0 "R50" V 9454 1350 50  0000 C CNN
F 1 "22R" V 9545 1350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 9650 1350 50  0001 C CNN
F 3 "~" H 9650 1350 50  0001 C CNN
F 4 "CRGP0805F22R" H 9650 1350 50  0001 C CNN "Manufacturer"
F 5 "176-4897" H 9650 1350 50  0001 C CNN "RS"
	1    9650 1350
	0    1    1    0   
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB?
U 1 1 5FA78712
P 9200 1350
AR Path="/5FA78712" Ref="FB?"  Part="1" 
AR Path="/5F9BBF68/5FA78712" Ref="FB?"  Part="1" 
AR Path="/5FF144B4/5FA78712" Ref="FB2"  Part="1" 
AR Path="/5FC1767E/5FA78712" Ref="FB?"  Part="1" 
AR Path="/5FC76325/5FA78712" Ref="FB4"  Part="1" 
F 0 "FB4" V 8963 1350 50  0000 C CNN
F 1 "1k @ 100MHz" V 9054 1350 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 9130 1350 50  0001 C CNN
F 3 "~" H 9200 1350 50  0001 C CNN
F 4 "81-BLM21SP102SH1D" V 9200 1350 50  0001 C CNN "Mouser"
F 5 "BLM21SP102SH1D " H 9200 1350 50  0001 C CNN "Manufacturer"
F 6 "ok" H 9200 1350 50  0001 C CNN "FP checked"
F 7 "230-3834" H 9200 1350 50  0001 C CNN "RS"
	1    9200 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	8900 1250 8900 1350
Wire Wire Line
	8900 1350 9100 1350
Wire Wire Line
	9300 1350 9500 1350
Wire Wire Line
	9750 1350 9800 1350
Wire Wire Line
	10000 1350 10100 1350
Wire Wire Line
	10100 1350 10100 1450
Connection ~ 10100 1350
$Comp
L Device:C_Small C?
U 1 1 5FA78721
P 10400 1550
AR Path="/5FA78721" Ref="C?"  Part="1" 
AR Path="/5F9BBF68/5FA78721" Ref="C?"  Part="1" 
AR Path="/5FF144B4/5FA78721" Ref="C9"  Part="1" 
AR Path="/5FC1767E/5FA78721" Ref="C?"  Part="1" 
AR Path="/5FC76325/5FA78721" Ref="C12"  Part="1" 
F 0 "C12" H 10308 1504 50  0000 R CNN
F 1 "10nF, Safety" H 10308 1595 50  0000 R CNN
F 2 "Capacitor_SMD:C_2220_5650Metric" H 10400 1550 50  0001 C CNN
F 3 "~" H 10400 1550 50  0001 C CNN
F 4 "77-VJ2220Y103KXUSTX2" H 10400 1550 50  0001 C CNN "Mouser"
F 5 "VJ2220Y103KXUSTX2 " H 10400 1550 50  0001 C CNN "Manufacturer"
F 6 "ok" H 10400 1550 50  0001 C CNN "FP checked"
F 7 "880-7004" H 10400 1550 50  0001 C CNN "RS"
	1    10400 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	10100 1350 10400 1350
Wire Wire Line
	10400 1350 10400 1450
Wire Wire Line
	10400 1650 10400 1750
Wire Wire Line
	10400 1750 10100 1750
Wire Wire Line
	10100 1750 10100 1650
Wire Wire Line
	10100 1750 9250 1750
Connection ~ 10100 1750
Wire Wire Line
	9050 1750 8900 1750
Wire Wire Line
	10400 1850 10400 1750
Connection ~ 10400 1750
Connection ~ 10400 1350
$Comp
L power:GND #PWR?
U 1 1 5FA78738
P 8900 1850
AR Path="/5FA78738" Ref="#PWR?"  Part="1" 
AR Path="/5F9BBF68/5FA78738" Ref="#PWR?"  Part="1" 
AR Path="/5FF144B4/5FA78738" Ref="#PWR051"  Part="1" 
AR Path="/5FC1767E/5FA78738" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FA78738" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 8900 1600 50  0001 C CNN
F 1 "GND" H 8905 1677 50  0001 C CNN
F 2 "" H 8900 1850 50  0001 C CNN
F 3 "" H 8900 1850 50  0001 C CNN
	1    8900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1850 8900 1750
Wire Wire Line
	9500 1350 9500 1000
Connection ~ 9500 1350
Wire Wire Line
	9500 1350 9550 1350
Wire Wire Line
	9500 1000 9550 1000
Wire Wire Line
	10400 1350 10650 1350
Text GLabel 10650 1350 2    50   Input ~ 0
Vclamp
Text GLabel 9550 1000 2    50   Input ~ 0
Vschmitt
$Comp
L Device:Ferrite_Bead_Small FB?
U 1 1 5FA78756
P 9150 1750
AR Path="/5FA78756" Ref="FB?"  Part="1" 
AR Path="/5F9BBF68/5FA78756" Ref="FB?"  Part="1" 
AR Path="/5FF144B4/5FA78756" Ref="FB1"  Part="1" 
AR Path="/5FC1767E/5FA78756" Ref="FB?"  Part="1" 
AR Path="/5FC76325/5FA78756" Ref="FB3"  Part="1" 
F 0 "FB3" V 8913 1750 50  0000 C CNN
F 1 "1k @ 100MHz" V 9004 1750 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 9080 1750 50  0001 C CNN
F 3 "~" H 9150 1750 50  0001 C CNN
F 4 "81-BLM21SP102SH1D" V 9150 1750 50  0001 C CNN "Mouser"
F 5 "BLM21SP102SH1D " H 9150 1750 50  0001 C CNN "Manufacturer"
F 6 "ok" H 9150 1750 50  0001 C CNN "FP checked"
F 7 "230-3834" H 9150 1750 50  0001 C CNN "RS"
	1    9150 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	10900 3050 10050 3050
$Comp
L Device:R_Small R27
U 1 1 5FA88E15
P 9950 3050
AR Path="/5FF144B4/5FA88E15" Ref="R27"  Part="1" 
AR Path="/5F956CA5/5FA88E15" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA88E15" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA88E15" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA88E15" Ref="R53"  Part="1" 
F 0 "R53" V 9900 3200 50  0000 C CNN
F 1 "10k" V 9900 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 3050 50  0001 C CNN
F 3 "~" H 9950 3050 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 3050 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 3050 50  0001 C CNN "RS"
	1    9950 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9850 3050 9600 3050
Wire Wire Line
	10900 3150 10050 3150
$Comp
L Device:R_Small R28
U 1 1 5FA8FE70
P 9950 3150
AR Path="/5FF144B4/5FA8FE70" Ref="R28"  Part="1" 
AR Path="/5F956CA5/5FA8FE70" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA8FE70" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA8FE70" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA8FE70" Ref="R54"  Part="1" 
F 0 "R54" V 9900 3300 50  0000 C CNN
F 1 "10k" V 9900 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 3150 50  0001 C CNN
F 3 "~" H 9950 3150 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 3150 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 3150 50  0001 C CNN "RS"
	1    9950 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9850 3150 9600 3150
Wire Wire Line
	10900 3250 10050 3250
$Comp
L Device:R_Small R29
U 1 1 5FA97281
P 9950 3250
AR Path="/5FF144B4/5FA97281" Ref="R29"  Part="1" 
AR Path="/5F956CA5/5FA97281" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA97281" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA97281" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA97281" Ref="R55"  Part="1" 
F 0 "R55" V 9900 3400 50  0000 C CNN
F 1 "10k" V 9900 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 3250 50  0001 C CNN
F 3 "~" H 9950 3250 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 3250 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 3250 50  0001 C CNN "RS"
	1    9950 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9850 3250 9600 3250
Wire Wire Line
	10900 3350 10050 3350
$Comp
L Device:R_Small R30
U 1 1 5FA9E8D6
P 9950 3350
AR Path="/5FF144B4/5FA9E8D6" Ref="R30"  Part="1" 
AR Path="/5F956CA5/5FA9E8D6" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA9E8D6" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA9E8D6" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA9E8D6" Ref="R56"  Part="1" 
F 0 "R56" V 9900 3500 50  0000 C CNN
F 1 "10k" V 9900 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 3350 50  0001 C CNN
F 3 "~" H 9950 3350 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 3350 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 3350 50  0001 C CNN "RS"
	1    9950 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9850 3350 9600 3350
Wire Wire Line
	4000 1550 4000 2100
Connection ~ 4000 1550
Wire Wire Line
	4000 1550 4100 1550
Wire Wire Line
	4000 3200 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4100 2650
Wire Wire Line
	4000 4300 4000 3750
Connection ~ 4000 3750
Wire Wire Line
	4000 3750 4100 3750
Wire Wire Line
	4000 5400 4000 4850
Connection ~ 4000 4850
Wire Wire Line
	4000 4850 4100 4850
Connection ~ 4000 7100
Wire Wire Line
	4000 7100 4100 7100
Wire Wire Line
	4000 6550 4000 6000
Connection ~ 4000 6000
Wire Wire Line
	4000 6000 4100 6000
Connection ~ 5200 2650
Wire Wire Line
	5200 3750 5950 3750
Connection ~ 5200 3750
Wire Wire Line
	5200 4850 6050 4850
Connection ~ 5200 4850
Wire Wire Line
	5200 6000 6150 6000
Connection ~ 5200 6000
Wire Wire Line
	5200 7100 6250 7100
Connection ~ 5200 7100
$Comp
L Device:Jumper_NC_Small JP1
U 1 1 5FE529BC
P 1350 1950
AR Path="/5FF144B4/5FE529BC" Ref="JP1"  Part="1" 
AR Path="/5FC76325/5FE529BC" Ref="JP7"  Part="1" 
F 0 "JP7" V 1350 1902 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1350 2071 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1350 1950 50  0001 C CNN
F 3 "~" H 1350 1950 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1350 1950 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1350 1950 50  0001 C CNN "Mouser"
F 6 "ok" H 1350 1950 50  0001 C CNN "FP checked"
	1    1350 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 2050 1350 2100
$Comp
L Device:Jumper_NC_Small JP4
U 1 1 5FE7AC98
P 1650 1950
AR Path="/5FF144B4/5FE7AC98" Ref="JP4"  Part="1" 
AR Path="/5FC76325/5FE7AC98" Ref="JP10"  Part="1" 
F 0 "JP10" V 1650 1902 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1650 2071 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1650 1950 50  0001 C CNN
F 3 "~" H 1650 1950 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1650 1950 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1650 1950 50  0001 C CNN "Mouser"
F 6 "ok" H 1650 1950 50  0001 C CNN "FP checked"
	1    1650 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 2050 1650 2100
Wire Wire Line
	1000 3750 1650 3750
Wire Wire Line
	1000 3950 1350 3950
Wire Wire Line
	1000 6200 1350 6200
Wire Wire Line
	1000 6000 1650 6000
$Comp
L Device:R_Small R6
U 1 1 5FEA67A4
P 1350 4400
AR Path="/5FF144B4/5FEA67A4" Ref="R6"  Part="1" 
AR Path="/5F956CA5/5FEA67A4" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FEA67A4" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FEA67A4" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FEA67A4" Ref="R32"  Part="1" 
F 0 "R32" H 1291 4354 50  0000 R CNN
F 1 "75R" H 1291 4445 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1350 4400 50  0001 C CNN
F 3 "~" H 1350 4400 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1350 4400 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1350 4400 50  0001 C CNN "RS"
	1    1350 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 3950 1350 4050
Wire Wire Line
	1650 3750 1650 4050
$Comp
L Device:R_Small R9
U 1 1 5FEA67AC
P 1650 4400
AR Path="/5FF144B4/5FEA67AC" Ref="R9"  Part="1" 
AR Path="/5F956CA5/5FEA67AC" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FEA67AC" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FEA67AC" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FEA67AC" Ref="R35"  Part="1" 
F 0 "R35" H 1591 4354 50  0000 R CNN
F 1 "75R" H 1591 4445 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1650 4400 50  0001 C CNN
F 3 "~" H 1650 4400 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1650 4400 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1650 4400 50  0001 C CNN "RS"
	1    1650 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 4550 1650 4500
Wire Wire Line
	1350 4550 1350 4500
$Comp
L Device:Jumper_NC_Small JP2
U 1 1 5FEA67C0
P 1350 4150
AR Path="/5FF144B4/5FEA67C0" Ref="JP2"  Part="1" 
AR Path="/5FC76325/5FEA67C0" Ref="JP8"  Part="1" 
F 0 "JP8" V 1350 4102 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1350 4271 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1350 4150 50  0001 C CNN
F 3 "~" H 1350 4150 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1350 4150 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1350 4150 50  0001 C CNN "Mouser"
F 6 "ok" H 1350 4150 50  0001 C CNN "FP checked"
	1    1350 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 4250 1350 4300
$Comp
L Device:Jumper_NC_Small JP5
U 1 1 5FEA67C7
P 1650 4150
AR Path="/5FF144B4/5FEA67C7" Ref="JP5"  Part="1" 
AR Path="/5FC76325/5FEA67C7" Ref="JP11"  Part="1" 
F 0 "JP11" V 1650 4102 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1650 4271 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1650 4150 50  0001 C CNN
F 3 "~" H 1650 4150 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1650 4150 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1650 4150 50  0001 C CNN "Mouser"
F 6 "ok" H 1650 4150 50  0001 C CNN "FP checked"
	1    1650 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 4250 1650 4300
$Comp
L Device:R_Small R7
U 1 1 5FEB226E
P 1350 6650
AR Path="/5FF144B4/5FEB226E" Ref="R7"  Part="1" 
AR Path="/5F956CA5/5FEB226E" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FEB226E" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FEB226E" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FEB226E" Ref="R33"  Part="1" 
F 0 "R33" H 1291 6604 50  0000 R CNN
F 1 "75R" H 1291 6695 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1350 6650 50  0001 C CNN
F 3 "~" H 1350 6650 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1350 6650 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1350 6650 50  0001 C CNN "RS"
	1    1350 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 6200 1350 6300
Wire Wire Line
	1650 6000 1650 6300
$Comp
L Device:R_Small R10
U 1 1 5FEB2276
P 1650 6650
AR Path="/5FF144B4/5FEB2276" Ref="R10"  Part="1" 
AR Path="/5F956CA5/5FEB2276" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FEB2276" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FEB2276" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FEB2276" Ref="R36"  Part="1" 
F 0 "R36" H 1591 6604 50  0000 R CNN
F 1 "75R" H 1591 6695 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 1650 6650 50  0001 C CNN
F 3 "~" H 1650 6650 50  0001 C CNN
F 4 "CRCW120675R0FKEAHP" H 1650 6650 50  0001 C CNN "Manufacturer"
F 5 "812-1956" H 1650 6650 50  0001 C CNN "RS"
	1    1650 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 6800 1650 6750
Wire Wire Line
	1350 6800 1350 6750
$Comp
L Device:Jumper_NC_Small JP3
U 1 1 5FEB228A
P 1350 6400
AR Path="/5FF144B4/5FEB228A" Ref="JP3"  Part="1" 
AR Path="/5FC76325/5FEB228A" Ref="JP9"  Part="1" 
F 0 "JP9" V 1350 6352 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1350 6521 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1350 6400 50  0001 C CNN
F 3 "~" H 1350 6400 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1350 6400 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1350 6400 50  0001 C CNN "Mouser"
F 6 "ok" H 1350 6400 50  0001 C CNN "FP checked"
	1    1350 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 6500 1350 6550
$Comp
L Device:Jumper_NC_Small JP6
U 1 1 5FEB2291
P 1650 6400
AR Path="/5FF144B4/5FEB2291" Ref="JP6"  Part="1" 
AR Path="/5FC76325/5FEB2291" Ref="JP12"  Part="1" 
F 0 "JP12" V 1650 6352 50  0000 R CNN
F 1 "Jumper_NC_Small" H 1650 6521 50  0001 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 1650 6400 50  0001 C CNN
F 3 "~" H 1650 6400 50  0001 C CNN
F 4 "TMS-101-02-G-D" H 1650 6400 50  0001 C CNN "Manufacturer"
F 5 "200-TMS10102GD" H 1650 6400 50  0001 C CNN "Mouser"
F 6 "ok" H 1650 6400 50  0001 C CNN "FP checked"
	1    1650 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 6500 1650 6550
Wire Notes Line
	8700 750  11100 750 
Wire Notes Line
	11100 750  11100 2050
Wire Notes Line
	11100 2050 8700 2050
Wire Notes Line
	8700 2050 8700 750 
Text Notes 8700 700  0    98   ~ 0
Input protection
Wire Notes Line width 10
	2700 7500 2700 1000
$Comp
L Device:R_Small R16
U 1 1 5FF919A5
P 2700 7100
AR Path="/5FF144B4/5FF919A5" Ref="R16"  Part="1" 
AR Path="/5F956CA5/5FF919A5" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FF919A5" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FF919A5" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FF919A5" Ref="R42"  Part="1" 
F 0 "R42" V 2650 7250 50  0000 C CNN
F 1 "10K" V 2650 6950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 2700 7100 50  0001 C CNN
F 3 "~" H 2700 7100 50  0001 C CNN
F 4 "RCS120610K0JNEA" H 2700 7100 50  0001 C CNN "Manufacturer"
F 5 "71-RCS120610K0JNEA" H 2700 7100 50  0001 C CNN "Mouser"
	1    2700 7100
	0    1    1    0   
$EndComp
Text Notes 2450 950  0    50   ~ 0
Safety moat
Text Notes 650  3200 0    118  ~ 24
No routing underneath\nthese components!
Connection ~ 1350 3950
Wire Wire Line
	1350 3950 2050 3950
Connection ~ 1350 6200
Wire Wire Line
	1350 6200 2050 6200
Connection ~ 1650 3750
Wire Wire Line
	1650 3750 2050 3750
Connection ~ 1650 6000
Wire Wire Line
	1650 6000 2050 6000
Wire Notes Line width 10
	3700 7500 3700 1000
Text Notes 3450 950  0    50   ~ 0
Secondary moat
Wire Notes Line
	8700 2650 8700 3550
Wire Notes Line
	8700 3550 11100 3550
Wire Notes Line
	11100 3550 11100 2650
Wire Notes Line
	11100 2650 8700 2650
Text Notes 8700 2600 0    98   ~ 0
Test signal injection
Text Notes 550  700  0    98   ~ 0
Differential line receiver
Text Notes 7750 2800 0    79   ~ 0
Single-ended\noutputs
$Comp
L Device:D_Zener_Small D?
U 1 1 5FA7874D
P 10100 1550
AR Path="/5FA7874D" Ref="D?"  Part="1" 
AR Path="/5F9BBF68/5FA7874D" Ref="D?"  Part="1" 
AR Path="/5FF144B4/5FA7874D" Ref="D8"  Part="1" 
AR Path="/5FC1767E/5FA7874D" Ref="D?"  Part="1" 
AR Path="/5FC76325/5FA7874D" Ref="D16"  Part="1" 
F 0 "D16" V 10054 1618 50  0000 L CNN
F 1 "3V3" V 10145 1618 50  0000 L CNN
F 2 "Diode_SMD:D_SMB" V 10100 1550 50  0001 C CNN
F 3 "~" V 10100 1550 50  0001 C CNN
F 4 "863-1SMB5913BT3G" V 10100 1550 50  0001 C CNN "Mouser"
F 5 "1SMB5913BT3G" V 10100 1550 50  0001 C CNN "Manufacturer"
F 6 "ok" H 10100 1550 50  0001 C CNN "FP checked"
F 7 "545-3308" H 10100 1550 50  0001 C CNN "RS"
	1    10100 1550
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR050
U 1 1 5FA78745
P 8900 1250
AR Path="/5FF144B4/5FA78745" Ref="#PWR050"  Part="1" 
AR Path="/5FC1767E/5FA78745" Ref="#PWR?"  Part="1" 
AR Path="/5FC76325/5FA78745" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 8900 1100 50  0001 C CNN
F 1 "+3V3" H 8915 1423 50  0000 C CNN
F 2 "" H 8900 1250 50  0001 C CNN
F 3 "" H 8900 1250 50  0001 C CNN
	1    8900 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5650 4500 5800
Wire Wire Line
	4500 1250 4500 1350
Connection ~ 4500 1250
Wire Wire Line
	4500 2450 4500 2300
Wire Wire Line
	5000 2650 5200 2650
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U6
U 1 1 5FC7B83F
P 4550 2650
AR Path="/5FF144B4/5FC7B83F" Ref="U6"  Part="1" 
AR Path="/5F956CA5/5FC7B83F" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FC7B83F" Ref="U?"  Part="1" 
AR Path="/5FC7B83F" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FC7B83F" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FC7B83F" Ref="U13"  Part="1" 
F 0 "U13" H 4650 2800 50  0000 L CNN
F 1 "74AHC1G14" H 4600 2550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 2515 50  0001 C CNN
F 3 "" H 4695 2765 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 2650 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 2650 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 2650 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 2650 50  0001 C CNN "RS"
	1    4550 2650
	1    0    0    -1  
$EndComp
$Comp
L WW-Steuerpult-Anzeige-rescue:74AHC1G14-ak U8
U 1 1 5FC7CCAB
P 4550 4850
AR Path="/5FF144B4/5FC7CCAB" Ref="U8"  Part="1" 
AR Path="/5F956CA5/5FC7CCAB" Ref="U?"  Part="1" 
AR Path="/5F9C92FC/5FC7CCAB" Ref="U?"  Part="1" 
AR Path="/5FC7CCAB" Ref="U?"  Part="1" 
AR Path="/5FC1767E/5FC7CCAB" Ref="U?"  Part="1" 
AR Path="/5FC76325/5FC7CCAB" Ref="U15"  Part="1" 
F 0 "U15" H 4650 5000 50  0000 L CNN
F 1 "74AHC1G14" H 4600 4750 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4645 4715 50  0001 C CNN
F 3 "" H 4695 4965 50  0001 C CNN
F 4 "SN74AHC1G14DBVR" H 4550 4850 50  0001 C CNN "Manufacturer"
F 5 "595-SN74AHC1G14DBVR" H 4550 4850 50  0001 C CNN "Mouser"
F 6 "ok" H 4550 4850 50  0001 C CNN "FP checked"
F 7 "526-381" H 4550 4850 50  0001 C CNN "RS"
	1    4550 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	550  750  8650 750 
Wire Notes Line
	6500 7700 550  7700
Wire Notes Line
	550  7700 550  750 
Wire Notes Line
	8650 750  8650 3550
Wire Notes Line
	8650 3550 6500 3550
Wire Notes Line
	6500 3550 6500 7700
$Comp
L ak:SAFETYGND #PWR053
U 1 1 5FB56D45
P 1350 2350
AR Path="/5FC76325/5FB56D45" Ref="#PWR053"  Part="1" 
AR Path="/5FF144B4/5FB56D45" Ref="#PWR027"  Part="1" 
F 0 "#PWR053" H 1350 2150 50  0001 C CNN
F 1 "SAFETYGND" H 1354 2424 50  0001 C CNN
F 2 "" H 1350 2300 50  0001 C CNN
F 3 "" H 1350 2300 50  0001 C CNN
	1    1350 2350
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR056
U 1 1 5FB575D5
P 1650 2350
AR Path="/5FC76325/5FB575D5" Ref="#PWR056"  Part="1" 
AR Path="/5FF144B4/5FB575D5" Ref="#PWR030"  Part="1" 
F 0 "#PWR056" H 1650 2150 50  0001 C CNN
F 1 "SAFETYGND" H 1654 2424 50  0001 C CNN
F 2 "" H 1650 2300 50  0001 C CNN
F 3 "" H 1650 2300 50  0001 C CNN
	1    1650 2350
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR060
U 1 1 5FB5772F
P 3100 3000
AR Path="/5FC76325/5FB5772F" Ref="#PWR060"  Part="1" 
AR Path="/5FF144B4/5FB5772F" Ref="#PWR034"  Part="1" 
F 0 "#PWR060" H 3100 2800 50  0001 C CNN
F 1 "SAFETYGND" H 3104 3074 50  0001 C CNN
F 2 "" H 3100 2950 50  0001 C CNN
F 3 "" H 3100 2950 50  0001 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR059
U 1 1 5FB57C5A
P 3100 1900
AR Path="/5FC76325/5FB57C5A" Ref="#PWR059"  Part="1" 
AR Path="/5FF144B4/5FB57C5A" Ref="#PWR033"  Part="1" 
F 0 "#PWR059" H 3100 1700 50  0001 C CNN
F 1 "SAFETYGND" H 3104 1974 50  0001 C CNN
F 2 "" H 3100 1850 50  0001 C CNN
F 3 "" H 3100 1850 50  0001 C CNN
	1    3100 1900
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR054
U 1 1 5FB581A5
P 1350 4550
AR Path="/5FC76325/5FB581A5" Ref="#PWR054"  Part="1" 
AR Path="/5FF144B4/5FB581A5" Ref="#PWR028"  Part="1" 
F 0 "#PWR054" H 1350 4350 50  0001 C CNN
F 1 "SAFETYGND" H 1354 4624 50  0001 C CNN
F 2 "" H 1350 4500 50  0001 C CNN
F 3 "" H 1350 4500 50  0001 C CNN
	1    1350 4550
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR057
U 1 1 5FB58573
P 1650 4550
AR Path="/5FC76325/5FB58573" Ref="#PWR057"  Part="1" 
AR Path="/5FF144B4/5FB58573" Ref="#PWR031"  Part="1" 
F 0 "#PWR057" H 1650 4350 50  0001 C CNN
F 1 "SAFETYGND" H 1654 4624 50  0001 C CNN
F 2 "" H 1650 4500 50  0001 C CNN
F 3 "" H 1650 4500 50  0001 C CNN
	1    1650 4550
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR062
U 1 1 5FB58921
P 3100 5200
AR Path="/5FC76325/5FB58921" Ref="#PWR062"  Part="1" 
AR Path="/5FF144B4/5FB58921" Ref="#PWR036"  Part="1" 
F 0 "#PWR062" H 3100 5000 50  0001 C CNN
F 1 "SAFETYGND" H 3104 5274 50  0001 C CNN
F 2 "" H 3100 5150 50  0001 C CNN
F 3 "" H 3100 5150 50  0001 C CNN
	1    3100 5200
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR061
U 1 1 5FB58DC6
P 3100 4100
AR Path="/5FC76325/5FB58DC6" Ref="#PWR061"  Part="1" 
AR Path="/5FF144B4/5FB58DC6" Ref="#PWR035"  Part="1" 
F 0 "#PWR061" H 3100 3900 50  0001 C CNN
F 1 "SAFETYGND" H 3104 4174 50  0001 C CNN
F 2 "" H 3100 4050 50  0001 C CNN
F 3 "" H 3100 4050 50  0001 C CNN
	1    3100 4100
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR055
U 1 1 5FB5918E
P 1350 6800
AR Path="/5FC76325/5FB5918E" Ref="#PWR055"  Part="1" 
AR Path="/5FF144B4/5FB5918E" Ref="#PWR029"  Part="1" 
F 0 "#PWR055" H 1350 6600 50  0001 C CNN
F 1 "SAFETYGND" H 1354 6874 50  0001 C CNN
F 2 "" H 1350 6750 50  0001 C CNN
F 3 "" H 1350 6750 50  0001 C CNN
	1    1350 6800
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR058
U 1 1 5FB5989A
P 1650 6800
AR Path="/5FC76325/5FB5989A" Ref="#PWR058"  Part="1" 
AR Path="/5FF144B4/5FB5989A" Ref="#PWR032"  Part="1" 
F 0 "#PWR058" H 1650 6600 50  0001 C CNN
F 1 "SAFETYGND" H 1654 6874 50  0001 C CNN
F 2 "" H 1650 6750 50  0001 C CNN
F 3 "" H 1650 6750 50  0001 C CNN
	1    1650 6800
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR064
U 1 1 5FB59AB2
P 3100 7450
AR Path="/5FC76325/5FB59AB2" Ref="#PWR064"  Part="1" 
AR Path="/5FF144B4/5FB59AB2" Ref="#PWR038"  Part="1" 
F 0 "#PWR064" H 3100 7250 50  0001 C CNN
F 1 "SAFETYGND" H 3104 7524 50  0001 C CNN
F 2 "" H 3100 7400 50  0001 C CNN
F 3 "" H 3100 7400 50  0001 C CNN
	1    3100 7450
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR063
U 1 1 5FB5A01E
P 3100 6350
AR Path="/5FC76325/5FB5A01E" Ref="#PWR063"  Part="1" 
AR Path="/5FF144B4/5FB5A01E" Ref="#PWR037"  Part="1" 
F 0 "#PWR063" H 3100 6150 50  0001 C CNN
F 1 "SAFETYGND" H 3104 6424 50  0001 C CNN
F 2 "" H 3100 6300 50  0001 C CNN
F 3 "" H 3100 6300 50  0001 C CNN
	1    3100 6350
	1    0    0    -1  
$EndComp
$Comp
L ak:SAFETYGND #PWR078
U 1 1 5FB5A5E9
P 10400 1850
AR Path="/5FC76325/5FB5A5E9" Ref="#PWR078"  Part="1" 
AR Path="/5FF144B4/5FB5A5E9" Ref="#PWR052"  Part="1" 
F 0 "#PWR078" H 10400 1650 50  0001 C CNN
F 1 "SAFETYGND" H 10404 1924 50  0001 C CNN
F 2 "" H 10400 1800 50  0001 C CNN
F 3 "" H 10400 1800 50  0001 C CNN
	1    10400 1850
	1    0    0    -1  
$EndComp
Text Label 10800 3050 2    50   ~ 0
TEST_DIFF1_R
Text Label 10800 3150 2    50   ~ 0
~TEST_DIFF1_R
Text Label 10800 3250 2    50   ~ 0
TEST_DIFF2_R
Text Label 10800 3350 2    50   ~ 0
~TEST_DIFF2_R
Text Label 10800 2850 2    50   ~ 0
TEST_DIFF0_R
Text Label 10800 2950 2    50   ~ 0
~TEST_DIFF0_R
Wire Wire Line
	9850 2950 9600 2950
$Comp
L Device:R_Small R26
U 1 1 5FA81F62
P 9950 2950
AR Path="/5FF144B4/5FA81F62" Ref="R26"  Part="1" 
AR Path="/5F956CA5/5FA81F62" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA81F62" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA81F62" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA81F62" Ref="R52"  Part="1" 
F 0 "R52" V 9900 3100 50  0000 C CNN
F 1 "10k" V 9900 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 2950 50  0001 C CNN
F 3 "~" H 9950 2950 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 2950 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 2950 50  0001 C CNN "RS"
	1    9950 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10900 2950 10050 2950
Wire Wire Line
	9850 2850 9600 2850
$Comp
L Device:R_Small R25
U 1 1 5FA729FB
P 9950 2850
AR Path="/5FF144B4/5FA729FB" Ref="R25"  Part="1" 
AR Path="/5F956CA5/5FA729FB" Ref="R?"  Part="1" 
AR Path="/5F9C92FC/5FA729FB" Ref="R?"  Part="1" 
AR Path="/5FC1767E/5FA729FB" Ref="R?"  Part="1" 
AR Path="/5FC76325/5FA729FB" Ref="R51"  Part="1" 
F 0 "R51" V 9900 3000 50  0000 C CNN
F 1 "10k" V 9900 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9950 2850 50  0001 C CNN
F 3 "~" H 9950 2850 50  0001 C CNN
F 4 "CRG0603F10K" H 9950 2850 50  0001 C CNN "Manufacturer"
F 5 "125-1173" H 9950 2850 50  0001 C CNN "RS"
	1    9950 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10900 2850 10050 2850
Text Label 4000 2100 1    50   ~ 0
TEST_DIFF0_R
Text Label 4000 3200 1    50   ~ 0
~TEST_DIFF0_R
Text Label 4000 4300 1    50   ~ 0
TEST_DIFF1_R
Text Label 4000 5400 1    50   ~ 0
~TEST_DIFF1_R
Text Label 4000 6550 1    50   ~ 0
TEST_DIFF2_R
Text Label 4000 7650 1    50   ~ 0
~TEST_DIFF2_R
Wire Wire Line
	4000 7650 4000 7100
Text HLabel 9600 2850 0    50   Input ~ 0
TEST_DIFF0
Text HLabel 9600 2950 0    50   Input ~ 0
~TEST_DIFF0
Text HLabel 9600 3050 0    50   Input ~ 0
TEST_DIFF1
Text HLabel 9600 3150 0    50   Input ~ 0
~TEST_DIFF1
Text HLabel 9600 3250 0    50   Input ~ 0
TEST_DIFF2
Text HLabel 9600 3350 0    50   Input ~ 0
~TEST_DIFF2
Text HLabel 8350 1550 2    50   Output ~ 0
SE_0
Text HLabel 8350 1850 2    50   Output ~ 0
SE_1
Wire Wire Line
	5850 2650 5200 2650
Wire Wire Line
	5850 2650 5850 1650
Wire Wire Line
	6050 1950 6050 4850
Wire Wire Line
	5950 3750 5950 1850
Wire Wire Line
	5950 1850 6850 1850
Wire Wire Line
	6050 1950 6850 1950
Connection ~ 5200 1550
Wire Wire Line
	5200 1550 6850 1550
Wire Wire Line
	5850 1650 6850 1650
NoConn ~ 6850 2750
$Comp
L Device:R_Small R23
U 1 1 5FBC7D70
P 6800 3100
AR Path="/5FF144B4/5FBC7D70" Ref="R23"  Part="1" 
AR Path="/5FC76325/5FBC7D70" Ref="R49"  Part="1" 
F 0 "R49" H 6859 3146 50  0000 L CNN
F 1 "0R" H 6859 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6800 3100 50  0001 C CNN
F 3 "~" H 6800 3100 50  0001 C CNN
F 4 "CRG0603ZR" H 6800 3100 50  0001 C CNN "Manufacturer"
F 5 "125-1171" H 6800 3100 50  0001 C CNN "RS"
	1    6800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2850 6800 3000
Wire Wire Line
	6800 3200 6800 3300
$EndSCHEMATC
