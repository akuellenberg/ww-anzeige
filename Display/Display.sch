EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Display_Character:HDSP-A151 U1
U 1 1 5FB974E1
P 1550 2000
F 0 "U1" H 1550 2667 50  0000 C CNN
F 1 "HDSP-A151" H 1550 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 1550 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 1050 2550 50  0001 C CNN
F 4 "HDSP-A151" H 1550 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 1550 2000 50  0001 C CNN "Mouser"
	1    1550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1700 1000 1700
Wire Wire Line
	1250 1800 1000 1800
Wire Wire Line
	1250 1900 1000 1900
Wire Wire Line
	1250 2000 1000 2000
Wire Wire Line
	1250 2100 1000 2100
Wire Wire Line
	1250 2200 1000 2200
Wire Wire Line
	1250 2300 1000 2300
Wire Wire Line
	1250 2400 1000 2400
Text Label 1000 1700 0    50   ~ 0
SEG_A
Text Label 1000 1800 0    50   ~ 0
SEG_B
Text Label 1000 1900 0    50   ~ 0
SEG_C
Text Label 1000 2000 0    50   ~ 0
SEG_D
Text Label 1000 2100 0    50   ~ 0
SEG_E
Text Label 1000 2200 0    50   ~ 0
SEG_F
Text Label 1000 2300 0    50   ~ 0
SEG_G
Text Label 1000 2400 0    50   ~ 0
DP
Wire Wire Line
	1850 2300 1900 2300
Wire Wire Line
	1850 2400 1900 2400
Wire Wire Line
	1900 2400 1900 2300
Connection ~ 1900 2300
Wire Wire Line
	1900 2300 2200 2300
Text Label 2200 2300 2    50   ~ 0
DIGIT_0
$Comp
L Display_Character:HDSP-A151 U2
U 1 1 5FBA6BAB
P 2850 2000
F 0 "U2" H 2850 2667 50  0000 C CNN
F 1 "HDSP-A151" H 2850 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 2850 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 2350 2550 50  0001 C CNN
F 4 "HDSP-A151" H 2850 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 2850 2000 50  0001 C CNN "Mouser"
	1    2850 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1700 2300 1700
Wire Wire Line
	2550 1800 2300 1800
Wire Wire Line
	2550 1900 2300 1900
Wire Wire Line
	2550 2000 2300 2000
Wire Wire Line
	2550 2100 2300 2100
Wire Wire Line
	2550 2200 2300 2200
Wire Wire Line
	2550 2300 2300 2300
Wire Wire Line
	2550 2400 2300 2400
Text Label 2300 1700 0    50   ~ 0
SEG_A
Text Label 2300 1800 0    50   ~ 0
SEG_B
Text Label 2300 1900 0    50   ~ 0
SEG_C
Text Label 2300 2000 0    50   ~ 0
SEG_D
Text Label 2300 2100 0    50   ~ 0
SEG_E
Text Label 2300 2200 0    50   ~ 0
SEG_F
Text Label 2300 2300 0    50   ~ 0
SEG_G
Text Label 2300 2400 0    50   ~ 0
DP
Wire Wire Line
	3150 2300 3200 2300
Wire Wire Line
	3150 2400 3200 2400
Wire Wire Line
	3200 2400 3200 2300
Connection ~ 3200 2300
Wire Wire Line
	3200 2300 3500 2300
Text Label 4800 2300 2    50   ~ 0
DIGIT_2
$Comp
L Display_Character:HDSP-A151 U3
U 1 1 5FBB40AC
P 4150 2000
F 0 "U3" H 4150 2667 50  0000 C CNN
F 1 "HDSP-A151" H 4150 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 4150 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 3650 2550 50  0001 C CNN
F 4 "HDSP-A151" H 4150 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 4150 2000 50  0001 C CNN "Mouser"
	1    4150 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1700 3600 1700
Wire Wire Line
	3850 1800 3600 1800
Wire Wire Line
	3850 1900 3600 1900
Wire Wire Line
	3850 2000 3600 2000
Wire Wire Line
	3850 2100 3600 2100
Wire Wire Line
	3850 2200 3600 2200
Wire Wire Line
	3850 2300 3600 2300
Wire Wire Line
	3850 2400 3600 2400
Text Label 3600 1700 0    50   ~ 0
SEG_A
Text Label 3600 1800 0    50   ~ 0
SEG_B
Text Label 3600 1900 0    50   ~ 0
SEG_C
Text Label 3600 2000 0    50   ~ 0
SEG_D
Text Label 3600 2100 0    50   ~ 0
SEG_E
Text Label 3600 2200 0    50   ~ 0
SEG_F
Text Label 3600 2300 0    50   ~ 0
SEG_G
Text Label 3600 2400 0    50   ~ 0
DP
Wire Wire Line
	4450 2300 4500 2300
Wire Wire Line
	4450 2400 4500 2400
Wire Wire Line
	4500 2400 4500 2300
Connection ~ 4500 2300
Wire Wire Line
	4500 2300 4800 2300
Text Label 6100 2300 2    50   ~ 0
DIGIT_3
$Comp
L Display_Character:HDSP-A151 U4
U 1 1 5FBB40C8
P 5450 2000
F 0 "U4" H 5450 2667 50  0000 C CNN
F 1 "HDSP-A151" H 5450 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 5450 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 4950 2550 50  0001 C CNN
F 4 "HDSP-A151" H 5450 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 5450 2000 50  0001 C CNN "Mouser"
	1    5450 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1700 4900 1700
Wire Wire Line
	5150 1800 4900 1800
Wire Wire Line
	5150 1900 4900 1900
Wire Wire Line
	5150 2000 4900 2000
Wire Wire Line
	5150 2100 4900 2100
Wire Wire Line
	5150 2200 4900 2200
Wire Wire Line
	5150 2300 4900 2300
Wire Wire Line
	5150 2400 4900 2400
Text Label 4900 1700 0    50   ~ 0
SEG_A
Text Label 4900 1800 0    50   ~ 0
SEG_B
Text Label 4900 1900 0    50   ~ 0
SEG_C
Text Label 4900 2000 0    50   ~ 0
SEG_D
Text Label 4900 2100 0    50   ~ 0
SEG_E
Text Label 4900 2200 0    50   ~ 0
SEG_F
Text Label 4900 2300 0    50   ~ 0
SEG_G
Text Label 4900 2400 0    50   ~ 0
DP
Wire Wire Line
	5750 2300 5800 2300
Wire Wire Line
	5750 2400 5800 2400
Wire Wire Line
	5800 2400 5800 2300
Connection ~ 5800 2300
Wire Wire Line
	5800 2300 6100 2300
Text Label 7400 2300 2    50   ~ 0
DIGIT_4
$Comp
L Display_Character:HDSP-A151 U5
U 1 1 5FBBE0A8
P 6750 2000
F 0 "U5" H 6750 2667 50  0000 C CNN
F 1 "HDSP-A151" H 6750 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 6750 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 6250 2550 50  0001 C CNN
F 4 "HDSP-A151" H 6750 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 6750 2000 50  0001 C CNN "Mouser"
	1    6750 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1700 6200 1700
Wire Wire Line
	6450 1800 6200 1800
Wire Wire Line
	6450 1900 6200 1900
Wire Wire Line
	6450 2000 6200 2000
Wire Wire Line
	6450 2100 6200 2100
Wire Wire Line
	6450 2200 6200 2200
Wire Wire Line
	6450 2300 6200 2300
Wire Wire Line
	6450 2400 6200 2400
Text Label 6200 1700 0    50   ~ 0
SEG_A
Text Label 6200 1800 0    50   ~ 0
SEG_B
Text Label 6200 1900 0    50   ~ 0
SEG_C
Text Label 6200 2000 0    50   ~ 0
SEG_D
Text Label 6200 2100 0    50   ~ 0
SEG_E
Text Label 6200 2200 0    50   ~ 0
SEG_F
Text Label 6200 2300 0    50   ~ 0
SEG_G
Text Label 6200 2400 0    50   ~ 0
DP
Wire Wire Line
	7050 2300 7100 2300
Wire Wire Line
	7050 2400 7100 2400
Wire Wire Line
	7100 2400 7100 2300
Connection ~ 7100 2300
Wire Wire Line
	7100 2300 7400 2300
Text Label 8700 2300 2    50   ~ 0
DIGIT_5
$Comp
L Display_Character:HDSP-A151 U6
U 1 1 5FBBE0C4
P 8050 2000
F 0 "U6" H 8050 2667 50  0000 C CNN
F 1 "HDSP-A151" H 8050 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 8050 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 7550 2550 50  0001 C CNN
F 4 "HDSP-A151" H 8050 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 8050 2000 50  0001 C CNN "Mouser"
	1    8050 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1700 7500 1700
Wire Wire Line
	7750 1800 7500 1800
Wire Wire Line
	7750 1900 7500 1900
Wire Wire Line
	7750 2000 7500 2000
Wire Wire Line
	7750 2100 7500 2100
Wire Wire Line
	7750 2200 7500 2200
Wire Wire Line
	7750 2300 7500 2300
Wire Wire Line
	7750 2400 7500 2400
Text Label 7500 1700 0    50   ~ 0
SEG_A
Text Label 7500 1800 0    50   ~ 0
SEG_B
Text Label 7500 1900 0    50   ~ 0
SEG_C
Text Label 7500 2000 0    50   ~ 0
SEG_D
Text Label 7500 2100 0    50   ~ 0
SEG_E
Text Label 7500 2200 0    50   ~ 0
SEG_F
Text Label 7500 2300 0    50   ~ 0
SEG_G
Text Label 7500 2400 0    50   ~ 0
DP
Wire Wire Line
	8350 2300 8400 2300
Wire Wire Line
	8350 2400 8400 2400
Wire Wire Line
	8400 2400 8400 2300
Connection ~ 8400 2300
Wire Wire Line
	8400 2300 8700 2300
Text Label 10000 2300 2    50   ~ 0
DIGIT_6
$Comp
L Display_Character:HDSP-A151 U7
U 1 1 5FBBE0E0
P 9350 2000
F 0 "U7" H 9350 2667 50  0000 C CNN
F 1 "HDSP-A151" H 9350 2576 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 9350 1450 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 8850 2550 50  0001 C CNN
F 4 "HDSP-A151" H 9350 2000 50  0001 C CNN "Manufacturer"
F 5 " 630-HDSP-A151 " H 9350 2000 50  0001 C CNN "Mouser"
	1    9350 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 1700 8800 1700
Wire Wire Line
	9050 1800 8800 1800
Wire Wire Line
	9050 1900 8800 1900
Wire Wire Line
	9050 2000 8800 2000
Wire Wire Line
	9050 2100 8800 2100
Wire Wire Line
	9050 2200 8800 2200
Wire Wire Line
	9050 2300 8800 2300
Wire Wire Line
	9050 2400 8800 2400
Text Label 8800 1700 0    50   ~ 0
SEG_A
Text Label 8800 1800 0    50   ~ 0
SEG_B
Text Label 8800 1900 0    50   ~ 0
SEG_C
Text Label 8800 2000 0    50   ~ 0
SEG_D
Text Label 8800 2100 0    50   ~ 0
SEG_E
Text Label 8800 2200 0    50   ~ 0
SEG_F
Text Label 8800 2300 0    50   ~ 0
SEG_G
Text Label 8800 2400 0    50   ~ 0
DP
Wire Wire Line
	9650 2300 9700 2300
Wire Wire Line
	9650 2400 9700 2400
Wire Wire Line
	9700 2400 9700 2300
Connection ~ 9700 2300
Wire Wire Line
	9700 2300 10000 2300
Wire Wire Line
	1950 3800 1350 3800
Wire Wire Line
	1950 3900 1350 3900
Wire Wire Line
	1950 4000 1350 4000
Wire Wire Line
	1950 4100 1350 4100
Wire Wire Line
	1950 4200 1350 4200
Wire Wire Line
	1950 4300 1350 4300
Wire Wire Line
	1950 4400 1350 4400
Wire Wire Line
	1950 4500 1350 4500
Wire Wire Line
	1950 4700 1350 4700
Wire Wire Line
	1950 4800 1350 4800
Wire Wire Line
	1950 4900 1350 4900
Wire Wire Line
	1950 5000 1350 5000
Wire Wire Line
	1950 5100 1350 5100
Wire Wire Line
	1950 5200 1350 5200
Wire Wire Line
	1950 5300 1350 5300
Wire Wire Line
	1950 5400 1350 5400
Text Label 1350 3800 0    50   ~ 0
DIGIT_0
Text Label 3500 2300 2    50   ~ 0
DIGIT_1
Text Label 1350 3900 0    50   ~ 0
DIGIT_1
Text Label 1350 4000 0    50   ~ 0
DIGIT_2
Text Label 1350 4100 0    50   ~ 0
DIGIT_3
Text Label 1350 4200 0    50   ~ 0
DIGIT_4
Text Label 1350 4300 0    50   ~ 0
DIGIT_5
Text Label 1350 4400 0    50   ~ 0
DIGIT_6
Text Label 1350 4500 0    50   ~ 0
DIGIT_7
Text Label 1350 4700 0    50   ~ 0
SEG_A
Text Label 1350 4800 0    50   ~ 0
SEG_B
Text Label 1350 4900 0    50   ~ 0
SEG_C
Text Label 1350 5000 0    50   ~ 0
SEG_D
Text Label 1350 5100 0    50   ~ 0
SEG_E
Text Label 1350 5200 0    50   ~ 0
SEG_F
Text Label 1350 5400 0    50   ~ 0
DP
Text Label 1350 5300 0    50   ~ 0
SEG_G
$Comp
L Connector:DB25_Female_MountingHoles J1
U 1 1 5FBFF90B
P 2250 5000
F 0 "J1" H 2430 5002 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 2430 4911 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Horizontal_P2.77x2.84mm_EdgePinOffset7.70mm_Housed_MountingHolesOffset9.12mm" H 2250 5000 50  0001 C CNN
F 3 " ~" H 2250 5000 50  0001 C CNN
F 4 "A-DF 25 A/KG-T2" H 2250 5000 50  0001 C CNN "Manufacturer"
F 5 "674-0824" H 2250 5000 50  0001 C CNN "RS"
	1    2250 5000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FC25A97
P 1250 7100
F 0 "H1" H 1350 7103 50  0000 L CNN
F 1 "MountingHole_Pad" H 1350 7058 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1250 7100 50  0001 C CNN
F 3 "~" H 1250 7100 50  0001 C CNN
	1    1250 7100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FC25F4E
P 1550 7100
F 0 "H2" H 1650 7103 50  0000 L CNN
F 1 "MountingHole_Pad" H 1650 7058 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1550 7100 50  0001 C CNN
F 3 "~" H 1550 7100 50  0001 C CNN
	1    1550 7100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5FC260B8
P 1850 7100
F 0 "H3" H 1950 7103 50  0000 L CNN
F 1 "MountingHole_Pad" H 1950 7058 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1850 7100 50  0001 C CNN
F 3 "~" H 1850 7100 50  0001 C CNN
	1    1850 7100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5FC26405
P 2150 7100
F 0 "H4" H 2250 7103 50  0000 L CNN
F 1 "MountingHole_Pad" H 2250 7058 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2150 7100 50  0001 C CNN
F 3 "~" H 2150 7100 50  0001 C CNN
	1    2150 7100
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0101
U 1 1 5FC27DBC
P 1250 7250
F 0 "#PWR0101" H 1250 7000 50  0001 C CNN
F 1 "Earth" H 1250 7100 50  0001 C CNN
F 2 "" H 1250 7250 50  0001 C CNN
F 3 "~" H 1250 7250 50  0001 C CNN
	1    1250 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 7200 1250 7250
$Comp
L power:Earth #PWR0102
U 1 1 5FC2C65C
P 1550 7250
F 0 "#PWR0102" H 1550 7000 50  0001 C CNN
F 1 "Earth" H 1550 7100 50  0001 C CNN
F 2 "" H 1550 7250 50  0001 C CNN
F 3 "~" H 1550 7250 50  0001 C CNN
	1    1550 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 7200 1550 7250
$Comp
L power:Earth #PWR0103
U 1 1 5FC308A9
P 1850 7250
F 0 "#PWR0103" H 1850 7000 50  0001 C CNN
F 1 "Earth" H 1850 7100 50  0001 C CNN
F 2 "" H 1850 7250 50  0001 C CNN
F 3 "~" H 1850 7250 50  0001 C CNN
	1    1850 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 7200 1850 7250
$Comp
L power:Earth #PWR0104
U 1 1 5FC34AB4
P 2150 7250
F 0 "#PWR0104" H 2150 7000 50  0001 C CNN
F 1 "Earth" H 2150 7100 50  0001 C CNN
F 2 "" H 2150 7250 50  0001 C CNN
F 3 "~" H 2150 7250 50  0001 C CNN
	1    2150 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 7200 2150 7250
$Comp
L power:Earth #PWR0105
U 1 1 5FC38F5F
P 2250 6450
F 0 "#PWR0105" H 2250 6200 50  0001 C CNN
F 1 "Earth" H 2250 6300 50  0001 C CNN
F 2 "" H 2250 6450 50  0001 C CNN
F 3 "~" H 2250 6450 50  0001 C CNN
	1    2250 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6400 2250 6450
$EndSCHEMATC
