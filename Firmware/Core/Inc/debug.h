/*
 * debug.h
 *
 *  Created on: Apr 7, 2021
 *      Author: ondre
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

int8_t generateError(int8_t state);
int8_t viewErrorMessages(int8_t state);
int8_t writeBackupSRAM(int8_t state);
int8_t readBackupSRAM(int8_t state);
int8_t testpulse(int8_t state);


#endif /* INC_DEBUG_H_ */
