/*
 * error.h
 *
 *  Created on: Mar 12, 2021
 *      Author: ondre
 */

#ifndef INC_ERROR_H_
#define INC_ERROR_H_


#include "system.h"


#define NUM_ERROR_MESSAGES 10

enum {
	ERR_NO_ERROR,
	ERR_GENERIC,
	ERR_TEST
};

enum {
	ERR_STAT_NOERR		= 0x00,
	ERR_STAT_PENDING	= 0x01,
	ERR_STAT_MUTE		= 0x02
};

typedef struct {
	uint8_t code;
	uint8_t status;
} error_t;

void error(uint8_t code);
error_t *getLastError(void);
void dropLastError(void);

#endif /* INC_ERROR_H_ */
