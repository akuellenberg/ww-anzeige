/*
 * fpcontrol.h
 *
 *  Created on: Jan 20, 2021
 *      Author: ondre
 */

#ifndef INC_FPCONTROL_H_
#define INC_FPCONTROL_H_

enum
{
	STATUS_RUN		= 0x01,
	STATUS_STOP		= 0x02,
	STATUS_AZ_FWD	= 0x04,
	STATUS_AZ_REV	= 0x08,
	STATUS_AZ_SET	= 0x10,
	STATUS_EL_FWD	= 0x20,
	STATUS_EL_REV	= 0x40,
	STATUS_EL_SET	= 0x80
};

enum {
	ROT_IDLE	= 0,
	ROT_CW		= 1,
	ROT_CCW		= 2
};

enum {
	BTN_IDLE	= 0,
	BTN_PUSHED	= 1,
	BTN_SHORT	= 2,
	BTN_LONG	= 4,
	BTN_FAULT	= 8,
	BTN_QUERIED	= 16
};
enum {BTN_S1, BTN_S2, BTN_S3, BTN_ENC};

volatile uint8_t gScreenRefresh;
volatile uint8_t statusLEDs;

void encoderISR(void);
void pushButtonISR(void);
uint8_t checkEncoder(uint8_t dir);
uint8_t checkButton(uint8_t button, uint8_t value);
uint8_t refreshScreen(void);
void updateStatusLEDs(void);
void encoderSpeedISR(void);
uint8_t getEncoderVelocity(void);

#endif /* INC_FPCONTROL_H_ */
