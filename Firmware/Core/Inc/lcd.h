/*
 * lcd.h
 *
 *  Created on: Feb 4, 2021
 *      Author: ondre
 */

#ifndef SRC_LCD_H_
#define SRC_LCD_H_

// commands
enum {
	LCD_CLEARDISPLAY	= 0x01,
	LCD_RETURNHOME      = 0x02,
	LCD_ENTRYMODESET    = 0x04,
	LCD_DISPLAYCONTROL  = 0x08,
	LCD_CURSORSHIFT     = 0x10,
	LCD_FUNCTIONSET     = 0x20,
	LCD_SETCGRAMADDR    = 0x40,
	LCD_SETDDRAMADDR    = 0x80
};

// flags for LCD_ENTRYMODESET
enum {
	LCD_ENTRYLEFT			= 0x02,
	LCD_ENTRYSHIFTINCREMENT	= 0x01,
};

// flags for LCD_DISPLAYCONTROL
enum {
	LCD_DISPLAYON	= 0x04,
	LCD_CURSORON	= 0x02,
	LCD_BLINKON		= 0x01
};

volatile uint8_t lcdBlinkFlag;
char BO, BC;

void LCD_SPIInit(SPI_HandleTypeDef *hspi);
void LCD_SendByte(uint8_t rw, uint8_t rs, uint8_t byte);
void LCD_WriteXY(uint8_t x, uint8_t y, const char *s);
void LCD_SetCursor(uint8_t x, uint8_t y);
void LCD_Clear(void);
void LCD_BlinkOff(void);
void LCD_BlinkOn(void);
void LCD_LoadCustomChars(void);
void LCD_BlinkingBracketISR(void);
void LCD_SetContrast(uint8_t contrast);

#endif /* SRC_LCD_H_ */
