/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define nTEST_DIFF_A2_Pin GPIO_PIN_13
#define nTEST_DIFF_A2_GPIO_Port GPIOC
#define TEST_DIFF_A2_Pin GPIO_PIN_14
#define TEST_DIFF_A2_GPIO_Port GPIOC
#define V_MON_Pin GPIO_PIN_15
#define V_MON_GPIO_Port GPIOC
#define nTEST_DIFF_A1_Pin GPIO_PIN_0
#define nTEST_DIFF_A1_GPIO_Port GPIOC
#define TEST_DIFF_A1_Pin GPIO_PIN_1
#define TEST_DIFF_A1_GPIO_Port GPIOC
#define nTEST_DIFF_A0_Pin GPIO_PIN_2
#define nTEST_DIFF_A0_GPIO_Port GPIOC
#define TEST_DIFF_A0_Pin GPIO_PIN_3
#define TEST_DIFF_A0_GPIO_Port GPIOC
#define A2_SE_Pin GPIO_PIN_0
#define A2_SE_GPIO_Port GPIOA
#define A2_SE_EXTI_IRQn EXTI0_IRQn
#define A1_SE_Pin GPIO_PIN_1
#define A1_SE_GPIO_Port GPIOA
#define A1_SE_EXTI_IRQn EXTI1_IRQn
#define A0_SE_Pin GPIO_PIN_2
#define A0_SE_GPIO_Port GPIOA
#define A0_SE_EXTI_IRQn EXTI2_IRQn
#define STATUS_LED_Pin GPIO_PIN_3
#define STATUS_LED_GPIO_Port GPIOA
#define nFP_LCD_RESET_Pin GPIO_PIN_4
#define nFP_LCD_RESET_GPIO_Port GPIOA
#define FP_ROT_A_Pin GPIO_PIN_5
#define FP_ROT_A_GPIO_Port GPIOA
#define FP_ROT_B_Pin GPIO_PIN_6
#define FP_ROT_B_GPIO_Port GPIOA
#define FP_S1_Pin GPIO_PIN_7
#define FP_S1_GPIO_Port GPIOA
#define FP_S4_Pin GPIO_PIN_4
#define FP_S4_GPIO_Port GPIOC
#define FP_S3_Pin GPIO_PIN_5
#define FP_S3_GPIO_Port GPIOC
#define FP_S2_Pin GPIO_PIN_0
#define FP_S2_GPIO_Port GPIOB
#define FP_STATUS_SDI_Pin GPIO_PIN_1
#define FP_STATUS_SDI_GPIO_Port GPIOB
#define nFP_STATUS_OE_Pin GPIO_PIN_2
#define nFP_STATUS_OE_GPIO_Port GPIOB
#define FP_STATUS_CLK_Pin GPIO_PIN_10
#define FP_STATUS_CLK_GPIO_Port GPIOB
#define FP_STATUS_LE_Pin GPIO_PIN_11
#define FP_STATUS_LE_GPIO_Port GPIOB
#define FP_LCD_BL_Pin GPIO_PIN_12
#define FP_LCD_BL_GPIO_Port GPIOB
#define nSEG_OE_Pin GPIO_PIN_6
#define nSEG_OE_GPIO_Port GPIOC
#define SEG_LE_Pin GPIO_PIN_7
#define SEG_LE_GPIO_Port GPIOC
#define SEG_CLK_Pin GPIO_PIN_8
#define SEG_CLK_GPIO_Port GPIOC
#define SEG_SDI_Pin GPIO_PIN_9
#define SEG_SDI_GPIO_Port GPIOC
#define SEG_A2_Pin GPIO_PIN_8
#define SEG_A2_GPIO_Port GPIOA
#define SEG_A1_Pin GPIO_PIN_9
#define SEG_A1_GPIO_Port GPIOA
#define SEG_A0_Pin GPIO_PIN_10
#define SEG_A0_GPIO_Port GPIOA
#define B2_SE_Pin GPIO_PIN_15
#define B2_SE_GPIO_Port GPIOA
#define B2_SE_EXTI_IRQn EXTI15_10_IRQn
#define B1_SE_Pin GPIO_PIN_10
#define B1_SE_GPIO_Port GPIOC
#define B1_SE_EXTI_IRQn EXTI15_10_IRQn
#define B0_SE_Pin GPIO_PIN_11
#define B0_SE_GPIO_Port GPIOC
#define B0_SE_EXTI_IRQn EXTI15_10_IRQn
#define nTEST_DIFF_B2_Pin GPIO_PIN_12
#define nTEST_DIFF_B2_GPIO_Port GPIOC
#define TEST_DIFF_B2_Pin GPIO_PIN_2
#define TEST_DIFF_B2_GPIO_Port GPIOD
#define nTEST_DIFF_B1_Pin GPIO_PIN_4
#define nTEST_DIFF_B1_GPIO_Port GPIOB
#define TEST_DIFF_B1_Pin GPIO_PIN_5
#define TEST_DIFF_B1_GPIO_Port GPIOB
#define nTEST_DIFF_B0_Pin GPIO_PIN_6
#define nTEST_DIFF_B0_GPIO_Port GPIOB
#define TEST_DIFF_B0_Pin GPIO_PIN_7
#define TEST_DIFF_B0_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
