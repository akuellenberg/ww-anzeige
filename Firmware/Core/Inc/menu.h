/*
 * menu.h
 *
 *  Created on: Feb 6, 2021
 *      Author: ondre
 */

#ifndef INC_MENU_H_
#define INC_MENU_H_

#include "system.h"


typedef int8_t (*fptr_t)(int8_t state);

typedef struct {
	uint8_t itemId;
	uint8_t menuId;
	char text[20];
	uint8_t subMenuId;
	fptr_t fctPtr;
} menu_t;

int8_t setStartStop(int8_t state);
int8_t setSegOrder(int8_t state);
int8_t setAzimuthWarnPos(int8_t state);
int8_t setElevationWarnPos(int8_t state);
int8_t setWarnPos(volatile axisData_t *axis, int8_t state);
int8_t showVersion(int8_t state);
int8_t setRefMaxSpeed(int8_t state);
int8_t setRefWarnThres(int8_t state);
int8_t setAutoRefMode(int8_t state);
int8_t selftest(int8_t state);
int8_t setReferencePos(volatile axisData_t *axis, int8_t state);
int8_t setAutoRefPos(volatile axisData_t *axis, int8_t state);
int8_t setAzimuthRefPos(int8_t state);
int8_t setElevationRefPos(int8_t state);
int8_t setAzimuthAutoRefPos(int8_t state);
int8_t setElevationAutoRefPos(int8_t state);
int8_t setContrast(int8_t state);
int8_t setBrightness(int8_t state);
int8_t StatusDisplay(int8_t state);

void rotaryInput(int32_t *var, int32_t min, int32_t max, uint8_t slowInc, uint16_t threshold);
void slider(int16_t var, int16_t min, int16_t max);
void initMenu(void);
fptr_t Menu(void);

#endif /* INC_MENU_H_ */
