/*
 * segment.h
 *
 *  Created on: Feb 6, 2021
 *      Author: ondre
 */

#ifndef INC_SEGMENT_H_
#define INC_SEGMENT_H_

typedef enum {
	DOT_ON	= 1,
	DOT_OFF = 0
} dpState_t;

enum {
	FLASH_ON 	= 1,
	FLASH_OFF	= 0
};

volatile uint8_t flashSegments, flashState;

void setSegments(uint8_t numDisp1, uint8_t numDisp2, dpState_t dp);
void displayMultiplexer(volatile uint8_t *segDisp1, volatile uint8_t *segDisp2);

#endif /* INC_SEGMENT_H_ */
