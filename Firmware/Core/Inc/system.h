/*
 * mystuff.h
 *
 *  Created on: Jan 20, 2021
 *      Author: ondre
 */

#ifndef INC_SYSTEM_H_
#define INC_SYSTEM_H_

#include "main.h"

#define FIRMWARE_VERSION "0.9"

//#define DEBUG_MODE			1

#define BAT_LOW_THRES 		2.5		// Backup battery voltage warning level (Volt)
#define SUPPLY_LOW_THRES	3.2		// Minimum supply voltage warning level (Volt)
#define TEMP_HIGH_THRES		50.0	// Max. MCU temperature (degC) warning level
#define MAX_MAN_REF_SPEED	5.0		// Max. allowed telescope speed (arcsec/s) during manual referencing



typedef enum {
	AXIS_INVALID, AXIS_STOPPED, AXIS_REFERENCED, AXIS_LOSTREF
} axisStatus_t;

typedef struct {
	uint16_t deg;
	uint8_t min;
	uint8_t sec;
} dms_t;

typedef struct {
	uint32_t counts;
	uint32_t lastCounts;
	uint32_t deltaCounts;
	uint32_t refCounts;
	uint32_t refWarnCounts;
	uint16_t refMaxSpeed;
	uint32_t totalArcSec;
	float velocity;
	dms_t dms;
	uint8_t segments[8];
	float secondsPerCount;
	uint16_t minDeg;
	uint16_t maxDeg;
	char name[10];
	axisStatus_t status;
	uint8_t setpuls;
	uint8_t refMode;
} axisData_t;

enum {
	AZ_FWD, AZ_REV, AZ_SET, ELV_FWD, ELV_REV, ELV_SET
};

enum {
	SYS_START, SYS_RUN, SYS_STOP, SYS_SELFTEST
};

enum {
	SELFTEST_UNKNOWN, SELFTEST_FAULT, SELFTEST_OK
};

enum {
	AZIMUT, ELEVATION
};

enum {
	REFMODE_OFF, REFMODE_WARN, REFMODE_AUTO
};

enum {
	SEG_ORDER_MSBFIRST, SEG_ORDER_LSBFIRST
};

extern const char *refModes[];
extern const char *segOrder[];
typedef struct {
	float supplyVoltage;
	float temperature;
	float batteryVoltage;
	uint8_t status;
	uint8_t selftest;
	uint8_t segOrder;
} system_t;


uint16_t adcDmaBuffer[3];
system_t sys;
volatile axisData_t elevationAxis, azimuthAxis;

void setStore(void);
void clearStore(void);
void storeSRAM(void);
void restoreSRAM(void);
void updateStatusLEDs(void);
void setupTestPorts(uint8_t mode);
void pulseDiffPort(uint8_t port);
void counterControl(uint8_t axis, uint8_t enable);
void updatePosition(volatile axisData_t *axix);
void updateVelocity(volatile axisData_t *axis);
void setSystemStatus(uint8_t status);
void checkStatus(void);

#endif /* INC_SYSTEM_H_ */
