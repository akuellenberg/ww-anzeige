/*
 * debug.c
 *
 *  Created on: Apr 7, 2021
 *      Author: ondre
 */

#include <stdio.h>
#include "menu.h"
#include "system.h"
#include "fpcontrol.h"
#include "lcd.h"
#include "error.h"

int8_t generateError(int8_t state)
{
	static int8_t code = 0;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Erzeuge Fehler");
		state++;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "Code: %d ", code);
			LCD_WriteXY(0, 1, s);
		}
		rotaryInput(&code, 0, 100, 1, 100);
		if (checkButton(BTN_S2, BTN_SHORT)) {
			error(code);
			state = -1;
		}
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t viewErrorMessages(int8_t state)
{
	char s[30];
	error_t *lastErr;

	switch(state) {
	case 0:
		lastErr = getLastError();
		LCD_Clear();
		LCD_WriteXY(0, 0, "Letzter Fehler:");
		sprintf(s, "Code: %d", lastErr->code);
		LCD_WriteXY(0, 1, s);
		sprintf(s, "Status: %d", lastErr->status);
		LCD_WriteXY(0, 2, s);
		state++;
		break;
	case 1:
		if (checkButton(BTN_S2, BTN_SHORT)) {
			dropLastError();
			state = -1;
		}
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t writeBackupSRAM(int8_t state)
{
	char s[30];
	uint8_t i;

	switch(state) {
	case 0:
		storeSRAM();
		LCD_Clear();
		LCD_WriteXY(0, 0, "Backup SRAM written.");
		state++;
		break;
	case 1:
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t readBackupSRAM(int8_t state)
{
	uint8_t i;
	char s[30];

	switch(state) {
	case 0:
		restoreSRAM();
		initMenu();
		LCD_Clear();
		LCD_WriteXY(0, 0, "Backup SRAM read.");
		state++;
		break;
	case 1:
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t testpulse(int8_t state)
{
	static int32_t c;
	uint32_t i;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0,0,"Testpulse");
		c = 1000;
		state++;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, "\%cAF%cAR AS EF ER ES ",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state = -1;
		} else if (checkButton(BTN_S3, BTN_SHORT)) {
			state++;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			for(i = 0; i < c; i++)
				pulseDiffPort(AZ_FWD);
		}
		break;
	case 2:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, " AF%cAR%cAS EF ER ES ",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state--;
		} else if (checkButton(BTN_S3, BTN_SHORT)) {
			state++;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			for(i = 0; i < c; i++)
				pulseDiffPort(AZ_REV);
		}
		break;
	case 3:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, " AF AR%cAS%cEF ER ES ",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state--;
		} else if (checkButton(BTN_S3, BTN_SHORT)) {
			state++;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			pulseDiffPort(AZ_SET);
		}
		break;
	case 4:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, " AF AR AS%cEF%cER ES ",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state--;
		} else if (checkButton(BTN_S3, BTN_SHORT)) {
			state++;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			for(i = 0; i < c; i++)
				pulseDiffPort(ELV_FWD);
		}
		break;
	case 5:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, " AF AR AS EF%cER%cES ",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state--;
		} else if (checkButton(BTN_S3, BTN_SHORT)) {
			state++;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			for(i = 0; i < c; i++)
				pulseDiffPort(ELV_REV);
		}
		break;
	case 6:
		if (refreshScreen()) {
			sprintf(s, "Counts: %ld ", c);
			LCD_WriteXY(0, 1, s);
			sprintf(s, " AF AR AS EF ER%cES%c",BO,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&c, 0, 100000, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) {
			state--;
		} else if (checkButton(BTN_S2, BTN_SHORT)) {
			pulseDiffPort(ELV_SET);
		}
		if (checkButton(BTN_S3, BTN_SHORT)) {
			state = -1;
		}
		break;

	default:
		state = -1;
	}

	return state;
}
