/*
 * error.c
 *
 *  Created on: Mar 12, 2021
 *      Author: ondre
 */

#include "error.h"

error_t errorMessageQueue[NUM_ERROR_MESSAGES];
static uint8_t head = 0;
static uint8_t tail = 0;

const error_t empty = {
	.code = ERR_NO_ERROR,
	.status = ERR_STAT_NOERR
};

void error(uint8_t code)
{
	errorMessageQueue[head].code = code;
	errorMessageQueue[head].status = ERR_STAT_PENDING;
	head = (head + 1) % NUM_ERROR_MESSAGES;
	if (head == tail)
		tail = (tail + 1) % NUM_ERROR_MESSAGES;
}

error_t *getLastError(void)
{
	return ((head == tail) ? &empty : &errorMessageQueue[tail]);
}

void dropLastError(void)
{
	if (head != tail)
		tail = (tail + 1) % NUM_ERROR_MESSAGES;
}
