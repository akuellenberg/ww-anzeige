/*
 * fpcontrol.c
 *
 *  Created on: Jan 20, 2021
 *      Author: ondre
 */

#include <stdlib.h>
#include "main.h"
#include "fpcontrol.h"
#include "system.h"

//-----------------------------------------------------------------------------
// Definitions
//-----------------------------------------------------------------------------
enum e_enc_states {START = 0, CW1, CW2, CW3, CCW1, CCW2, CCW3};
enum e_enc_flags {CW_FLAG = 0b10000000, CCW_FLAG = 0b01000000};

#define NUM_BUTTONS 4

//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------

// transition table for encoder FSM
const uint8_t transition_table[7][4] = {
/*                 00           01      10      11  */
/* -----------------------------------------------------*/
/*START     | */  {START,       CCW1,   CW1,    START},
/*CW_Step1  | */  {CW2|CW_FLAG, START,  CW1,    START},
/*CW_Step2  | */  {CW2,         CW3,    CW1,    START},
/*CW_Step3  | */  {CW2,         CW3,    START,  START|CW_FLAG},
/*CCW_Step1 | */  {CCW2|CCW_FLAG,CCW1,  START,  START},
/*CCW_Step2 | */  {CCW2,        CCW1,   CCW3,   START},
/*CCW_Step3 | */  {CCW2,        START,  CCW3,   START|CCW_FLAG}
};

volatile uint8_t curEncoderState = START;
volatile uint8_t encoderDirection = 0;
volatile uint8_t buttonState[NUM_BUTTONS] = {BTN_IDLE, BTN_IDLE, BTN_IDLE, BTN_IDLE};
volatile uint16_t encoderVelocity;
volatile int16_t encoderCounts;
//-----------------------------------------------------------------------------
// Rotary encoder ISR
// Sets rotDir to ROT_CW or ROT_CCW
//-----------------------------------------------------------------------------
void encoderISR()
{
	uint8_t rot_a, rot_b;
	rot_a = HAL_GPIO_ReadPin(FP_ROT_A_GPIO_Port, FP_ROT_A_Pin);
	rot_b = HAL_GPIO_ReadPin(FP_ROT_B_GPIO_Port, FP_ROT_B_Pin);
	// set current state according to transition table (cw/ccw flags masked out)
	curEncoderState = transition_table[curEncoderState & 0b00000111][(rot_b << 1) | rot_a];

	// set global direction flag
	if (curEncoderState & CW_FLAG) {
		encoderDirection = ROT_CW;
		encoderCounts++;
	}
	if (curEncoderState & CCW_FLAG) {
		encoderDirection = ROT_CCW;
		encoderCounts--;
	}
}

//-----------------------------------------------------------------------------
// Returns last rotary encoder action
//-----------------------------------------------------------------------------
/*
uint8_t getEncDir(void)
{
	uint8_t ret = encoderDirection;
	encoderDirection = ROT_IDLE;
	return ret;
}
*/

uint8_t checkEncoder(uint8_t dir)
{
	if (encoderDirection & dir) {
		encoderDirection = ROT_IDLE;
		gScreenRefresh = 1;
		return 1;
	} else {
		return 0;
	}
}

void encoderSpeedISR(void)
{
	encoderVelocity = abs(encoderCounts);
	encoderCounts = 0;
}

uint8_t getEncoderVelocity(void)
{
	return encoderVelocity;
}

//-----------------------------------------------------------------------------
// Frontpanel push-button ISR
// Sets buttonState to BTN_PUSHED, BTN_SHORT, BTN_LONG or BTN_FAULT
//-----------------------------------------------------------------------------
void pushButtonISR(void)
{
	static uint32_t buttonTime[NUM_BUTTONS];

	// Read raw button state
	if ( HAL_GPIO_ReadPin(FP_S1_GPIO_Port, FP_S1_Pin) ) buttonState[0] |= BTN_PUSHED; else buttonState[0] &= ~BTN_PUSHED;
	if ( HAL_GPIO_ReadPin(FP_S2_GPIO_Port, FP_S2_Pin) ) buttonState[1] |= BTN_PUSHED; else buttonState[1] &= ~BTN_PUSHED;
	if ( HAL_GPIO_ReadPin(FP_S3_GPIO_Port, FP_S3_Pin) ) buttonState[2] |= BTN_PUSHED; else buttonState[2] &= ~BTN_PUSHED;
	if ( HAL_GPIO_ReadPin(FP_S4_GPIO_Port, FP_S4_Pin) ) buttonState[3] |= BTN_PUSHED; else buttonState[3] &= ~BTN_PUSHED;

	for (uint8_t n = 0; n < NUM_BUTTONS; n++) {
		// button has not been queried before
		if (~buttonState[n] & BTN_QUERIED) {
			if ((buttonTime[n] >= 10) && (buttonTime[n] < 50)) {
				buttonState[n] |= BTN_SHORT;
			} else if ((buttonTime[n] >= 50) && (buttonTime[n] < 500)) {
				buttonState[n] |= BTN_LONG;
			} else if (buttonTime[n] >= 500) {
				buttonState[n] |= BTN_FAULT;
			}

			// Button pushed? Increase time
			if (buttonState[n] & BTN_PUSHED) {
				buttonTime[n]++;
			}
		}
		// Button released? Reset state and time
		if (~buttonState[n] & BTN_PUSHED){
			buttonState[n] = 0;
			buttonTime[n] = 0;
		}
	}
}

//-----------------------------------------------------------------------------
// Checks frontpanel push button 'button' against 'value'
// Sets buttonState to BTN_QUERIED
//-----------------------------------------------------------------------------
uint8_t checkButton(uint8_t button, uint8_t value)
{
	uint8_t stat = buttonState[button];

	if (stat & value) {
		buttonState[button] = BTN_QUERIED;
		gScreenRefresh = 1;
		return 1;
	} else {
		return 0;
	}
}

uint8_t refreshScreen(void)
{
	if (gScreenRefresh) {
		gScreenRefresh = 0;
		return 1;
	} else {
		return 0;
	}
}

//-----------------------------------------------------------------------------
// Shifts data from 'statusLEDs' into frontpanel shift register
//-----------------------------------------------------------------------------
void updateStatusLEDs(void)
{
	for(int i = 0; i < 8; i++) {
		if ((statusLEDs >> i) & 1)
			HAL_GPIO_WritePin(FP_STATUS_SDI_GPIO_Port, FP_STATUS_SDI_Pin, GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(FP_STATUS_SDI_GPIO_Port, FP_STATUS_SDI_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(FP_STATUS_CLK_GPIO_Port, FP_STATUS_CLK_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(FP_STATUS_CLK_GPIO_Port, FP_STATUS_CLK_Pin, GPIO_PIN_RESET);
	}
	HAL_GPIO_WritePin(FP_STATUS_LE_GPIO_Port, FP_STATUS_LE_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(FP_STATUS_LE_GPIO_Port, FP_STATUS_LE_Pin, GPIO_PIN_RESET);
}


