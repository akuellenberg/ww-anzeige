/*
 * lcd.c
 *
 *  Created on: Feb 4, 2021
 *      Author: ondre
 */

#include "main.h"
#include "lcd.h"

const uint8_t customChars[] = {
	0x18, 0x18, 0x03, 0x04, 0x04, 0x04, 0x03, 0x00, // °C
	0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, // center dot
	0x01, 0x01, 0x05, 0x09, 0x1F, 0x08, 0x04, 0x00, // back arrow
	0x00, 0x10, 0x18, 0x1c, 0x18, 0x10, 0x00, 0x00, // entry arrow
	0x0c, 0x12, 0x12, 0x0c, 0x00, 0x00, 0x00, 0x00, // °
	0x00, 0x04, 0x06, 0x07, 0x06, 0x04, 0x00, 0x00, // >
	0x00, 0x04, 0x0c, 0x1c, 0x0c, 0x04, 0x00, 0x00, // <
	0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x07, // |_
	0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x1c  // _|
};

SPI_HandleTypeDef *hspi_lcd;

void LCD_SPIInit(SPI_HandleTypeDef *hspi)
{
	// Store SPI handle
	hspi_lcd = hspi;
	HAL_GPIO_WritePin(FP_LCD_BL_GPIO_Port, FP_LCD_BL_Pin, GPIO_PIN_SET);
	// Pull reset line low for 5ms
	HAL_GPIO_WritePin(nFP_LCD_RESET_GPIO_Port, nFP_LCD_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(nFP_LCD_RESET_GPIO_Port, nFP_LCD_RESET_Pin, GPIO_PIN_SET);
	LCD_SendByte(0, 0, 0x3a);	// Initialization according to datasheet...
	LCD_SendByte(0, 0, 0x09);
	LCD_SendByte(0, 0, 0x06);
	LCD_SendByte(0, 0, 0x1e);
	LCD_SendByte(0, 0, 0x39);
	LCD_SendByte(0, 0, 0x1b);
	LCD_SendByte(0, 0, 0x6e);
	LCD_SendByte(0, 0, 0x57);
	LCD_SendByte(0, 0, 0x72);
	LCD_SendByte(0, 0, 0x38);
	LCD_SendByte(0, 0, 0x0C);	// Display on
	LCD_SendByte(0, 0, 0x01);	// Clear display

	// Select character ROM A
	LCD_SendByte(0, 0, 0x3a);
	LCD_SendByte(0, 0, 0x72);
	LCD_SendByte(0, 1, 0x00);
	LCD_SendByte(0, 0, 0x38);

}

void LCD_SendByte(uint8_t rw, uint8_t rs, uint8_t byte)
{
	uint8_t data[3];
	data[0] = 0x1f | (rw << 5) | (rs << 6);
	data[1] = byte & 0x0f;
	data[2] = (byte >> 4) & 0x0f;
	HAL_SPI_Transmit(hspi_lcd, data, 3, HAL_MAX_DELAY);
}

void LCD_SetCursor(uint8_t x, uint8_t y)
{
	LCD_SendByte(0, 0, LCD_SETDDRAMADDR | (x + (y << 5)));
}

void LCD_BlinkOn(void)
{
	LCD_SendByte(0,0, LCD_DISPLAYCONTROL | LCD_DISPLAYON | LCD_BLINKON );
}

void LCD_BlinkOff(void)
{
	LCD_SendByte(0,0, LCD_DISPLAYCONTROL | LCD_DISPLAYON);
}

void LCD_Clear(void)
{
	LCD_SendByte(0, 0, LCD_CLEARDISPLAY);
}

void LCD_SetContrast(uint8_t contrast)
{
	LCD_SendByte(0, 0, 0x39);
	LCD_SendByte(0, 0, 0x54 | ((contrast & 0x30) >> 4));
	LCD_SendByte(0, 0, 0x70 | (contrast & 0x0f));
	LCD_SendByte(0, 0, 0x38);
}

void LCD_WriteXY(uint8_t x, uint8_t y, const char *s)
{
#ifdef NO_LCD
	for (uint8_t n = 0; n < x; n++)
		printf(" ");
	printf(s);
	printf("\n");
#else
	LCD_SendByte(0, 0, LCD_SETDDRAMADDR | (x + (y << 5)));
	while(*s)
		LCD_SendByte(0, 1, *s++);
#endif
}


void LCD_LoadCustomChars(void)
{
	uint8_t n;

	LCD_SendByte(0, 0, LCD_SETCGRAMADDR + 8);
	for(n = 0; n < sizeof(customChars); n++) {
		LCD_SendByte(0, 1, customChars[n]);
	}
}

void LCD_BlinkingBracketISR(void)
{
	lcdBlinkFlag ^= 1;
	if (lcdBlinkFlag) {
		BO = '\372';
		BC = '\374';
	} else {
		BO = ' ';
		BC = ' ';
	}
}
