/*
 * menu.c
 *
 *  Created on: Feb 6, 2021
 *      Author: ondre
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "main.h"
#include "menu.h"
#include "system.h"
#include "fpcontrol.h"
#include "lcd.h"
#include "segment.h"
#include "error.h"

#ifdef DEBUG_MENU
#include "debug.h"
#endif

enum {MENU_DEFAULT, MENU_REFMODE, MENU_REFSPEED, MENU_REFWARN, MENU_SEGORDER};

static menu_t menuStruct[] = {
		{MENU_DEFAULT, 0, "Hauptmen\176",			0, NULL},
		{MENU_DEFAULT, 0, "Statusanzeige",			0, StatusDisplay},
		{MENU_DEFAULT, 0, "Einstellungen",			1, NULL},
		{MENU_DEFAULT, 0, "Referenz setzen", 		2, NULL},
#ifdef DEBUG_MODE
		{MENU_DEFAULT, 0, "Debug",		 			5, NULL},
#endif

		{MENU_DEFAULT, 1, "Einstellungen",			0, NULL},
		{MENU_DEFAULT, 1, "\336 Hauptmen\176",		0, NULL},
		{MENU_DEFAULT, 1, "Auto Ref. Pos.",			4, NULL},
#ifdef DEBUG_MENU
		{MENU_DEFAULT, 1, "Warnpositionen",			6, NULL},
#endif
		{MENU_DEFAULT, 1, "Selbsttest",				0, selftest},
		{MENU_DEFAULT, 1, "Display Kontrast",		0, setContrast},
		{MENU_SEGORDER,1, "Seg.folge: \3726..0\374",	0, setSegOrder},
		{MENU_DEFAULT, 1, "Info",					0, showVersion},

		{MENU_DEFAULT, 2, "Referenz setzen",		0, NULL},
		{MENU_DEFAULT, 2, "\336 Hauptmen\176",		0, NULL},
		{MENU_DEFAULT, 2, "Z\173hler start/stop", 	0, setStartStop},
		{MENU_DEFAULT, 2, "Azimut",					0, setAzimuthRefPos},
		{MENU_DEFAULT, 2, "Elevation",				0, setElevationRefPos},

		{MENU_DEFAULT, 3, "Display",				0, NULL},
		{MENU_DEFAULT, 3, "\336 Einstellungen",		1, NULL},
		{MENU_DEFAULT, 3, "Kontrast",				0, setContrast},

		{MENU_DEFAULT, 4, "Auto Ref. Pos.",			0, NULL},
		{MENU_DEFAULT, 4, "\336 Einstellungen",		1, NULL},
		{MENU_DEFAULT, 4, "Azimut",					0, setAzimuthAutoRefPos},
		{MENU_DEFAULT, 4, "Elevation",				0, setElevationAutoRefPos},
		{MENU_REFMODE, 4, "Modus: \372aus\374",		0, setAutoRefMode},
		{MENU_REFSPEED,4, "Max.geschw.:1000\231",	0, setRefMaxSpeed},
		{MENU_REFWARN, 4, "Warngrenzw.:100\231",	0, setRefWarnThres},
/*
#ifdef DEBUG_MENU
		{MENU_DEFAULT, 5, "Debug",					0, NULL},
		{MENU_DEFAULT, 5, "\336 Hauptmen\176",		0, NULL},
		{MENU_DEFAULT, 5, "Fehlermeldungen",		0, viewErrorMessages},
		{MENU_DEFAULT, 5, "Testpulse",	 			0, testpulse},
		{MENU_DEFAULT, 5, "Write Backup", 			0, writeBackupSRAM},
		{MENU_DEFAULT, 5, "Read Backup", 			0, readBackupSRAM},
		{MENU_DEFAULT, 5, "Fehler erzeugen",		0, generateError},
#endif
*/
		{MENU_DEFAULT, 6, "Warnpositionen",			0, NULL},
		{MENU_DEFAULT, 6, "\336 Hauptmen\176",		0, NULL},
		{MENU_DEFAULT, 6, "Azimut",					0, setAzimuthWarnPos},
		{MENU_DEFAULT, 6, "Elevation",				0, setElevationWarnPos}
};

int8_t setStartStop(int8_t state)
{
	char s[30];
	static uint8_t n = 0;

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		if (sys.status == SYS_RUN)
			state = 7;
		else
			state = 1;
		break;
	case 1:
		LCD_Clear();
		LCD_WriteXY(0, 0, " Winkelwertanzeige");
		LCD_WriteXY(0, 1, " wird gestartet");
		setSystemStatus(SYS_START);
		n = 0;
		state++;
		break;
	case 2:
		if ((sys.status != SYS_RUN) && n < 5) {
			n++;
			LCD_WriteXY(n, 2, ".");
			HAL_Delay(200);
		} else {
			state = 3;
		}
		break;
	case 3:
		if (sys.status == SYS_RUN)
			LCD_WriteXY(n, 2, "ok");
		else
			LCD_WriteXY(n, 2, "fehlgeschlagen.");
		state++;
		break;
	case 4:
		if (refreshScreen()) {
			sprintf(s, "%cOK%c", BO, BC);
			LCD_WriteXY(8, 3, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;

	case 7:
		LCD_Clear();
		LCD_WriteXY(0, 0, "  Winkelwertanzeige");
		LCD_WriteXY(0, 1, "     anhalten?");
		state++;
		break;
	case 8:
		if (refreshScreen()) {
			sprintf(s, "   %cnein%c      ja ", BO, BC);
			LCD_WriteXY(0, 2, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		if (checkButton(BTN_S3, BTN_SHORT)) state = 9;
		break;
	case 9:
		if (refreshScreen()) {
			sprintf(s, "    nein      %cja%c", BO, BC);
			LCD_WriteXY(0, 2, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state--;
		else if (checkButton(BTN_S2, BTN_SHORT)) state++;
		break;
	case 10:
		LCD_Clear();
		LCD_WriteXY(0, 0, " Winkelwertanzeige");
		LCD_WriteXY(0, 1, "    angehalten.");
		setSystemStatus(SYS_STOP);
		state++;
		break;
	case 11:
		if (refreshScreen()) {
			sprintf(s, "%cOK%c", BO, BC);
			LCD_WriteXY(8, 2, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t setSegOrder(int8_t state)
{
	uint8_t i;

	if (sys.segOrder) {
		sys.segOrder = 0;
	} else {
		sys.segOrder = 1;
	}

	for(i = 0; menuStruct[i].itemId != MENU_SEGORDER;i++);
	sprintf(menuStruct[i].text, "Seg.folge: \372%s\374", segOrder[sys.segOrder]);
	return -1;
}

int8_t setAzimuthWarnPos(int8_t state)
{
	return setWarnPos(&azimuthAxis, state);
}

int8_t setElevationWarnPos(int8_t state)
{
	return setWarnPos(&elevationAxis, state);
}

int8_t setWarnPos(volatile axisData_t *axis, int8_t state)
{
	static dms_t min,max;
	char s[30];


	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch(state) {
	case 0:
		LCD_Clear();
		sprintf(s, "Warnpos. %s:", axis->name);
		LCD_WriteXY(0, 0, s);
		sprintf(s, "Max:  %3d\005  %2d' ", max.deg, max.min);
		LCD_WriteXY(0, 2, s);
		LCD_WriteXY(0, 3, " abbruch     weiter ");
		state = 1;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "Min: %c%3d\005%c %2d' ", BO,min.deg,BC, min.min);
			LCD_WriteXY(0, 1, s);
		}
		rotaryInput(&min.deg, axis->minDeg, axis->maxDeg, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 2;
		break;
	case 2:
		if (refreshScreen()) {
			sprintf(s, "Min:  %3d\005 %c%2d'%c ", min.deg, BO,min.min,BC);
			LCD_WriteXY(0, 1, s);
		}
		rotaryInput(&min.min, 0, 59, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 3;
		break;
	case 3:
		if (refreshScreen()) {
			sprintf(s, "Min:  %3d\005  %2d' ", min.deg, min.min);
			LCD_WriteXY(0, 1, s);
			sprintf(s, "Max: %c%3d\005%c %2d' ", BO,max.deg,BC, max.min);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&max.deg, axis->minDeg, axis->maxDeg, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 4;
		break;
	case 4:
		if (refreshScreen()) {
			sprintf(s, "Max:  %3d\005 %c%2d'%c ", max.deg,BO,max.min,BC);
			LCD_WriteXY(0, 2, s);
		}
		rotaryInput(&max.min, 0, 59, 1, 10);
		if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 5;
		break;
	case 5:

	case 6:

	case 7:

	case 8:

	case 9:

	case 10:
			state = -1;
		break;

	default:
		state = -1;
	}

	return state;
}


int8_t setAutoRefMode(int8_t state)
{
	uint8_t i;
	uint8_t refMode = azimuthAxis.refMode;

	if (refMode < 2) {
		refMode++;
	} else {
		refMode = 0;
	}

	azimuthAxis.refMode = refMode;
	elevationAxis.refMode = refMode;

	for(i = 0; menuStruct[i].itemId != MENU_REFMODE;i++);
	sprintf(menuStruct[i].text, "Modus: \372%s\374", refModes[refMode]);
	return -1;
}


int8_t showVersion(int8_t state)
{
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0,0,"Winkelwertanzeige");
		LCD_WriteXY(0,1,"Steuerpult");
		LCD_WriteXY(0,2,"Firmware Ver.:");
		LCD_WriteXY(0,3,"Date:");
		sprintf(s, "%s", FIRMWARE_VERSION);
		LCD_WriteXY(15,2,s);
		sprintf(s, "%s", __DATE__);
		LCD_WriteXY(6,3,s);

		state++;
		break;
	case 1:
		if (checkButton(BTN_S2, BTN_SHORT)) {
			state = -1;
		}
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t setRefMaxSpeed(int8_t state)
{
	static int32_t refMaxSpeed = 1000;
	uint8_t i;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Max. Geschwindigkeit");
		LCD_WriteXY(0, 1, "bei Auto. Referenz:");
		state++;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "%ld\231  ", refMaxSpeed);
			LCD_WriteXY(7, 2, s);
		}
		rotaryInput(&refMaxSpeed, 0, 3000, 1, 1);
		if (checkButton(BTN_S2, BTN_SHORT)) {
			elevationAxis.refMaxSpeed = refMaxSpeed;
			azimuthAxis.refMaxSpeed = refMaxSpeed;
			for(i = 0; menuStruct[i].itemId != MENU_REFSPEED;i++);
			sprintf(menuStruct[i].text, "Max.geschw.:%d\231", refMaxSpeed);
			state = -1;
		}
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t setRefWarnThres(int8_t state)
{
	static int32_t refWarnSec = 60;
	uint8_t i;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Erlaubte Abweichung");
		LCD_WriteXY(0, 1, "zur Ref. Position:");
		state++;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "%ld\231  ", refWarnSec);
			LCD_WriteXY(7, 2, s);
		}
		rotaryInput(&refWarnSec, 0, 100, 1, 1);
		if (checkButton(BTN_S2, BTN_SHORT)) {
			elevationAxis.refWarnCounts = refWarnSec / elevationAxis.secondsPerCount;
			azimuthAxis.refWarnCounts = refWarnSec / azimuthAxis.secondsPerCount;
			for(i = 0; menuStruct[i].itemId != MENU_REFWARN;i++);
			sprintf(menuStruct[i].text, "Warngrenzw.:%d\231", refWarnSec);
			state = -1;
		}
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t selftest(int8_t state)
{
	static int32_t n = 0;
	uint8_t i;
	char s[30];
	static axisData_t azimuthTmp, elevationTmp;
	static uint8_t result = 0;

	if (checkButton(BTN_S2, BTN_LONG))
		state = 100;

	switch(state) {

	case 0:
		if ((elevationAxis.velocity != 0) || (azimuthAxis.velocity != 0)) {
			LCD_Clear();
			LCD_WriteXY(0, 0,"W\173hrend Selbsttest");
			LCD_WriteXY(0, 1," Teleskop anhalten!");
			state++;
		} else {
			state = 2;
		}
		break;
	case 1:
		if (checkButton(BTN_S1, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%czur\176ck%c",BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		break;

	/*
	 * LED Test
	 */

	case 2:
		sys.status = SYS_SELFTEST;
		flashState = FLASH_OFF;
		for (i = 0; i < 8; i++) {
			azimuthAxis.segments[i] = 8;
			elevationAxis.segments[i] = 8;
		}

		elevationTmp = elevationAxis;
		azimuthTmp = azimuthAxis;

		elevationAxis.counts = 0;
		azimuthAxis.counts = 0;

		setupTestPorts(1);
		counterControl(AZIMUT, 1);
		counterControl(ELEVATION, 1);

		statusLEDs = 0xFF;

		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		LCD_WriteXY(0, 1, "LED Test");
		state++;
		break;
	case 3:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Inputs
	 */

	case 4:
		statusLEDs = 0;
		n = 0;
		static uint8_t keys;
		for (i = 0; i < 8; i++) {
			azimuthAxis.segments[i] = 0x0b;
			elevationAxis.segments[i] = 0x0b;
		}
		LCD_Clear();
		LCD_WriteXY(0, 0, "Taster & Encoder:");
		keys = 0;
		state++;


		state=6;
		break;
	case 5:
		if ((n < 90) || (keys != 15)) {
			rotaryInput(&n, 0, 100, 1, 20);
			keys ^= checkButton(BTN_S1, BTN_SHORT);
			keys ^= checkButton(BTN_S2, BTN_SHORT) << 1;
			keys ^= checkButton(BTN_S3, BTN_SHORT) << 2;
			keys ^= checkButton(BTN_ENC, BTN_SHORT) << 3;
			if (refreshScreen()) {
				slider(n, 0, 90);
				sprintf(s, "(%c) (%c) (%c) (%c) ", (keys&1?'<':' '), (keys&2?'\034':' '), (keys&4?'>':' '), (keys&8?'\273':' '));
				LCD_WriteXY(0, 1, s);
				LCD_WriteXY(12, 3, "        ");
			}
		} else {
			if (refreshScreen()) {
				sprintf(s, "%cweiter%c",BO,BC);
				LCD_WriteXY(12, 3, s);
			}
			if (checkButton(BTN_S3, BTN_SHORT)) state++;
		}

		break;

	/*
	 * Azimuth Counter Test
	 */

	case 6:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		LCD_WriteXY(0, 1, "Azimut Counter...");
		LCD_WriteXY(0, 2, "Soll:");
		LCD_WriteXY(0, 3, "Ist:");
		state++;
		n = 0;
		break;

	case 7:
		if (refreshScreen()) {
			sprintf(s, "%ld ", n);
			LCD_WriteXY(7, 2, s);
			sprintf(s, "%ld ", azimuthAxis.counts);
			LCD_WriteXY(7, 3, s);
		}
		pulseDiffPort(AZ_FWD);
		n++;
		if (n == 1000) state++;
		HAL_Delay(1);
		break;
	case 8:
		if (refreshScreen()) {
			sprintf(s, "%ld ", n);
			LCD_WriteXY(7, 2, s);
			sprintf(s, "%ld ", azimuthAxis.counts);
			LCD_WriteXY(7, 3, s);
		}
		pulseDiffPort(AZ_REV);
		n--;
		if (n == 100) state++;
		HAL_Delay(1);
		break;
	case 9:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		if (azimuthAxis.counts == 100) {
			LCD_WriteXY(0, 1, "Azimut Counter: OK");
		} else {
			LCD_WriteXY(0, 1, "Az. Counter: Fehler");
			LCD_WriteXY(0, 2, "Soll: 100");
			sprintf(s, "Ist: %ld", azimuthAxis.counts);
			LCD_WriteXY(0, 3, s);
			result |= 1;
		}
		state++;
		break;
	case 10:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Elevation Counter Test
	 */

	case 11:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		LCD_WriteXY(0, 1, "Elevation Counter...");
		LCD_WriteXY(0, 2, "Soll:");
		LCD_WriteXY(0, 3, "Ist:");
		state++;
		n = 0;
		break;
	case 12:
		if (refreshScreen()) {
			sprintf(s, "%ld ", n);
			LCD_WriteXY(7, 2, s);
			sprintf(s, "%ld ", elevationAxis.counts);
			LCD_WriteXY(7, 3, s);
		}
		pulseDiffPort(ELV_FWD);
		n++;
		if (n == 1000) state++;
		HAL_Delay(1);
		break;
	case 13:
		if (refreshScreen()) {
			sprintf(s, "%ld ", n);
			LCD_WriteXY(7, 2, s);
			sprintf(s, "%ld ", elevationAxis.counts);
			LCD_WriteXY(7, 3, s);
		}
		pulseDiffPort(ELV_REV);
		n--;
		if (n == 100) state++;
		HAL_Delay(1);
		break;
	case 14:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		if (azimuthAxis.counts == 100) {
			LCD_WriteXY(0, 1, "Elv. Counter: OK");
		} else {
			LCD_WriteXY(0, 1, "Elv. Counter: Fehler");
			LCD_WriteXY(0, 2, "Soll: 100");
			sprintf(s, "Ist: %ld", elevationAxis.counts);
			LCD_WriteXY(0, 3, s);
			result |= 2;
		}
		state++;
		break;
	case 15:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Set-Puls Test
	 */

	case 16:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Setz-Impuls...");
		LCD_WriteXY(0, 1, "Azimut:");
		LCD_WriteXY(0, 2, "Elevation:");
		azimuthAxis.setpuls = 0;
		elevationAxis.setpuls = 0;
		pulseDiffPort(AZ_SET);
		pulseDiffPort(ELV_SET);
		state++;
		break;
	case 17:
		if (azimuthAxis.setpuls) {
			LCD_WriteXY(8, 1, "OK");
		} else {
			LCD_WriteXY(8, 1, "Fehler");
			result |= 4;
		}
		if (elevationAxis.setpuls) {
			LCD_WriteXY(11, 2, "OK");
		} else {
			LCD_WriteXY(11, 2, "Fehler");
			result |= 8;
		}
		state++;
		break;
	case 18:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Spannung
	 */

	case 19:
		statusLEDs = 0;
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		LCD_WriteXY(0, 1, "Spannung:");

		if ((sys.supplyVoltage < 3.2) || (sys.supplyVoltage > 3.4)) {
			LCD_WriteXY(10, 1, "Fehler");
			LCD_WriteXY(0, 2, "Soll: 3.3V");
			sprintf(s, "Ist: %2.2fV", sys.supplyVoltage);
			LCD_WriteXY(0, 3, s);
			result |= 0x10;
		} else {
			LCD_WriteXY(10, 1, "OK");
		}
		state++;
		break;
	case 20:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Batterie
	 */

	case 21:
		statusLEDs = 0;
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest");
		LCD_WriteXY(0, 1, "Batterie:");

		if ((sys.batteryVoltage < 2.5)) {
			LCD_WriteXY(10, 1, "Fehler");
			LCD_WriteXY(0, 2, "Soll: >2.5V");
			sprintf(s, "Ist: %2.2fV", sys.batteryVoltage);
			LCD_WriteXY(0, 3, s);
			result |= 0x20;
		} else {
			LCD_WriteXY(10, 1, "OK");
		}
		state++;
		break;
	case 22:
		if (checkButton(BTN_S3, BTN_SHORT)) state++;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Result
	 */

	case 23:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Selbsttest...");
		if (result > 0) {
			LCD_WriteXY(13, 0, "Fehler");
			sprintf(s, "Code: %d", result);
			LCD_WriteXY(0, 2, s);
			sys.selftest = SELFTEST_FAULT;
		} else {
			LCD_WriteXY(13, 0, "OK");
			sys.selftest = SELFTEST_OK;
		}
		state++;
	case 24:
		if (checkButton(BTN_S3, BTN_SHORT)) state = 100;
		if (refreshScreen()) {
			sprintf(s, "%cweiter%c",BO,BC);
			LCD_WriteXY(12, 3, s);
		}
		break;

	/*
	 * Restore Axis Data
	 */

	case 100:
		setupTestPorts(1);
		elevationAxis = elevationTmp;
		azimuthAxis = azimuthTmp;

		sys.status = SYS_STOP;
		state = -1;
	}

	return state;
}


int8_t setAutoRefPos(volatile axisData_t *axis, int8_t state)
{
	static int32_t deg;
	static int32_t min;
	static int32_t sec;
	uint32_t totalArcSec;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch(state) {
	case 0:
		totalArcSec = axis->refCounts * axis->secondsPerCount;
		deg = totalArcSec / 3600;
		min = (totalArcSec % 3600) / 60;
		sec = totalArcSec % 60;

		LCD_Clear();
		LCD_WriteXY(0, 0, "Auto. Referenz");
		sprintf(s, "Position %s:", axis->name);
		LCD_WriteXY(0, 1, s);
		LCD_WriteXY(0, 3, " abbruch     weiter ");
		state = 1;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "%c%3d\005%c %2d'  %2d\231  ", BO,deg,BC, min,sec);
			LCD_WriteXY(2, 2, s);
		}
		rotaryInput(&deg, axis->minDeg, axis->maxDeg, 1, 10);
		if (checkButton(BTN_ENC, BTN_SHORT)) deg = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 2;
		break;

	case 2:
		if (refreshScreen()) {
			sprintf(s, " %3d\005 %c%2d'%c %2d\231  ", deg,BO,min,BC,sec);
			LCD_WriteXY(2, 2, s);
		}
		rotaryInput(&min, 0, 59, 1, 15);
		if (checkButton(BTN_ENC, BTN_SHORT)) min = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 3;
		break;

	case 3:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d' %c%2d\231%c ", deg,min,BO,sec,BC);
			LCD_WriteXY(2, 2, s);
			LCD_WriteXY(0, 3, " abbruch     weiter ");
		}
		rotaryInput(&sec, 0, 59, 1, 15);
		if (checkButton(BTN_ENC, BTN_SHORT)) sec = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 2;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 4;
		break;

	case 4:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
			LCD_WriteXY(2, 2, s);
			sprintf(s, "%cabbruch%c    weiter ",BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 3;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 5;
		break;

	case 5:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
			LCD_WriteXY(2, 2, s);
			sprintf(s, " abbruch    %cweiter%c", BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 4;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = 6;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 1;
		break;

	case 6:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Auto. Referenz");
		sprintf(s, "Position %s", axis->name);
		LCD_WriteXY(0, 1, s);
		sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
		LCD_WriteXY(2, 2, s);
		LCD_WriteXY(0, 3, "ok?");
		state = 7;
		break;

	case 7:
		if (refreshScreen()) {
			sprintf(s, "%cnein%c ja ", BO,BC);
			LCD_WriteXY(9, 3, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = 8;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 9;
		break;

	case 8:

		state = 0;
		break;

	case 9:
		if (refreshScreen()) {
			sprintf(s, " nein %cja%c", BO,BC);
			LCD_WriteXY(9, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 7;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = 10;
		break;

	case 10:
		axis->refCounts = ceil((sec + min * 60 + deg * 3600) / axis->secondsPerCount);

		state = -1;
		break;

	case 11:
		if (refreshScreen()) {
			sprintf(s, "%cOK%c", BO, BC);
			LCD_WriteXY(8, 3, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;

	default:
		state = -1;
	}

	return state;
}

int8_t setReferencePos(volatile axisData_t *axis, int8_t state)
{
	static int32_t deg = 0;
	static int32_t min = 0;
	static int32_t sec = 0;
	static axisStatus_t status;
	char s[30];

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	if (state < 11) {
		if ((axis->velocity > MAX_MAN_REF_SPEED) || (sys.status == SYS_RUN)) {
			LCD_Clear();
			LCD_WriteXY(0, 0," Zum Referenzieren");
			LCD_WriteXY(0, 1," Teleskop anhalten");
			LCD_WriteXY(0, 2," und Z\173hler stoppen!");
			state = 11;
		}
	}

	switch(state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Referenzposition");
		sprintf(s, "%s setzen:", axis->name);
		LCD_WriteXY(0, 1, s);
		LCD_WriteXY(0, 3, " abbruch     weiter ");
		deg = (axis->dms.deg > axis->minDeg) ? axis->dms.deg : axis->minDeg;
		min = axis->dms.min;
		sec = axis->dms.sec;
		state = 1;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "%c%3d\005%c %2d'  %2d\231  ", BO,deg,BC, min,sec);
			LCD_WriteXY(2, 2, s);
		}
		rotaryInput(&deg, axis->minDeg, axis->maxDeg, 1, 10);
		if (checkButton(BTN_ENC, BTN_SHORT)) deg = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 5;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 2;
		break;

	case 2:
		if (refreshScreen()) {
			sprintf(s, " %3d\005 %c%2d'%c %2d\231  ", deg,BO,min,BC,sec);
			LCD_WriteXY(2, 2, s);
		}
		rotaryInput(&min, 0, 59, 1, 15);
		if (checkButton(BTN_ENC, BTN_SHORT)) min = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 3;
		break;

	case 3:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d' %c%2d\231%c ", deg,min,BO,sec,BC);
			LCD_WriteXY(2, 2, s);
			LCD_WriteXY(0, 3, " abbruch     weiter ");
		}
		rotaryInput(&sec, 0, 59, 1, 15);
		if (checkButton(BTN_ENC, BTN_SHORT)) sec = 0;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 2;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 4;
		break;

	case 4:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
			LCD_WriteXY(2, 2, s);
			sprintf(s, "%cabbruch%c    weiter ",BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 3;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 5;
		break;

	case 5:
		if (refreshScreen()) {
			sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
			LCD_WriteXY(2, 2, s);
			sprintf(s, " abbruch    %cweiter%c", BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 4;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = 6;
		break;

	case 6:
		LCD_Clear();
		sprintf(s, "%s Position", axis->name);
		LCD_WriteXY(0, 0, s);
		sprintf(s, " %3d\005  %2d'  %2d\231  ", deg,min,sec);
		LCD_WriteXY(2, 1, s);
		LCD_WriteXY(0, 2, "      setzen?");
		status = axis->status;
		axis->status = AXIS_STOPPED;
		state = 7;
		break;

	case 7:
		if (refreshScreen()) {
			sprintf(s, "%cnein%c           ja ", BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = 8;
		else if (checkButton(BTN_S1, BTN_SHORT)) state = 9;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 9;
		break;

	case 8:
		axis->status = status;
		state = 0;
		break;

	case 9:
		if (refreshScreen()) {
			sprintf(s, " nein           %cja%c", BO,BC);
			LCD_WriteXY(0, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 7;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 7;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = 10;
		break;

	case 10:
		axis->counts = ceil((sec + min * 60 + deg * 3600) / axis->secondsPerCount);
		axis->status = AXIS_REFERENCED;
		updatePosition(axis);
		state = -1;
		break;

	case 11:
		if (refreshScreen()) {
			sprintf(s, "%cOK%c", BO, BC);
			LCD_WriteXY(8, 3, s);
		}
		if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		break;

	default:
		state = -1;
	}

	return state;
}

int8_t setContrast(int8_t state)
{
	static int16_t contrast = 63;

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Kontrast");
		state = 1;
		break;
	case 1:
		if (refreshScreen()) {
			LCD_SetContrast(contrast);
			slider(contrast, 20, 63);
		}
		rotaryInput(&contrast, 20, 63, 1, 20);

		if (checkButton(BTN_S1, BTN_SHORT)) state = -1;
		else if (checkButton(BTN_S2, BTN_SHORT)) state = -1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = -1;
		break;
	default:
		state = -1;
	}

	return state;
}

int8_t setElevationRefPos(int8_t state)
{
	return setReferencePos(&elevationAxis, state);
}

int8_t setAzimuthRefPos(int8_t state)
{
	return setReferencePos(&azimuthAxis, state);
}

int8_t setAzimuthAutoRefPos(int8_t state)
{
	return setAutoRefPos(&azimuthAxis, state);
}

int8_t setElevationAutoRefPos(int8_t state)
{
	return setAutoRefPos(&elevationAxis, state);
}

int8_t StatusDisplay(int8_t state)
{
	char s[30];
	const char *azStat[] = {"AzIvd", "AzStp", "AzRef", "AzLst"};
	const char *elStat[] = {"ElIvd", "ElStp", "ElRef", "ElLst"};

	if (checkButton(BTN_S2, BTN_LONG))
		state = -1;

	switch (state) {
	case 0:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Position");
		LCD_WriteXY(0, 1, "Azimut:");
		LCD_WriteXY(0, 2, "Elev.:");
		LCD_WriteXY(0, 3, "Status:");
		state = 1;
		break;
	case 1:
		if (refreshScreen()) {
			sprintf(s, "%3d\005 %2d' %2d\231", azimuthAxis.dms.deg, azimuthAxis.dms.min, azimuthAxis.dms.sec);
			LCD_WriteXY(8, 1, ((azimuthAxis.dms.deg <= azimuthAxis.maxDeg && azimuthAxis.dms.deg >= azimuthAxis.minDeg) || lcdBlinkFlag) ? s : "            ");

			sprintf(s, "%3d\005 %2d' %2d\231", elevationAxis.dms.deg, elevationAxis.dms.min, elevationAxis.dms.sec);
			LCD_WriteXY(8, 2, ((elevationAxis.dms.deg <= elevationAxis.maxDeg && elevationAxis.dms.deg >= elevationAxis.minDeg) || lcdBlinkFlag) ? s : "            ");

			sprintf(s, "%s %s", azStat[azimuthAxis.status], elStat[elevationAxis.status]);
			LCD_WriteXY(8, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = -1;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 2;
		break;
	case 2:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Geschwindigkeit");
		LCD_WriteXY(0, 1, "Azimut:");
		LCD_WriteXY(0, 2, "Elev.:");
		LCD_WriteXY(0, 3, "Status:");
		state = 3;
		break;
	case 3:
		if (refreshScreen()) {
			sprintf(s, "%.2f\x99/s ", azimuthAxis.velocity);
			LCD_WriteXY(8, 1, s);
			sprintf(s, "%.2f\x99/s ", elevationAxis.velocity);
			LCD_WriteXY(8, 2, s);
			sprintf(s, "%s %s", azStat[azimuthAxis.status], elStat[elevationAxis.status]);
			LCD_WriteXY(8, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 0;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 4;
		break;
	case 4:
		LCD_Clear();
		LCD_WriteXY(0, 0, "Spannung:");
		LCD_WriteXY(0, 1, "Batterie:");
		LCD_WriteXY(0, 2, "Temperatur:");
		LCD_WriteXY(0, 3, "Selbsttest:");
		state = 5;
		break;
	case 5:
		if (refreshScreen()) {
			sprintf(s, "%2.2fV", sys.supplyVoltage);
			LCD_WriteXY(15, 0, (sys.supplyVoltage >= SUPPLY_LOW_THRES) || lcdBlinkFlag ? s : "     ");
			sprintf(s, "%2.2fV", sys.batteryVoltage);
			LCD_WriteXY(15, 1, (sys.batteryVoltage >= BAT_LOW_THRES) || lcdBlinkFlag ? s : "     ");
			sprintf(s, "%3.1f\005C", sys.temperature);
			LCD_WriteXY(14, 2, (sys.temperature <= TEMP_HIGH_THRES) || lcdBlinkFlag ? s : "      ");
			if (sys.selftest == SELFTEST_OK)
				LCD_WriteXY(14, 3, "    Ok");
			else if (sys.selftest == SELFTEST_UNKNOWN)
				LCD_WriteXY(14, 3, "------");
			else
				LCD_WriteXY(14, 3, "Fehler");
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 2;
		else if (checkButton(BTN_S3, BTN_SHORT)) state = 6;
		break;
	case 6:
		LCD_Clear();
		LCD_WriteXY(0, 0, "AzCount:");
		LCD_WriteXY(0, 1, "ElCount:");
		LCD_WriteXY(0, 2, "AzDelta:");
		LCD_WriteXY(0, 3, "ElDelta:");
		state = 7;
		break;
	case 7:
		if (refreshScreen()) {
			sprintf(s, "%ld    ", azimuthAxis.counts);
			LCD_WriteXY(9, 0, s);
			sprintf(s, "%ld    ", elevationAxis.counts);
			LCD_WriteXY(9, 1, s);
			sprintf(s, "%ld    ", azimuthAxis.deltaCounts);
			LCD_WriteXY(9, 2, s);
			sprintf(s, "%ld    ", elevationAxis.deltaCounts);
			LCD_WriteXY(9, 3, s);
		}
		if (checkButton(BTN_S1, BTN_SHORT)) state = 4;
		break;
	default:
		state = -1;
	}

	return state;
}

void rotaryInput(int32_t *var, int32_t min, int32_t max, uint8_t slowInc, uint16_t threshold)
{
	uint16_t inc;
	uint16_t velocity;

	velocity = getEncoderVelocity();
	if (velocity > threshold) inc = velocity;
	else inc = slowInc;

	if (checkEncoder(ROT_CW)) {
		if (*var + inc <= max)
			*var += inc;
		else
			*var = max;
	} else if (checkEncoder(ROT_CCW)) {
		if (*var - inc >= min)
			*var -= inc;
		else
			*var = min;
	}
}

void slider(int16_t var, int16_t min, int16_t max)
{
	char s[30];
	uint8_t n, bar;
	float step;

	step = 90.0 / abs(max - min);
	bar = (var - min) * step;

	s[0] = '\372';
	for(n = 0; n < bar / 5; n++)
		s[n+1] = '\320';
	s[n+1] = ' ';
	if (bar % 5) s[n+1] = '\325' - bar  % 5;
	for(++n; n < 18; n++)
		s[n+1] = ' ';
	s[19] = '\374';
	s[20] = '\0';

	LCD_WriteXY(0, 2, s);
}

void initMenu(void)
{
	uint8_t i;
	uint16_t tmp;

	for(i = 0; menuStruct[i].itemId != MENU_SEGORDER;i++);
	sprintf(menuStruct[i].text, "Seg.folge: \372%s\374", segOrder[sys.segOrder]);

	for(i = 0; menuStruct[i].itemId != MENU_REFMODE;i++);
	sprintf(menuStruct[i].text, "Modus: \372%s\374", refModes[azimuthAxis.refMode]);

	for(i = 0; menuStruct[i].itemId != MENU_REFSPEED;i++);
	sprintf(menuStruct[i].text, "Max.geschw.:%d\231", azimuthAxis.refMaxSpeed);

	tmp = ceil(azimuthAxis.refWarnCounts * azimuthAxis.secondsPerCount);
	for(i = 0; menuStruct[i].itemId != MENU_REFWARN;i++);
	sprintf(menuStruct[i].text, "Warngrenzw.:%d\231", tmp);
}

fptr_t Menu(void)
{
	static uint8_t curMenuId = 0;
	static uint8_t curItemId = 0;
	static uint8_t lastItemId = 255;
	static uint8_t cw = 0;
	static uint8_t ccw = 0;

	const uint8_t numItems = sizeof(menuStruct) / sizeof(menu_t);
	const menu_t *item;
	char s[20];
	int8_t begin, end;

	if (checkButton(BTN_ENC, BTN_SHORT) || checkButton(BTN_S2, BTN_SHORT)) {
		item = &menuStruct[curItemId];
		if (item->fctPtr == NULL) {
			curMenuId = item->subMenuId;
			curItemId = 0;
		} else {
			lastItemId = 255;
			return item->fctPtr;
		}
	}

	begin  = -1;
	for (uint8_t n = 0; n < numItems; n++) {
		item = &menuStruct[n];
		if (curMenuId == item->menuId) {
			if (begin == -1) {
				begin = n + 1;
				end = begin;
			} else {
				end++;
			}
		}
	}

	if (curItemId == 0)
		curItemId = begin;

	if (checkEncoder(ROT_CW)) {
		cw++;
	} else if (checkEncoder(ROT_CCW)) {
		ccw++;
	}

	if ((cw > 2) && (curItemId < end - 1)) {
		curItemId++; cw = 0;
	} else if ((ccw > 2) && (curItemId > begin)) {
		curItemId--; ccw = 0;
	}

	if (curItemId == lastItemId)
		return NULL;

	lastItemId = curItemId;

	LCD_Clear();
	LCD_WriteXY(0,0, menuStruct[begin - 1].text);

	if (end - begin > 3) {
		if (curItemId > begin + 1) {
			if (curItemId < end - 1)
				begin = curItemId - 1;
			else
				begin = curItemId - 2;
		}
		end = begin + 2;
	}

	for (uint8_t n = begin; n <= end; n++) {
		item = &menuStruct[n];
		if (curMenuId == item->menuId) {
			if (curItemId == n) {
				strcpy(s, "\020 ");
				strcat(s, item->text);
				for(uint8_t i = 0; i < 17 - strlen(item->text); i++)
					strcat(s, " ");
				strcat(s, "\021");
			} else {
				strcpy(s, "  ");
				strcat(s, item->text);
			}
			LCD_WriteXY(0,n - begin + 1, s);
		}
	}

	return NULL;
}

