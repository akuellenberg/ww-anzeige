/*
 * segment.c
 *
 *  Created on: Feb 6, 2021
 *      Author: ondre
 */

#include "main.h"
#include "system.h"
#include "segment.h"



//-----------------------------------------------------------------------------
// Maps decimal number to 7 segment pattern and shifts data into both display registers.
//-----------------------------------------------------------------------------
void setSegments(uint8_t numDisp1, uint8_t numDisp2, dpState_t dp)
{
	static const uint8_t map[12] = {0xf3, 0x60, 0xb5, 0xf4, 0x66, 0xd6, 0xd7, 0x70, 0xf7, 0xf6, 0x00, 0x04};

	for(int i = 0; i < 8; i++) {
		if (((map[numDisp1]|(dp << 3)) >> i) & 1)
			HAL_GPIO_WritePin(SEG_SDI_GPIO_Port, SEG_SDI_Pin, GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(SEG_SDI_GPIO_Port, SEG_SDI_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(SEG_CLK_GPIO_Port, SEG_CLK_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(SEG_CLK_GPIO_Port, SEG_CLK_Pin, GPIO_PIN_RESET);
	}
	for(int i = 0; i < 8; i++) {
		if (((map[numDisp2]|(dp << 3)) >> i) & 1)
			HAL_GPIO_WritePin(SEG_SDI_GPIO_Port, SEG_SDI_Pin, GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(SEG_SDI_GPIO_Port, SEG_SDI_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(SEG_CLK_GPIO_Port, SEG_CLK_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(SEG_CLK_GPIO_Port, SEG_CLK_Pin, GPIO_PIN_RESET);
	}
	HAL_GPIO_WritePin(SEG_LE_GPIO_Port, SEG_LE_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(SEG_LE_GPIO_Port, SEG_LE_Pin, GPIO_PIN_RESET);
}

//-----------------------------------------------------------------------------
// Updates 7 segment displays
//-----------------------------------------------------------------------------
void displayMultiplexer(volatile uint8_t *segDisp1, volatile uint8_t *segDisp2)
{
	static uint8_t n = 0;
	dpState_t dp;

	// decimal point used as separator: ddd.mm.ss
	if (sys.segOrder == SEG_ORDER_MSBFIRST) {
		if ((n == 1) || (n == 3))
			dp = DOT_ON;
		else
			dp = DOT_OFF;
	} else {
		if ((n == 2) || (n == 4))
			dp = DOT_ON;
		else
			dp = DOT_OFF;
	}

	// disable outputs on segment driver to reduce ghosting effect (active low)
	HAL_GPIO_WritePin(nSEG_OE_GPIO_Port, nSEG_OE_Pin, GPIO_PIN_SET);

	// load shift register with segment data
	setSegments(segDisp1[n], segDisp2[n], dp);

	// anode register is controlled by a 3-to-8 line decoder
	if (n & 1)
		HAL_GPIO_WritePin(SEG_A0_GPIO_Port, SEG_A0_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(SEG_A0_GPIO_Port, SEG_A0_Pin, GPIO_PIN_RESET);

	if (n & 2)
		HAL_GPIO_WritePin(SEG_A1_GPIO_Port, SEG_A1_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(SEG_A1_GPIO_Port, SEG_A1_Pin, GPIO_PIN_RESET);

	if (n & 4)
		HAL_GPIO_WritePin(SEG_A2_GPIO_Port, SEG_A2_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(SEG_A2_GPIO_Port, SEG_A2_Pin, GPIO_PIN_RESET);

	// enable outputs on segment driver
	if (!flashSegments) HAL_GPIO_WritePin(nSEG_OE_GPIO_Port, nSEG_OE_Pin, GPIO_PIN_RESET);

	// update counter
	if (++n == 7) n = 0;
}
