/*
 * mystuff.c
 *
 *  Created on: Jan 20, 2021
 *      Author: ondre
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "main.h"
#include "system.h"
#include "segment.h"
#include "fpcontrol.h"
#include "lcd.h"

volatile axisData_t azimuthAxis = {
		.counts				= 0,
		.lastCounts			= 0,
		.refCounts			= 1800000,
		.refWarnCounts		= 166,
		.refMaxSpeed		= 1000,
		.totalArcSec		= 0,
		.secondsPerCount	= 0.5,
		.minDeg				= 50,
		.maxDeg				= 540,
		.name				= "Azimut",
		.status				= AXIS_INVALID,
		.setpuls			= 0,
		.refMode			= REFMODE_OFF
};

volatile axisData_t elevationAxis = {
		.counts				= 0,
		.lastCounts			= 0,
		.refCounts			= 450000,
		.refWarnCounts		= 166,
		.refMaxSpeed		= 1000,
		.totalArcSec		= 0,
		.secondsPerCount	= 0.36,
		.minDeg				= 7,
		.maxDeg				= 95,
		.name				= "Elevation",
		.status				= AXIS_INVALID,
		.setpuls			= 0,
		.refMode			= REFMODE_OFF
};

system_t sys = {
		.batteryVoltage		= 0,
		.supplyVoltage		= 0,
		.temperature		= 0,
		.status				= SYS_START,
		.selftest			= SELFTEST_UNKNOWN,
		.segOrder			= SEG_ORDER_MSBFIRST
};

const char *refModes[] = {"aus", "warn", "auto"};
const char *segOrder[] = {"6..0", "0..6"};
static uint8_t store = 0;

void setStore(void)
{
	store = 1;
}

void clearStore(void)
{
	store = 0;
}

void storeSRAM(void)
{
	if (! store)
		return;
	uint32_t *sram = (uint32_t *) BKPSRAM_BASE;
	uint8_t size = sizeof(axisData_t);
	memcpy(sram, &azimuthAxis, size);
	memcpy(sram + size, &elevationAxis, size);
}

void restoreSRAM(void)
{
	uint32_t *sram = (uint32_t *) BKPSRAM_BASE;
	uint8_t size = sizeof(axisData_t);
	memcpy(&azimuthAxis, sram, size);
	memcpy(&elevationAxis, sram + size, size);
}

void autoReference(volatile axisData_t *axis) {
	if (axis->refMode == REFMODE_AUTO) {
		if (axis->velocity <= axis->refMaxSpeed) {
			axis->counts = axis->refCounts;
			axis->status = AXIS_REFERENCED;
		} else {
			axis->status = AXIS_LOSTREF;
		}
	} else if (axis->refMode == REFMODE_WARN) {
		if (abs(axis->counts - axis->refCounts) > axis->refWarnCounts) {
			axis->status = AXIS_LOSTREF;
		}
	}
}

void counterControl(uint8_t axis, uint8_t enable)
{
	if (axis == ELEVATION) {
		if (enable) {
			HAL_NVIC_EnableIRQ(EXTI0_IRQn);
			HAL_NVIC_EnableIRQ(EXTI1_IRQn);
			HAL_NVIC_EnableIRQ(EXTI2_IRQn);
		} else {
			HAL_NVIC_DisableIRQ(EXTI0_IRQn);
			HAL_NVIC_DisableIRQ(EXTI1_IRQn);
			HAL_NVIC_DisableIRQ(EXTI2_IRQn);
		}
	} else {
		if (enable) {
			HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		} else {
			HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
		}
	}
}

void setSystemStatus(uint8_t status) {
	if (sys.status == status)
		return;

	sys.status = status;

	if (sys.status == SYS_RUN) {
		counterControl(AZIMUT, 1);
		counterControl(ELEVATION, 1);
		statusLEDs &= ~STATUS_STOP;
		statusLEDs |= STATUS_RUN;
		flashState = FLASH_OFF;
	} else if (sys.status == SYS_STOP) {
		counterControl(AZIMUT, 0);
		counterControl(ELEVATION, 0);
		statusLEDs |= STATUS_STOP;
		statusLEDs &= ~STATUS_RUN;
		flashState = FLASH_ON;
	}
}

void checkStatus(void)
{
	if (sys.status == SYS_SELFTEST)
		return;

	if ((sys.status == SYS_START) && (azimuthAxis.status == AXIS_REFERENCED) && (elevationAxis.status == AXIS_REFERENCED)) {
		setSystemStatus(SYS_RUN);
	} else if ((azimuthAxis.status == AXIS_INVALID) && (elevationAxis.status == AXIS_INVALID)) {
		setSystemStatus(SYS_STOP);
	} else if ((azimuthAxis.status == AXIS_LOSTREF) || (elevationAxis.status == AXIS_LOSTREF)) {
		flashState = FLASH_ON;
	}

	statusLEDs &= ~(STATUS_EL_FWD | STATUS_EL_REV | STATUS_EL_SET | STATUS_AZ_FWD | STATUS_AZ_REV | STATUS_AZ_SET);

}

//-----------------------------------------------------------------------------
// Converts encoder counts to degrees, minutes and arc-seconds
//-----------------------------------------------------------------------------
void updatePosition(volatile axisData_t *axis)
{
	axis->totalArcSec = axis->counts * axis->secondsPerCount;
	
	axis->dms.deg = axis->totalArcSec / 3600;
	axis->dms.min = (axis->totalArcSec % 3600) / 60;
	axis->dms.sec = axis->totalArcSec % 60;

	if (sys.segOrder == SEG_ORDER_LSBFIRST) {
		axis->segments[0] = axis->dms.deg / 100;
		axis->segments[1] = (axis->dms.deg % 100) / 10;
		axis->segments[2] = (axis->dms.deg % 100) % 10;
		axis->segments[3] = axis->dms.min / 10;
		axis->segments[4] = axis->dms.min % 10;
		axis->segments[5] = axis->dms.sec / 10;
		axis->segments[6] = axis->dms.sec % 10;
		if (axis->segments[0] == 0) {
			axis->segments[0] = 10;
			if (axis->segments[1] == 0)
				axis->segments[1] = 10;
		}
	} else {
		axis->segments[6] = axis->dms.deg / 100;
		axis->segments[5] = (axis->dms.deg % 100) / 10;
		axis->segments[4] = (axis->dms.deg % 100) % 10;
		axis->segments[3] = axis->dms.min / 10;
		axis->segments[2] = axis->dms.min % 10;
		axis->segments[1] = axis->dms.sec / 10;
		axis->segments[0] = axis->dms.sec % 10;
		if (axis->segments[6] == 0) {
			axis->segments[6] = 10;
			if (axis->segments[5] == 0)
				axis->segments[5] = 10;
		}
	}

}

void updateVelocity(volatile axisData_t *axis)
{
	axis->deltaCounts = abs(axis->counts - axis->lastCounts);
	axis->velocity = (axis->deltaCounts * axis->secondsPerCount);
	axis->lastCounts = axis->counts;
}

//-----------------------------------------------------------------------------
// Setting up initial condition for differential test ports
//-----------------------------------------------------------------------------
void setupTestPorts(uint8_t mode)
{
	if (mode == 1) {
		HAL_GPIO_WritePin(TEST_DIFF_A0_GPIO_Port, TEST_DIFF_A0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A0_GPIO_Port, nTEST_DIFF_A0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_A1_GPIO_Port, TEST_DIFF_A1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A1_GPIO_Port, nTEST_DIFF_A1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_A2_GPIO_Port, TEST_DIFF_A2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A2_GPIO_Port, nTEST_DIFF_A2_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_B0_GPIO_Port, TEST_DIFF_B0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B0_GPIO_Port, nTEST_DIFF_B0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_B1_GPIO_Port, TEST_DIFF_B1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B1_GPIO_Port, nTEST_DIFF_B1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_B2_GPIO_Port, TEST_DIFF_B2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B2_GPIO_Port, nTEST_DIFF_B2_Pin, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(TEST_DIFF_A0_GPIO_Port, TEST_DIFF_A0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A0_GPIO_Port, nTEST_DIFF_A0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_A1_GPIO_Port, TEST_DIFF_A1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A1_GPIO_Port, nTEST_DIFF_A1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_A2_GPIO_Port, TEST_DIFF_A2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A2_GPIO_Port, nTEST_DIFF_A2_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_B0_GPIO_Port, TEST_DIFF_B0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B0_GPIO_Port, nTEST_DIFF_B0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_B1_GPIO_Port, TEST_DIFF_B1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B1_GPIO_Port, nTEST_DIFF_B1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(TEST_DIFF_B2_GPIO_Port, TEST_DIFF_B2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B2_GPIO_Port, nTEST_DIFF_B2_Pin, GPIO_PIN_RESET);
	}
}

//-----------------------------------------------------------------------------
// Injects test signal into differtial input ports
//-----------------------------------------------------------------------------
void pulseDiffPort(uint8_t port)
{
	switch(port) {
	case ELV_FWD:
		HAL_GPIO_WritePin(TEST_DIFF_A1_GPIO_Port, TEST_DIFF_A1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A1_GPIO_Port, nTEST_DIFF_A1_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_A1_GPIO_Port, TEST_DIFF_A1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A1_GPIO_Port, nTEST_DIFF_A1_Pin, GPIO_PIN_SET);
		break;
	case ELV_REV:
		HAL_GPIO_WritePin(TEST_DIFF_A2_GPIO_Port, TEST_DIFF_A2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A2_GPIO_Port, nTEST_DIFF_A2_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_A2_GPIO_Port, TEST_DIFF_A2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A2_GPIO_Port, nTEST_DIFF_A2_Pin, GPIO_PIN_SET);
		break;
	case ELV_SET:
		HAL_GPIO_WritePin(TEST_DIFF_A0_GPIO_Port, TEST_DIFF_A0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_A0_GPIO_Port, nTEST_DIFF_A0_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_A0_GPIO_Port, TEST_DIFF_A0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_A0_GPIO_Port, nTEST_DIFF_A0_Pin, GPIO_PIN_SET);
		break;

	case AZ_FWD:
		HAL_GPIO_WritePin(TEST_DIFF_B1_GPIO_Port, TEST_DIFF_B1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B1_GPIO_Port, nTEST_DIFF_B1_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_B1_GPIO_Port, TEST_DIFF_B1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B1_GPIO_Port, nTEST_DIFF_B1_Pin, GPIO_PIN_SET);
		break;
	case AZ_REV:
		HAL_GPIO_WritePin(TEST_DIFF_B2_GPIO_Port, TEST_DIFF_B2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B2_GPIO_Port, nTEST_DIFF_B2_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_B2_GPIO_Port, TEST_DIFF_B2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B2_GPIO_Port, nTEST_DIFF_B2_Pin, GPIO_PIN_SET);
		break;
	case AZ_SET:
		HAL_GPIO_WritePin(TEST_DIFF_B0_GPIO_Port, TEST_DIFF_B0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(nTEST_DIFF_B0_GPIO_Port, nTEST_DIFF_B0_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(TEST_DIFF_B0_GPIO_Port, TEST_DIFF_B0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(nTEST_DIFF_B0_GPIO_Port, nTEST_DIFF_B0_Pin, GPIO_PIN_SET);
		break;
	}
}


//-----------------------------------------------------------------------------
// External GPIO interrupt callback.
// Implements pulse counter and toggles frontpanel status LEDs
//-----------------------------------------------------------------------------
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == A0_SE_Pin)
	{
		statusLEDs ^= STATUS_EL_SET;
		elevationAxis.setpuls = 1;
		autoReference(&elevationAxis);
	}
	if (GPIO_Pin == A1_SE_Pin)
	{
		statusLEDs ^= STATUS_EL_FWD;
		elevationAxis.counts++;
	}
	if (GPIO_Pin == A2_SE_Pin)
	{
		statusLEDs |= STATUS_EL_REV;
		elevationAxis.counts--;
	}
	if (GPIO_Pin == B0_SE_Pin)
	{
		statusLEDs ^= STATUS_AZ_SET;
		azimuthAxis.setpuls = 1;
		autoReference(&azimuthAxis);
	}
	if (GPIO_Pin == B1_SE_Pin)
	{
		statusLEDs ^= STATUS_AZ_FWD;
		azimuthAxis.counts++;
	}
	if (GPIO_Pin == B2_SE_Pin)
	{
		statusLEDs |= STATUS_AZ_REV;
		azimuthAxis.counts--;
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	ADC_Common_TypeDef *ADC_Common;
	static uint32_t n;

	// Timer 1: 7 segment display anode multiplexer. Period: 5 ms.
	if (htim->Instance == TIM1)
	{
		displayMultiplexer(elevationAxis.segments, azimuthAxis.segments);
	}

	// Timer 2: Period: 1s.
	if (htim->Instance == TIM2)
	{
		HAL_GPIO_TogglePin(STATUS_LED_GPIO_Port, STATUS_LED_Pin);

		updateVelocity(&elevationAxis);
		updateVelocity(&azimuthAxis);

		ADC_Common = ADC_COMMON_REGISTER(hadc);
		ADC_Common->CCR |= ADC_CCR_VBATE;

		checkStatus();
	}

	// Timer 3: Encoder ISR. Period: 1 ms.
	if (htim->Instance == TIM3)
	{
		encoderISR();
		updateStatusLEDs();
	}

	// Timer 4: Frontpanel I/O. Period: 10 ms.
	if (htim->Instance == TIM4)
	{
		if (sys.status != SYS_SELFTEST) {
			updatePosition(&elevationAxis);
			updatePosition(&azimuthAxis);
			storeSRAM();
		}

		pushButtonISR();

		if (n % 25 == 0) {
			encoderSpeedISR();
			if (flashState == FLASH_ON) flashSegments ^= flashState;
			else flashSegments = 0;
		}
		if (n % 50 == 0) {
			gScreenRefresh = 1;
			LCD_BlinkingBracketISR();
		}
		n++;
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	ADC_Common_TypeDef *ADC_Common;

	ADC_Common = ADC_COMMON_REGISTER(hadc);
	sys.temperature = (((3.3 * (float)adcDmaBuffer[0] / 4095) - 0.76) / 0.0025) + 25;
	sys.supplyVoltage = 1.21 * 4095 / (float)adcDmaBuffer[1];
	sys.batteryVoltage = 3.3 * 2 * (float)adcDmaBuffer[2] / 4095;
	ADC_Common->CCR &= ~ADC_CCR_VBATE;
}

void ___TestOutput(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin = TEST_DIFF_A2_Pin|TEST_DIFF_A1_Pin|TEST_DIFF_A0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = nTEST_DIFF_B1_Pin|TEST_DIFF_B1_Pin|TEST_DIFF_B0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = TEST_DIFF_B2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = nTEST_DIFF_A2_Pin|nTEST_DIFF_A1_Pin|nTEST_DIFF_A0_Pin|nTEST_DIFF_B2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = nTEST_DIFF_B1_Pin|nTEST_DIFF_B0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

