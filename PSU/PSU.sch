EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2700 2300 0    50   ~ 0
N
Text Label 2700 1900 0    50   ~ 0
L
Wire Wire Line
	3700 2300 3850 2300
Wire Wire Line
	3850 1900 3850 1600
Wire Wire Line
	3700 1900 3850 1900
Wire Wire Line
	5250 2100 5250 2400
$Comp
L Device:Fuse F1
U 1 1 5FA791A9
P 4250 1600
F 0 "F1" V 4150 1600 50  0000 C CNN
F 1 "1.25A" V 4350 1600 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" V 4180 1600 50  0001 C CNN
F 3 "~" H 4250 1600 50  0001 C CNN
F 4 "0031.8201" H 4250 1600 50  0001 C CNN "Manufacturer"
F 5 "693-0031.8201" H 4250 1600 50  0001 C CNN "Mouser"
F 6 "176-9047" H 4250 1600 50  0001 C CNN "RS"
	1    4250 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 1600 4100 1600
Wire Wire Line
	4400 1600 4650 1600
Connection ~ 5250 2100
Wire Wire Line
	3850 2300 3850 2600
Wire Wire Line
	4350 2100 4300 2100
Wire Wire Line
	4650 2600 4650 2400
Wire Wire Line
	4650 1600 4650 1800
Wire Wire Line
	3850 2600 4650 2600
Wire Wire Line
	4300 2100 4300 3000
Wire Wire Line
	5250 3000 5250 2700
Connection ~ 5250 3000
Wire Wire Line
	5800 3000 5800 2700
Wire Wire Line
	5800 2100 5800 2400
Wire Wire Line
	5250 2100 5800 2100
$Comp
L Device:CP C1
U 1 1 6025F846
P 5250 2550
F 0 "C1" H 5368 2596 50  0000 L CNN
F 1 "1500uF" H 5368 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 5288 2400 50  0001 C CNN
F 3 "~" H 5250 2550 50  0001 C CNN
F 4 "SLP152M063A3P3 " H 5250 2550 50  0001 C CNN "Manufacturer"
F 5 "598-SLP152M063A3P3" H 5250 2550 50  0001 C CNN "Mouser"
	1    5250 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 6026004D
P 5800 2550
F 0 "C2" H 5918 2596 50  0000 L CNN
F 1 "1500uF" H 5918 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 5838 2400 50  0001 C CNN
F 3 "~" H 5800 2550 50  0001 C CNN
F 4 "SLP152M063A3P3 " H 5800 2550 50  0001 C CNN "Manufacturer"
F 5 "598-SLP152M063A3P3" H 5800 2550 50  0001 C CNN "Mouser"
	1    5800 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:Transformer_1P_1S T1
U 1 1 600733B0
P 3300 2100
F 0 "T1" H 3300 2400 50  0000 C CNN
F 1 "Block VC 16/1/6" H 3300 1800 50  0000 C CNN
F 2 "ak:Block_VC_16" H 3300 2100 50  0001 C CNN
F 3 "~" H 3300 2100 50  0001 C CNN
F 4 "732-0553" H 3300 2100 50  0001 C CNN "RS"
F 5 "VC 16/1/6" H 3300 2100 50  0001 C CNN "Manufacturer"
	1    3300 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2100 5250 2100
Wire Wire Line
	7550 2500 7500 2500
Wire Wire Line
	7500 2500 7500 2100
Wire Wire Line
	7550 2600 7500 2600
Wire Wire Line
	7500 2600 7500 3000
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 6010F829
P 7750 2500
F 0 "J2" H 7830 2446 50  0000 L CNN
F 1 "Conn_01x02" H 7830 2401 50  0001 L CNN
F 2 "ak:Molex_022112022" H 7750 2500 50  0001 C CNN
F 3 "~" H 7750 2500 50  0001 C CNN
F 4 "22-11-2022" H 7750 2500 50  0001 C CNN "Manufacturer"
F 5 "538-22-11-2022" H 7750 2500 50  0001 C CNN "Mouser"
F 6 "679-5515" H 7750 2500 50  0001 C CNN "RS"
	1    7750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2100 6350 2400
Connection ~ 6350 2100
Wire Wire Line
	6350 3000 6350 2700
Connection ~ 6350 3000
Wire Wire Line
	6350 3000 6900 3000
Wire Wire Line
	6900 3000 6900 2700
Wire Wire Line
	6900 2100 6900 2400
Wire Wire Line
	6350 2100 6900 2100
$Comp
L Device:CP C3
U 1 1 5FDE6487
P 6350 2550
F 0 "C3" H 6468 2596 50  0000 L CNN
F 1 "1500uF" H 6468 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 6388 2400 50  0001 C CNN
F 3 "~" H 6350 2550 50  0001 C CNN
F 4 "SLP152M063A3P3 " H 6350 2550 50  0001 C CNN "Manufacturer"
F 5 "598-SLP152M063A3P3" H 6350 2550 50  0001 C CNN "Mouser"
	1    6350 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5FDE648F
P 6900 2550
F 0 "C4" H 7018 2596 50  0000 L CNN
F 1 "1500uF" H 7018 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 6938 2400 50  0001 C CNN
F 3 "~" H 6900 2550 50  0001 C CNN
F 4 "SLP152M063A3P3 " H 6900 2550 50  0001 C CNN "Manufacturer"
F 5 "598-SLP152M063A3P3" H 6900 2550 50  0001 C CNN "Mouser"
	1    6900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2100 6350 2100
Wire Wire Line
	5250 3000 5800 3000
Connection ~ 5800 2100
Wire Wire Line
	4300 3000 5250 3000
Wire Wire Line
	6900 2100 7500 2100
Connection ~ 6900 2100
Wire Wire Line
	7500 3000 6900 3000
Connection ~ 6900 3000
Connection ~ 5800 3000
Wire Wire Line
	5800 3000 6350 3000
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5FDFA0A3
P 2250 2050
F 0 "J1" H 2168 2175 50  0000 C CNN
F 1 "Conn_01x02" H 2168 2176 50  0001 C CNN
F 2 "ak:PC_1017503_2POS" H 2250 2050 50  0001 C CNN
F 3 "~" H 2250 2050 50  0001 C CNN
F 4 "1017503 " H 2250 2050 50  0001 C CNN "Manufacturer"
F 5 "651-1017503" H 2250 2050 50  0001 C CNN "Mouser"
F 6 "175-3905" H 2250 2050 50  0001 C CNN "RS"
	1    2250 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2450 2050 2500 2050
Wire Wire Line
	2500 2050 2500 1900
Wire Wire Line
	2450 2150 2500 2150
Wire Wire Line
	2500 2150 2500 2300
$Comp
L Mechanical:MountingHole H1
U 1 1 5FDFDC8E
P 4300 7050
F 0 "H1" H 4400 7050 50  0000 L CNN
F 1 "MountingHole" H 4400 7005 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4300 7050 50  0001 C CNN
F 3 "~" H 4300 7050 50  0001 C CNN
	1    4300 7050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FDFE0F0
P 4300 7250
F 0 "H2" H 4400 7250 50  0000 L CNN
F 1 "MountingHole" H 4400 7205 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4300 7250 50  0001 C CNN
F 3 "~" H 4300 7250 50  0001 C CNN
	1    4300 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FDFEF81
P 4700 7050
F 0 "H3" H 4800 7050 50  0000 L CNN
F 1 "MountingHole" H 4800 7005 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4700 7050 50  0001 C CNN
F 3 "~" H 4700 7050 50  0001 C CNN
	1    4700 7050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FDFEF87
P 4700 7250
F 0 "H4" H 4800 7250 50  0000 L CNN
F 1 "MountingHole" H 4800 7205 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4700 7250 50  0001 C CNN
F 3 "~" H 4700 7250 50  0001 C CNN
	1    4700 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 1900 2900 1900
Wire Wire Line
	2500 2300 2900 2300
$Comp
L Device:D_Bridge_+AA- D1
U 1 1 5FB45548
P 4650 2100
F 0 "D1" H 4994 2146 50  0000 L CNN
F 1 "D_Bridge_+AA-" H 4994 2055 50  0000 L CNN
F 2 "Diode_THT:Diode_Bridge_Vishay_GBU" H 4650 2100 50  0001 C CNN
F 3 "~" H 4650 2100 50  0001 C CNN
F 4 "GBU4M" H 4650 2100 50  0001 C CNN "Manufacturer"
F 5 "512-GBU4M" H 4650 2100 50  0001 C CNN "Mouser"
F 6 "700-5431" H 4650 2100 50  0001 C CNN "RS"
	1    4650 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FB4C172
P 7500 3050
F 0 "#PWR02" H 7500 2800 50  0001 C CNN
F 1 "GND" H 7505 2877 50  0001 C CNN
F 2 "" H 7500 3050 50  0001 C CNN
F 3 "" H 7500 3050 50  0001 C CNN
	1    7500 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3050 7500 3000
Connection ~ 7500 3000
$Comp
L power:VDC #PWR01
U 1 1 5FB4D8BB
P 7500 2050
F 0 "#PWR01" H 7500 1950 50  0001 C CNN
F 1 "VDC" H 7515 2223 50  0000 C CNN
F 2 "" H 7500 2050 50  0001 C CNN
F 3 "" H 7500 2050 50  0001 C CNN
	1    7500 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2050 7500 2100
Connection ~ 7500 2100
$EndSCHEMATC
