EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7800 2050 7800 1950
Connection ~ 7800 2350
Wire Wire Line
	8050 2350 7800 2350
Wire Wire Line
	7800 2250 7800 2350
$Comp
L Device:R_Small R53
U 1 1 5ED5AF61
P 8150 2350
F 0 "R53" V 7954 2350 50  0000 C CNN
F 1 "100R" V 8045 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8150 2350 50  0001 C CNN
F 3 "~" H 8150 2350 50  0001 C CNN
	1    8150 2350
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Dual JP13
U 1 1 5FB79014
P 5150 1950
F 0 "JP13" V 5150 2051 50  0000 L CNN
F 1 "Jumper_NC_Dual" H 5150 2188 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5150 1950 50  0001 C CNN
F 3 "~" H 5150 1950 50  0001 C CNN
	1    5150 1950
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J12
U 1 1 5FBA4FD3
P 4850 2550
F 0 "J12" H 4768 2675 50  0000 C CNN
F 1 "Conn_01x02" H 4768 2676 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4850 2550 50  0001 C CNN
F 3 "~" H 4850 2550 50  0001 C CNN
	1    4850 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 1450 5500 1950
Wire Wire Line
	5150 1450 5150 1700
Wire Wire Line
	5250 1950 5500 1950
Connection ~ 5500 1950
Text Label 1100 2150 0    50   ~ 0
N
Text Label 1100 1750 0    50   ~ 0
L
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 5FEE8E59
P 900 2150
F 0 "J11" H 1050 2150 50  0000 C CNN
F 1 "Conn_01x01" H 818 2276 50  0001 C CNN
F 2 "Connector_Pin:Pin_D1.3mm_L10.0mm_W3.5mm_Flat" H 900 2150 50  0001 C CNN
F 3 "~" H 900 2150 50  0001 C CNN
	1    900  2150
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 5FEE8479
P 900 1750
F 0 "J10" H 1050 1750 50  0000 C CNN
F 1 "Conn_01x01" H 818 1876 50  0001 C CNN
F 2 "Connector_Pin:Pin_D1.3mm_L10.0mm_W3.5mm_Flat" H 900 1750 50  0001 C CNN
F 3 "~" H 900 1750 50  0001 C CNN
	1    900  1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1100 2150 1300 2150
Wire Wire Line
	1100 1750 1300 1750
Wire Wire Line
	2100 2150 2250 2150
Wire Wire Line
	2250 1750 2250 1450
Wire Wire Line
	2100 1750 2250 1750
$Comp
L Device:Transformer_1P_1S T1
U 1 1 5FA4EBCB
P 1700 1950
F 0 "T1" H 1700 2250 50  0000 C CNN
F 1 "Block VC 10/1/9" H 1700 1650 50  0000 C CNN
F 2 "ak:Block-VC_16_1_6" H 1700 1950 50  0001 C CNN
F 3 "~" H 1700 1950 50  0001 C CNN
F 4 "732-0553" H 1700 1950 50  0001 C CNN "RS"
F 5 "VC 16/1/6" H 1700 1950 50  0001 C CNN "Manufacturer"
	1    1700 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1450 3650 2000
$Comp
L Device:Fuse F1
U 1 1 5FA791A9
P 2650 1450
F 0 "F1" V 2550 1450 50  0000 C CNN
F 1 "1.25A" V 2750 1450 50  0000 C CNN
F 2 "ak:Fuseholder_SMD_Littelfuse_154" V 2580 1450 50  0001 C CNN
F 3 "~" H 2650 1450 50  0001 C CNN
F 4 "01541.25DR" H 2650 1450 50  0001 C CNN "Manufacturer"
F 5 "576-01541.25DR" H 2650 1450 50  0001 C CNN "Mouser"
	1    2650 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 1450 2500 1450
Wire Wire Line
	2800 1450 3050 1450
Wire Wire Line
	3500 1450 3650 1450
Connection ~ 3650 1450
Wire Wire Line
	5050 2550 5150 2550
Wire Wire Line
	5150 2550 5150 2450
Wire Wire Line
	5050 2650 5150 2650
Wire Wire Line
	5150 2250 5150 2200
$Comp
L Device:D_Small D19
U 1 1 5FA5D0BC
P 5150 2350
F 0 "D19" V 5104 2420 50  0000 L CNN
F 1 "S1M" V 5195 2420 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" V 5150 2350 50  0001 C CNN
F 3 "~" V 5150 2350 50  0001 C CNN
F 4 "S1M" H 5150 2350 50  0001 C CNN "Manufacturer"
F 5 "512-S1M " H 5150 2350 50  0001 C CNN "Mouser"
	1    5150 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 2150 2250 2450
Wire Wire Line
	3500 1950 3500 1450
Wire Wire Line
	3350 1950 3500 1950
Wire Wire Line
	2750 1950 2700 1950
Wire Wire Line
	3050 2450 3050 2250
Wire Wire Line
	3050 1450 3050 1650
Wire Wire Line
	2250 2450 3050 2450
$Comp
L Device:D_Bridge_+-AA BR1
U 1 1 5F9ADCED
P 3050 1950
F 0 "BR1" H 3200 1750 50  0000 L CNN
F 1 "D_Bridge_+-AA" H 3394 1905 50  0001 L CNN
F 2 "Diode_SMD:Diode_Bridge_Vishay_DFS" H 3050 1950 50  0001 C CNN
F 3 "~" H 3050 1950 50  0001 C CNN
F 4 "512-DF02S " H 3050 1950 50  0001 C CNN "Mouser"
F 5 "DF02S " H 3050 1950 50  0001 C CNN "Manufacturer"
	1    3050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1950 2700 2850
Connection ~ 5150 2850
Wire Wire Line
	5150 2650 5150 2850
Wire Wire Line
	3650 2850 3650 2300
Connection ~ 3650 2850
Wire Wire Line
	3050 2900 3050 2850
Connection ~ 3050 2850
Wire Wire Line
	2700 2850 3050 2850
$Comp
L power:GND #PWR082
U 1 1 5F4FC886
P 3050 2900
F 0 "#PWR082" H 3050 2650 50  0001 C CNN
F 1 "GND" H 3055 2727 50  0001 C CNN
F 2 "" H 3050 2900 50  0001 C CNN
F 3 "" H 3050 2900 50  0001 C CNN
	1    3050 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2850 3650 2850
Connection ~ 7800 2850
Wire Wire Line
	7800 2650 7800 2850
Wire Wire Line
	7800 2350 7800 2450
Text Notes 4600 2800 0    79   ~ 0
DC Input
Text Notes 4650 1750 0    50   ~ 0
AC/DC input\nselector
Text Notes 600  2000 0    79   ~ 0
AC Input
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F3C9C1D
P 1400 6900
F 0 "H1" V 1450 7150 50  0000 C CNN
F 1 "MountingHole_Pad" V 1350 7400 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1400 6900 50  0001 C CNN
F 3 "~" H 1400 6900 50  0001 C CNN
	1    1400 6900
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F3CADB5
P 1400 7100
F 0 "H2" V 1450 7350 50  0000 C CNN
F 1 "MountingHole_Pad" V 1350 7600 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1400 7100 50  0001 C CNN
F 3 "~" H 1400 7100 50  0001 C CNN
	1    1400 7100
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F3CB0D7
P 1400 7300
F 0 "H3" V 1450 7550 50  0000 C CNN
F 1 "MountingHole_Pad" V 1350 7800 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1400 7300 50  0001 C CNN
F 3 "~" H 1400 7300 50  0001 C CNN
	1    1400 7300
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F3CB39E
P 1400 7500
F 0 "H4" V 1450 7750 50  0000 C CNN
F 1 "MountingHole_Pad" V 1350 8000 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1400 7500 50  0001 C CNN
F 3 "~" H 1400 7500 50  0001 C CNN
	1    1400 7500
	0    -1   -1   0   
$EndComp
NoConn ~ 1500 6900
NoConn ~ 1500 7100
NoConn ~ 1500 7300
NoConn ~ 1500 7500
Wire Wire Line
	8450 2850 8450 2650
Connection ~ 8450 2850
Wire Wire Line
	7800 2850 8450 2850
$Comp
L Device:C_Small C16
U 1 1 5ED5C26A
P 8450 2550
F 0 "C16" H 8542 2596 50  0000 L CNN
F 1 "100nF" H 8542 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8450 2550 50  0001 C CNN
F 3 "~" H 8450 2550 50  0001 C CNN
	1    8450 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2350 8450 2350
Wire Wire Line
	8450 2450 8450 2350
Text Notes 7750 3250 0    79   ~ 0
Overvoltage protection/ \ncrowbar circuit
$Comp
L Device:R_Small R56
U 1 1 5FA791A4
P 7800 2550
F 0 "R56" H 7859 2596 50  0000 L CNN
F 1 "470R" H 7859 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7800 2550 50  0001 C CNN
F 3 "~" H 7800 2550 50  0001 C CNN
	1    7800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2400 9100 2850
Wire Wire Line
	9100 2850 8450 2850
Wire Wire Line
	5500 1450 9100 1450
Wire Wire Line
	9100 1450 9100 2100
$Comp
L Device:Q_SCR_KAG Q1
U 1 1 5ED58F94
P 9100 2250
F 0 "Q1" H 9188 2296 50  0000 L CNN
F 1 "S6002xS" H 9188 2205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" V 9100 2250 50  0001 C CNN
F 3 "~" V 9100 2250 50  0001 C CNN
F 4 "S6002TSRP " H 9100 2250 50  0001 C CNN "Manufacturer"
F 5 "576-S6002TSRP" H 9100 2250 50  0001 C CNN "Mouser"
	1    9100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2850 5500 2850
Wire Wire Line
	5900 1650 5900 1950
Wire Wire Line
	5900 1650 6350 1650
$Comp
L Device:D_Small D?
U 1 1 5F9A523D
P 6450 1650
AR Path="/5F9A523D" Ref="D?"  Part="1" 
AR Path="/5F9BBF68/5F9A523D" Ref="D17"  Part="1" 
F 0 "D17" H 6600 1600 50  0000 C CNN
F 1 "S1M" H 6300 1600 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" V 6450 1650 50  0001 C CNN
F 3 "~" V 6450 1650 50  0001 C CNN
F 4 "512-S1M " H 6450 1650 50  0001 C CNN "Mouser"
F 5 "S1M" H 6450 1650 50  0001 C CNN "Manufacturer"
	1    6450 1650
	1    0    0    1   
$EndComp
Wire Wire Line
	5500 1950 5900 1950
Wire Wire Line
	7100 1650 7100 1950
Wire Wire Line
	6550 1650 7100 1650
$Comp
L Regulator_Linear:LT3080xQ U25
U 1 1 5FB58110
P 6500 2050
F 0 "U25" H 6150 2300 50  0000 C CNN
F 1 "LT3080xQ" H 6750 1800 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-5_TabPin3" H 6500 1750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/3080fc.pdf" H 4780 2820 50  0001 C CNN
F 4 "LT3080EQ#TRPBF" H 6500 2050 50  0001 C CNN "Manufacturer"
F 5 "584-LT3080EQ#TRPBF" H 6500 2050 50  0001 C CNN "Mouser"
	1    6500 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1950 7100 1950
Connection ~ 7100 1950
Wire Wire Line
	7000 2050 7100 2050
Wire Wire Line
	7100 2050 7100 1950
Wire Wire Line
	6000 1950 5900 1950
Connection ~ 5900 1950
Wire Wire Line
	6000 2050 5900 2050
Wire Wire Line
	5900 2050 5900 1950
$Comp
L Device:R_Small R52
U 1 1 5FBE3C7E
P 5900 2450
F 0 "R52" H 5959 2496 50  0000 L CNN
F 1 "330k" H 5959 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5900 2450 50  0001 C CNN
F 3 "~" H 5900 2450 50  0001 C CNN
	1    5900 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2350 5900 2150
Wire Wire Line
	5900 2150 6000 2150
Wire Wire Line
	5900 2550 5900 2850
Connection ~ 5900 2850
Text Notes 6200 2500 0    50   ~ 0
Rset = Vout * 10uA
$Comp
L Device:C_Small C11
U 1 1 5FC0A443
P 5500 2450
F 0 "C11" H 5592 2496 50  0000 L CNN
F 1 "2.2uF" H 5592 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5500 2450 50  0001 C CNN
F 3 "~" H 5500 2450 50  0001 C CNN
	1    5500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2350 5500 1950
Wire Wire Line
	5500 2550 5500 2850
Connection ~ 5500 2850
Wire Wire Line
	5500 2850 5900 2850
$Comp
L Device:C_Small C12
U 1 1 5FC432D2
P 7250 2450
F 0 "C12" H 7342 2496 50  0000 L CNN
F 1 "2.2uF" H 7342 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7250 2450 50  0001 C CNN
F 3 "~" H 7250 2450 50  0001 C CNN
	1    7250 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2350 7250 1950
Wire Wire Line
	7250 2550 7250 2850
Wire Wire Line
	7800 1900 7800 1950
Connection ~ 7800 1950
$Comp
L power:+3V3 #PWR0124
U 1 1 5FCFA7FF
P 7800 1900
F 0 "#PWR0124" H 7800 1750 50  0001 C CNN
F 1 "+3V3" H 7815 2073 50  0000 C CNN
F 2 "" H 7800 1900 50  0001 C CNN
F 3 "" H 7800 1900 50  0001 C CNN
	1    7800 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J14
U 1 1 5FD70C6F
P 10700 2150
F 0 "J14" H 10650 1850 50  0000 L CNN
F 1 "Conn_01x04" H 10450 2350 50  0001 L CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0004_1x04_P3.96mm_Vertical" H 10700 2150 50  0001 C CNN
F 3 "~" H 10700 2150 50  0001 C CNN
	1    10700 2150
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 2150 10450 2150
$Comp
L Device:LED LD1
U 1 1 5FD70C78
P 9750 2250
F 0 "LD1" V 9789 2132 50  0000 R CNN
F 1 "green" V 9698 2132 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 9750 2250 50  0001 C CNN
F 3 "~" H 9750 2250 50  0001 C CNN
	1    9750 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 2100 9750 1950
Connection ~ 9750 1950
Wire Wire Line
	9750 1950 10450 1950
$Comp
L Device:R_Small R54
U 1 1 5FD70C8A
P 9750 2600
F 0 "R54" H 9809 2646 50  0000 L CNN
F 1 "68R" H 9809 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9750 2600 50  0001 C CNN
F 3 "~" H 9750 2600 50  0001 C CNN
	1    9750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 2700 9750 2850
Wire Wire Line
	9750 2500 9750 2400
Wire Wire Line
	10450 2150 10450 2250
Wire Wire Line
	7100 1950 7250 1950
Wire Wire Line
	5900 2850 7250 2850
Connection ~ 7250 1950
Connection ~ 7250 2850
Wire Wire Line
	7250 2850 7800 2850
Wire Wire Line
	7800 1950 9750 1950
Wire Notes Line
	7700 1350 9550 1350
Wire Notes Line
	9550 3300 7700 3300
Wire Notes Line
	9550 1350 9550 3300
Wire Wire Line
	4200 2850 5150 2850
Wire Wire Line
	4200 1450 5150 1450
Wire Wire Line
	3650 2850 4200 2850
Connection ~ 4200 2850
Wire Wire Line
	4200 2850 4200 2300
Wire Wire Line
	4200 1450 4200 2000
Wire Wire Line
	3650 1450 4200 1450
Connection ~ 4200 1450
$Comp
L Device:CP C9
U 1 1 6025F846
P 3650 2150
F 0 "C9" H 3768 2196 50  0000 L CNN
F 1 "2200uF" H 3768 2105 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_16x17.5" H 3688 2000 50  0001 C CNN
F 3 "~" H 3650 2150 50  0001 C CNN
F 4 "MAL225099515E3" H 3650 2150 50  0001 C CNN "Manufacturer"
F 5 "594-MAL225099515E3" H 3650 2150 50  0001 C CNN "Mouser"
	1    3650 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C10
U 1 1 6026004D
P 4200 2150
F 0 "C10" H 4318 2196 50  0000 L CNN
F 1 "2200uF" H 4318 2105 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_16x17.5" H 4238 2000 50  0001 C CNN
F 3 "~" H 4200 2150 50  0001 C CNN
F 4 "MAL225099515E3" H 4200 2150 50  0001 C CNN "Manufacturer"
F 5 "594-MAL225099515E3" H 4200 2150 50  0001 C CNN "Mouser"
	1    4200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2350 8950 2350
Connection ~ 8450 2350
Wire Wire Line
	9750 2850 10450 2850
Wire Wire Line
	7250 1950 7800 1950
Wire Notes Line
	7700 3300 7700 1350
Wire Wire Line
	10500 2250 10450 2250
Connection ~ 10450 2250
Wire Wire Line
	10450 2250 10450 2850
Wire Wire Line
	10500 2050 10450 2050
Wire Wire Line
	10450 2050 10450 1950
Connection ~ 10450 1950
Wire Wire Line
	10450 1950 10500 1950
$Comp
L Device:D_Zener_Small D18
U 1 1 602D597F
P 7800 2150
F 0 "D18" V 7754 2220 50  0000 L CNN
F 1 "3V6" V 7845 2220 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 7800 2150 50  0001 C CNN
F 3 "~" V 7800 2150 50  0001 C CNN
F 4 "BZX84C3V6LT1G" H 7800 2150 50  0001 C CNN "Manufacturer"
F 5 "863-BZX84C3V6LT1G" H 7800 2150 50  0001 C CNN "Mouser"
	1    7800 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 2850 9100 2850
Connection ~ 9750 2850
Connection ~ 9100 2850
$EndSCHEMATC
